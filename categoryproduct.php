<?php include'includes/header.php';?>
<?php include'includes/allproductheader.php';?>
<div class="products-container">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 col-lg-12 p-cont-header">
				<h5>Women clothing</h5>
				<span class="pull-right">Sort by <i class="caret"></i></span>
			</div>
			<div class="col-md-12 col-lg-12 main-products-container">
				<div class="col-md-3 col-lg-3 filter-sec">
					<?php include'includes/filters.php';?>
				</div>
				<div class="col-md-9 col-lg-9 product-sec">
					<?php for ($i=1; $i < 9 ; $i++): { 
						# code...
					} ;?>
					<div class="col-sm-6 col-md-6 col-lg-4 product-land">
						<div class="product">
							<div class="product-img">
								<div id="carousel-example-generic-<?=$i;?>" class="carousel slide" data-ride="carousel" data-interval="false">
								  <!-- Indicators -->
								  <ol class="carousel-indicators">
								    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
								    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
								    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
								  </ol>

								  <!-- Wrapper for slides -->
								  <div class="carousel-inner" role="listbox">
								    <div class="item active">
								      <img src="img/products/women/<?=$i;?>.jpg" class="img-responsive">
								    </div>
								    <div class="item">
								      <img src="img/products/women/<?=$i;?>.jpg" class="img-responsive">
								    </div>
								    <div class="item">
								      <img src="img/products/women/<?=$i;?>.jpg" class="img-responsive">
								    </div>
								  </div>

								  <!-- Controls -->
								  <a class="left carousel-control slider-contols" href="#carousel-example-generic-<?=$i;?>" role="button" data-slide="prev">
								    <span class="glyphicon glyphicon-chevron-left slider-contols-span" aria-hidden="true"></span>
								    <span class="sr-only">Previous</span>
								  </a>
								  <a class="right carousel-control slider-contols" href="#carousel-example-generic-<?=$i;?>" role="button" data-slide="next">
								    <span class="glyphicon glyphicon-chevron-right slider-contols-span" aria-hidden="true"></span>
								    <span class="sr-only">Next</span>
								  </a>
								</div>
							</div>
							<div class="product-desc">
								<h5>product description</h5>
								<h6>$60.55 | 1 Available</h6>
								<a class="button button--shikoba button--border-thin add-cart-btn "><i class="button__icon icon fa fa-shopping-cart" aria-hidden="true"></i><span> &nbsp ADD CART</span></a>	
							</div>
						</div>
					</div>
				<?php endfor ;?>
				</div>
			</div>	

		</div>
	</div>
</div>


<?php include'includes/footer.php';?>
