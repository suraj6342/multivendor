<?php session_start();

if(isset($_SESSION))
{
	if(!empty($_SESSION['login']))
	{	
		if(($_SESSION['login'] == 1))
		{
			Header("Location:pages/admin.php");
		} 
	}
	else
	{ ?>
<!DOCTYPE html>
<html>
<head>
<title>index</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta http-equiv="X-UA-Compatible" content="IE=edge"><!--for internet explorer-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet">
 </head>
 <body>

 
<nav style="padding:5px;" class="navbar navbar-default navbar-fixed-top index-nav">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand a1" href="#">Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li><a style="cursor:pointer;" data-toggle="modal" data-target=".bs-example-modal-lg1"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Signup/Register</a></li>
</button>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="slide1">
<div class="container-fluid">
	<div class="row">
				
				<h1 data-scroll-reveal="enter from the top after 0.2s" class="col-xs-10 col-md-5 slide1_heading">Education</h1>
				<h1 data-scroll-reveal="enter from the top after 0.3s" class="col-xs-2 col-md-1">+</h1>
				<h1 data-scroll-reveal="enter from the top after 0.4s" class="col-xs-10 col-md-6">Management</h1>
				<p><a class="btn btn-primary col-md-5 col-md-offset-3" role="button" data-toggle="modal" data-target="#myModal">Get-In</a></p>			
	</div>
</div>
</div>

<!--
<div class="slide2">
	<div class="container bottom-margin margin-top-50">
		<div class="row">
			<div data-scroll-reveal="enter from the bottom after 0.1s" style="padding:0px 5px;" class="col-xs-6 col-md-3">
				<div class="row">
				  <div class="pad-all">
					<div class="thumbnail slide2-tab">
					  <img src="..." alt="...">
					  <div style="text-align:center;" class="caption">
						<h3 class="white-col">Admin</h3>
						<p>...</p>
						<p><a class="btn btn-primary" role="button" data-toggle="modal" data-target="#myModal">Get-In</a></p>
					  </div>
					</div>
				  </div>
				</div>
			</div>
			<div data-scroll-reveal="enter from the bottom after 0.2s" style="padding:0px 5px;" class="col-xs-6 col-md-3 ">
				<div class="row">
				  <div class="pad-all">
					<div class="thumbnail slide2-tab ">
					  <img src="..." alt="...">
					  <div style="text-align:center;" class="caption">
						<h3 class="white-col">School</h3>
						<p>...</p>
						<p><a class="btn btn-primary" role="button" data-toggle="modal" data-target="#myModal-others">Get-In</a></p>
					  </div>
					</div>
				  </div>
				</div>
			</div>
			<div data-scroll-reveal="enter from the bottom after 0.3s" style="padding:0px 5px;" class="col-xs-6 col-md-3">
				<div class="row">
				  <div class=" pad-all">
					<div class="thumbnail slide2-tab">
					  <img src="..." alt="...">
					  <div style="text-align:center;" class="caption ">
						<h3 class="white-col">Parent</h3>
						<p>...</p>
						<p><a class="btn btn-primary" role="button" data-toggle="modal" data-target="#myModal-others">Get-In</a></p>
					  </div>
					</div>
				  </div>
				</div>
			</div>
			<div data-scroll-reveal="enter from the bottom after 0.4s" style="padding:0px 5px;" class="col-xs-6 col-md-3">
				<div class="row">
				  <div class="pad-all">
					<div class="thumbnail slide2-tab">
					   <img src="..." alt="...">
					  <div style="text-align:center;" class="caption">
						<h3 class="white-col" >Student</h3>
						<p>...</p>
						<p><a class="btn btn-primary" role="button" data-toggle="modal" data-target="#myModal-others" >Get-In</a></p>
					  </div>
					</div>
				  </div>
				</div>
			</div>			
		</div>
	</div>
</div>
 -->

<!--login-modal start-->

<div class="modal mymodal fade back-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div style="margin-top:100px;" class=" modal-dialog" role="document">
    <div class="modal-content login-modal-header">
      <div class="modal-header login-modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2 class="modal-title" id="gridSystemModalLabel">Login</h2>
      </div>
      <div class="modal-body">
		<div class="row">
          
        </div>
        <div class="row">
          <div class="col-xs-12 col-md-12">
			<div class="input-group userid-input-bottom-margin">
			  <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
			  <input type="email" class="form-control " id="user_email" placeholder="User email" aria-describedby="basic-addon1" data-toggle="tooltip" data-placement="top" title="Tooltip on left">
			</div>
		  </div>
          <div class="col-xs-12 col-md-12">
			<div class="input-group">
			  <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-lock"></span></span>
			  <input type="password" class="form-control" id="user_pass" placeholder="Password" aria-describedby="basic-addon1" data-toggle="tooltip" data-placement="top" title="Tooltip on left">
			</div>
		  </div>
        </div>                
      </div>
      <div class="modal-footer login-modal-header">
		<button type="button" class="btn btn-primary" id="login">Log-In</button>	
      </div>
    </div>
  </div>
</div>

<!-- other login modal-->
<div class="modal fade back-modal-others" id="myModal-others" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div style="margin-top:100px;" class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h2 class="modal-title" id="gridSystemModalLabel">Login</h2>
			</div>
		<div class="modal-body">
		<div class="row">
          
        </div>
        <div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="input-group userid-input-bottom-margin">
					<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
					<input type="email" class="form-control" id="other_email" placeholder="User email" required="required" aria-describedby="basic-addon1">
				</div>
			</div>
			<div class="col-xs-12 col-md-12">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-lock"></span></span>
					<input type="password" class="form-control" id="other_pass" placeholder="Password" required="required" aria-describedby="basic-addon1">
				</div>
			</div>
        </div>                
      </div>
      <div class="modal-footer login-modal-header">
		<button type="button" class="btn btn-primary" id="other_login">Log-In</button>	
      </div>
    </div>
  </div>
</div>


<!--signup modal-->
<!-- Button trigger modal -->
<div class="modal back-modal modal3 fade bs-example-modal-lg1""  id="mymodal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content login-modal-header">
      <div class="modal-header login-modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><h3>Signpup / Register</h3></h4>
      </div>
      <div class="modal-body pad-left-right-40">
						<div class="form-group has-success has-feedback">
						  <label for="signup-name">Name</label>
						  <input type="text" class="form-control" id="signup-name">
						  <span class="glyphicon glyphicon-ok form-control-feedback"></span>
						  <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						</div>
						<div class="form-group has-success has-feedback">
						  <label for="email">E-mail</label>
						  <input type="email" class="form-control" id="email">
						  <span class="glyphicon glyphicon-ok form-control-feedback"></span>
						  <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						  
						</div>
						<div class="form-group has-success has-feedback">
						  <label for="pass">Password</label>
						  <input type="password" class="form-control" id="pass">
						  <span class="glyphicon glyphicon-ok form-control-feedback" id="ok"></span>
						  <span class="glyphicon glyphicon-remove form-control-feedback" id="not-ok"></span>
						</div>
						<div class="form-group has-success has-feedback">
						  <label for="re-pass">Re-enter password</label>
						  <input type="password" class="form-control" id="re_pass">
						  <span class="glyphicon glyphicon-ok form-control-feedback"></span>
						  <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						</div>
						<div class="form-group has-success has-feedback">
						  <label for="mob">mobile no</label>
						  <input type="tel" class="form-control" id="mob">
						  <span class="glyphicon glyphicon-ok form-control-feedback"></span>
						  <span class="glyphicon glyphicon-remove form-control-feedback"></span>
						</div>
      <div class="modal-footer login-modal-header">
			<button type="button" class="btn btn-primary" id="signupdone" >Done</button>
      </div>
    </div>
  </div>
</div>
</div>

<!-- Modal -->


<!--pass error modal-->
<!--from index.js-->
<div id="error-modal">
</div>
<!--end-->


<div class="slide3">
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-md-8 col-md-offset-2 margin-top margin-bottom-30">
			<div class="jumbotron heading-style-con">
				<div class="row text-center">
					<div class="col-lg-8 col-lg-offset-2 col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                     <h4 data-scroll-reveal="enter from the bottom after 0.1s" class="header-line">CONTACT US</h4>                     
					</div>
				</div>
             <!--/.HEADER LINE END-->
				<div class="row set-row-pad"  data-scroll-reveal="enter from the bottom after 0.5s" >
					<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
						<form>
							<div class="form-group">
								<input style="background:transparent;" type="text" class="form-control white-col "  required="required" placeholder="Your Name" />
							</div>
							<div class="form-group">
								<input style="background:transparent;" type="text" class="form-control white-col" required="required"  placeholder="Your Email" />
							</div>
							<div class="form-group">
								<textarea name="message" style="background:transparent;" required="required" class="form-control white-col" style="min-height: 150px;" placeholder="Message"></textarea>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block">SUBMIT</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>


<div class="container">
    <div class="row set-row-pad"  >
		<div class="col-xs-12 col-md-4 col-md-offset-1" data-scroll-reveal="enter from the bottom after 0.4s">
            <h4 ><strong>Our Location</strong></h4>
			<hr />
            <div>
                <h5>234/80-UFG, dehradun</h5>
                <h5>Uttrakhand</h5>
                <h5><strong>Call:</strong>+91 9557263391 </h5>
                <h5><strong>Email: </strong>suraj63421@gmail.com</h5>
            </div>
		</div>
            <div class="col-xs-12 col-md-4 navbar-right" data-scroll-reveal="enter from the bottom after 0.4s">
				<h4 ><strong>Social Conectivity </strong></h4>
				<hr />
                <div >
                     <i class="fa fa-facebook-official fa-lg"></i> 
					 <i class="fa fa-google fa-lg"></i> 
					 <i class="fa fa-linkedin fa-lg"></i> 
                </div>
          </div>
	</div>
 </div>
<!-- CONTACT SECTION END-->



 <div style="background:black; padding:5px 0px; text-align:center; color:white; font-size:16px; font-family:"Calibri";" id="footer">
          <p>&copy 2016 yourdomain.com | All Rights Reserved | <a href="#" target="_blank">Design by : Suraj Rawat</a> </p>
</div>

<script src="js/jquery.js"></script>
<script src="js/index.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/login.js"></script>
<script src="js/custom.js"></script>
<script src="js/scrollReveal.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
</body>

</html>

	<?php
	}
}


?>