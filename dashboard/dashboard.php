<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';

$result = is_logged_in();
if ($result == '0' ) {
	login_error_redirect();
}

$vendorsql = $db->query("SELECT * FROM vendors ORDER BY vendor_id DESC");


include'includes/header.php'; ?>
				
				<ul class="nav">
	                <li class="active">
	                    <a href="dashboard.php">
	                        <i class="material-icons">dashboard</i>
	                        <p>Dashboard</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="brands.php">
	                        <i class="material-icons">content_paste</i>
	                        <p>Brands</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="categories.php">
	                        <i class="material-icons">library_books</i>
	                        <p>Categories</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="products.php">
	                        <i class="material-icons">bubble_chart</i>
	                        <p>Products</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="archieve.php">
	                        <i class="material-icons">location_on</i>
	                        <p>Archieve</p>
	                    </a>
	                </li>
					<?php  $result = has_permission(); 
					if( $result == '1'): ?>
						<li>
		                    <a href="vendors.php">
		                        <i class="material-icons text-gray">notifications</i>
		                        <p>Vendors</p>
		                    </a>
	                	</li>
					<?php endif;?>
	                
	                <?php  $result = has_permission(); 
					if( $result == '1'): ?>
					<li>
	                    <a href="frontui.php">
	                        <i class="material-icons text-gray">notifications</i>
	                        <p>Front UI</p>
	                    </a>
	                </li>
	                <?php endif;?>
					
	            </ul>
	    	</div>
	    </div>
	    
	    <div class="main-panel">
			<?php include'includes/navbar.php'; ?>

			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header" data-background-color="orange">
									<img src="assets/img/icons/icons8-Gift.png" class="img-responsive">
								</div>
								<div class="card-content">
									<h6 class="category">Products Sold</h6>
									<h5 class="title">500P/1000P<small></small></h5>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">date_range</i> Updated Last 24 Hours
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header" data-background-color="green">
									<img src="assets/img/icons/icons8-Rupee.png" class="img-responsive">
								</div>
								<div class="card-content">
									<h6 class="category">Yearly Revenue</h6>
									<h5 class="title">₹ 1,60,000<small></small></h5>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">date_range</i> Updated Last 24 Hours
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header" data-background-color="red">
									<img src="assets/img/icons/icons8-Delivery.png" class="img-responsive">
								</div>
								<div class="card-content">
									<h6 class="category">Today Orders</h6>
									<h5 class="title">100<small></small></h5>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">date_range</i> Updated Last 24 Hours
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header" data-background-color="blue">
									<img src="assets/img/icons/icons8-Facebook.png" class="img-responsive">
								</div>
								<div class="card-content">
									<h6 class="category">Total Followers</h6>
									<h5 class="title">+20,000<small></small></h5>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">date_range</i> Updated Last 24 Hours
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							<div class="card">
								<div class="card-header card-chart" data-background-color="green">
									<div class="ct-chart" id="dailySalesChart"></div>
								</div>
								<div class="card-content">
									<h4 class="title">Daily Sales</h4>
									<p class="category"><span class="text-success"><i class="fa fa-long-arrow-up"></i> 55%  </span> increase in today sales.</p>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">access_time</i> updated 4 minutes ago
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-4">
							<div class="card">
								<div class="card-header card-chart" data-background-color="orange">
									<div class="ct-chart" id="emailsSubscriptionChart"></div>
								</div>
								<div class="card-content">
									<h4 class="title">Email Subscriptions</h4>
									<p class="category">Last Campaign Performance</p>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">access_time</i> campaign sent 2 days ago
									</div>
								</div>

							</div>
						</div>

						<div class="col-md-4">
							<div class="card">
								<div class="card-header card-chart" data-background-color="red">
									<div class="ct-chart" id="completedTasksChart"></div>
								</div>
								<div class="card-content">
									<h4 class="title">Completed Tasks</h4>
									<p class="category">Last Campaign Performance</p>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">access_time</i> campaign sent 2 days ago
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<div class="card">
	                            <div class="card-header text-center" data-background-color="orange">
	                                <h4 class="title"> Vendors Details </h4>
	                                <p class="category">all vendors in your websites </p>
	                            </div>
	                            <div class="card-content table-responsive">
	                                <table class="table table-hover">
	                                    <thead class="text-warning">
	                                        <th>ID</th>
	                                    	<th>Profile</th>
	                                    	<th>Name</th>
	                                    	<th>Email</th>
	                                    	<th>Mobile</th>
	                                    </thead>
	                                    <tbody>
	                                    	<?php while($vendor = mysqli_fetch_assoc($vendorsql)): ?>
	                                        <tr>
	                                        	<td><?=$vendor['vendor_id'];?></td>
	                                        	<td><img style="height: 50px; width: 70px; border-radius: 50%;" src="<?=$vendor['profile_image'];?>" class="img-responsive"></td>
	                                        	<td><?=$vendor['full_name'];?></td>
	                                        	<td><?=$vendor['email'];?></td>
	                                        	<td><?=$vendor['mobile'];?></td>
	                                        </tr>
	                                    <?php endwhile; ?>
	                                    </tbody>
	                                </table>
	                            </div>
	                        </div>
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="card">
	                            <div class="card-header text-center" data-background-color="orange">
	                                <h4 class="title">Active Vendors</h4>
	                                <p class="category">all vendors in your websites </p>
	                            </div>
	                            <div class="card-content table-responsive">
	                                <table class="table table-hover">
	                                    <thead class="text-warning">
	                                        <th>ID</th>
	                                    	<th>Profile</th>
	                                    	<th>Name</th>
	                                    	<th>Email</th>
	                                    	<th>Mobile</th>
	                                    </thead>
	                                    <tbody>
	                                        <tr>
	                                    	<?php

	                                    	$activesql = $db->query("SELECT * FROM vendors WHERE current_login = '1'");
	                                    	while($vendor = mysqli_fetch_assoc($activesql)): ?>
	                                        <tr>
	                                        	<td><?=$vendor['vendor_id'];?></td>
	                                        	<td><img style="height: 50px; width: 70px; border-radius: 50%;" src="<?=$vendor['profile_image'];?>" class="img-responsive"></td>
	                                        	<td><?=$vendor['full_name'];?></td>
	                                        	<td><?=$vendor['email'];?></td>
	                                        	<td><?=$vendor['mobile'];?></td>
	                                        </tr>
	                                    	<?php endwhile; ?>
	                                        </tr>
	                                    </tbody>
	                                </table>
	                            </div>
	                        </div>
						</div>

					</div>

					<div class="row">
						<div class="col-lg-6 col-md-12">
							<div class="card">
	                            <div class="card-header" data-background-color="orange">
	                                <h4 class="title">Orders</h4>
	                                <p class="category">orders from customer, ready for shipping</p>
	                            </div>
	                            <div class="card-content table-responsive">
	                                <table class="table table-hover">
	                                    <thead class="text-warning">
	                                        <th>ID</th>
	                                    	<th>Name</th>
	                                    	<th>Salary</th>
	                                    	<th>Country</th>
	                                    </thead>
	                                    <tbody>
	                                        <tr>
	                                        	<td>1</td>
	                                        	<td>Dakota Rice</td>
	                                        	<td>$36,738</td>
	                                        	<td>Niger</td>
	                                        </tr>
	                                        <tr>
	                                        	<td>2</td>
	                                        	<td>Minerva Hooper</td>
	                                        	<td>$23,789</td>
	                                        	<td>Curaçao</td>
	                                        </tr>
	                                        <tr>
	                                        	<td>3</td>
	                                        	<td>Sage Rodriguez</td>
	                                        	<td>$56,142</td>
	                                        	<td>Netherlands</td>
	                                        </tr>
	                                        <tr>
	                                        	<td>4</td>
	                                        	<td>Philip Chaney</td>
	                                        	<td>$38,735</td>
	                                        	<td>Korea, South</td>
	                                        </tr>
	                                    </tbody>
	                                </table>
	                            </div>
	                        </div>
						</div>

						<div class="col-lg-6 col-md-12">
							<div class="card">
	                            <div class="card-header" data-background-color="orange">
	                                <h4 class="title">Shipped Order</h4>
	                                <p class="category">order shipped, click for more</p>
	                            </div>
	                            <div class="card-content table-responsive">
	                                <table class="table table-hover">
	                                    <thead class="text-warning">
	                                        <th>ID</th>
	                                    	<th>Name</th>
	                                    	<th>Salary</th>
	                                    	<th>Country</th>
	                                    </thead>
	                                    <tbody>
	                                        <tr>
	                                        	<td>1</td>
	                                        	<td>Dakota Rice</td>
	                                        	<td>$36,738</td>
	                                        	<td>Niger</td>
	                                        </tr>
	                                        <tr>
	                                        	<td>2</td>
	                                        	<td>Minerva Hooper</td>
	                                        	<td>$23,789</td>
	                                        	<td>Curaçao</td>
	                                        </tr>
	                                        <tr>
	                                        	<td>3</td>
	                                        	<td>Sage Rodriguez</td>
	                                        	<td>$56,142</td>
	                                        	<td>Netherlands</td>
	                                        </tr>
	                                        <tr>
	                                        	<td>4</td>
	                                        	<td>Philip Chaney</td>
	                                        	<td>$38,735</td>
	                                        	<td>Korea, South</td>
	                                        </tr>
	                                    </tbody>
	                                </table>
	                            </div>
	                        </div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-6 col-md-12">
							<div class="card">
	                            <div class="card-header" data-background-color="orange">
	                                <h4 class="title"> Low Inventory </h4>
	                                <p class="category">add more products quantity in products page</p>
	                            </div>
	                            <div class="card-content table-responsive">
	                                <table class="table table-hover">
	                                    <thead class="text-warning">
	                                        <th>ID</th>
	                                    	<th>Name</th>
	                                    	<th>Salary</th>
	                                    	<th>Country</th>
	                                    </thead>
	                                    <tbody>
	                                        <tr>
	                                        	<td>1</td>
	                                        	<td>Dakota Rice</td>
	                                        	<td>$36,738</td>
	                                        	<td>Niger</td>
	                                        </tr>
	                                        <tr>
	                                        	<td>2</td>
	                                        	<td>Minerva Hooper</td>
	                                        	<td>$23,789</td>
	                                        	<td>Curaçao</td>
	                                        </tr>
	                                        <tr>
	                                        	<td>3</td>
	                                        	<td>Sage Rodriguez</td>
	                                        	<td>$56,142</td>
	                                        	<td>Netherlands</td>
	                                        </tr>
	                                        <tr>
	                                        	<td>4</td>
	                                        	<td>Philip Chaney</td>
	                                        	<td>$38,735</td>
	                                        	<td>Korea, South</td>
	                                        </tr>
	                                    </tbody>
	                                </table>
	                            </div>
	                        </div>
						</div>
					</div>
				</div>
			</div>

<?php include'includes/footer.php'; ?>
<script type="text/javascript">
    	$(document).ready(function(){

			// Javascript method's body can be found in assets/js/demos.js
        	demo.initDashboardPageCharts();

    	});
</script>