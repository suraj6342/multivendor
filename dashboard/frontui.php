<?php 
require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';
$result = is_logged_in();
if ($result == '0' ) {
	login_error_redirect();
}


$productquery = $db->query("SELECT * FROM products WHERE deleted = '0' ORDER BY id DESC");

if (isset($_GET['showcase'])) {
 	$id = (int)$_GET['id'];
 	$showcase = (int)$_GET['showcase'];
 	$showcasesql = " UPDATE products SET showcase_select = '$showcase' WHERE id = '$id' ";
 	$db->query($showcasesql);
 	header('Location:frontui.php');
 } 
if (isset($_GET['newcollection'])) {
 	$id = (int)$_GET['id'];
 	$newcollection = (int)$_GET['newcollection'];
 	$newcollectionsql = " UPDATE products SET new_collection_select = '$newcollection' WHERE id = '$id' ";
 	$db->query($newcollectionsql);
 	header('Location:frontui.php');
 } 
if (isset($_GET['newinmen'])) {
 	$id = (int)$_GET['id'];
 	$newinmen = (int)$_GET['newinmen'];
 	$newinmensql = " UPDATE products SET new_in_men_select = '$newinmen' WHERE id = '$id' ";
 	$db->query($newinmensql);
 	header('Location:frontui.php');
 } 
if (isset($_GET['newinwomen'])) {
 	$id = (int)$_GET['id'];
 	$newinwomen = (int)$_GET['newinwomen'];
 	$newinwomensql = " UPDATE products SET new_in_women_select = '$newinwomen' WHERE id = '$id' ";
 	$db->query($newinwomensql);
 	header('Location:frontui.php');
 } 


include'includes/header.php'; ?>
		<ul class="nav" id="nav">
	                <li>
	                    <a href="dashboard.php">
	                        <i class="material-icons">dashboard</i>
	                        <p>Dashboard</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="brands.php">
	                        <i class="material-icons">content_paste</i>
	                        <p>Brands</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="categories.php">
	                        <i class="material-icons">library_books</i>
	                        <p>Categories</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="products.php">
	                        <i class="material-icons">bubble_chart</i>
	                        <p>Products</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="archieve.php">
	                        <i class="material-icons">location_on</i>
	                        <p>Archieve</p>
	                    </a>
	                </li>
   	               	<?php  $result = has_permission(); 
					if( $result == '1'): ?>
						<li>
		                    <a href="vendors.php">
		                        <i class="material-icons text-gray">notifications</i>
		                        <p>Vendors</p>
		                    </a>
	                	</li>
					<?php endif;?>

	                <?php  $result = has_permission(); 
					if( $result == '1'): ?>
					<li class="active">
	                    <a href="frontui.php">
	                        <i class="material-icons text-gray">notifications</i>
	                        <p>Front UI</p>
	                    </a>
	                </li>
	                <?php endif;?>
					
	            </ul>
	    	</div>
	    </div>
	    
	    <div class="main-panel">
			<?php include'includes/navbar.php'; ?>


	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-12">
	                    	<div class="card">
		                            <div class="card-header text-center" data-background-color="orange">
		                                <h4 class="title">Products List</h4>
		                                <p class="category">all products in database</p>
		                            </div>
		                            <div class="card-content table-responsive">
		                            	<table class="table table-hover table-stiped">
		                                    <thead class="text-warning">
		                                        <tr>
			                                        <th class="text-center">Id</th>
			                                    	<th class="text-center">Title</th>
			                                    	<th class="text-center">Price</th>
			                                    	<th class="text-center">Category</th>
			                                    	<th class="text-center">Image</th>
			                                    	<th class="text-center">Showcase Product (select only one)</th>
			                                    	<th class="text-center">New Collection Products</th>
			                                    	<th class="text-center">New In (Men)</th>
			                                    	<th class="text-center">New In (Women)</th>

			                                    </tr>
		                                    </thead>
		                                    <tbody class="text-center" id="data-row">
		                                        <?php while($product = mysqli_fetch_assoc($productquery)): 
		                                        
		                                        $vendorid = $product['vendor_id'];
		                                        $shopnamequery = $db->query("SELECT * FROM vendors WHERE vendor_id = $vendorid");
		                                        $shopname = mysqli_fetch_assoc($shopnamequery);

		                                        $brandid = $product['brand_id'];
		                                        $brandname = $db->query("SELECT * FROM brands WHERE id = '$brandid' ");
		                                        $brand = mysqli_fetch_assoc($brandname);

		                                       	$childID = $product['category_id'];
												$result = $db->query("SELECT * FROM categories Where id = $childID");
												$child = mysqli_fetch_assoc($result);
												$parentID = $child['parent'];
												$presult = $db->query("SELECT * FROM categories WHERE id = '$parentID' ");
												$parent = mysqli_fetch_assoc($presult);
												$category = $parent['category_name'].'-'.$child['category_name'];

		                                        ?>
		                                        
		                                        <tr class="tablerow" id="pro<?=$product['id'];?>">
		                                        	<td><?=$product['id'];?></td>
		                                        	<td><?=$product['title'];?></td>
		                                        	<td><?=money($product['price']);?></td>
		                                        	<td><?=$category;?></td>
		                                        	<?php $photos = explode(',',$product['product_image']);?>
		                                        	<td><img style="height: 70px; width: 70px; border-radius: 50%;" src="<?=$photos[0];?>" class="img-responsive"></td>
		                                        	<td>
														<a href="frontui.php?showcase=<?=(($product['showcase_select']== '0')?'1':'0'); ?>&id=<?=$product['id'];?>" class="btn btn-xs btn-danger"><i class="material-icons"><?=(($product['showcase_select']== '1')?'remove':'add'); ?>_circle_outline</i> 
														</a>
		                                        	<td>
			    	                                	<a href="frontui.php?newcollection=<?=(($product['new_collection_select']== '0')?'1':'0'); ?>&id=<?=$product['id'];?>" class="btn btn-xs btn-danger"><i class="material-icons"><?=(($product['new_collection_select']== '1')?'remove':'add'); ?>_circle_outline</i> 
														</a>
		                                        	</td>
		                                        	<td>
			    	                                	<a href="frontui.php?newinmen=<?=(($product['new_in_men_select']== '0')?'1':'0'); ?>&id=<?=$product['id'];?>" class="btn btn-xs btn-danger"><i class="material-icons"><?=(($product['new_in_men_select']== '1')?'remove':'add'); ?>_circle_outline</i> 
														</a>
		                                        	</td>
		                                        	<td>
			    	                                	<a href="frontui.php?newinwomen=<?=(($product['new_in_women_select']== '0')?'1':'0'); ?>&id=<?=$product['id'];?>" class="btn btn-xs btn-danger"><i class="material-icons"><?=(($product['new_in_women_select']== '1')?'remove':'add'); ?>_circle_outline</i> 
														</a>
		                                        	</td>
		                                        </tr>
		                                    	<?php endwhile; ?>
		                                    </tbody>
		                                </table>
		                            </div>
		                    </div>
	                    </div>
	                </div>
	            </div>
	        </div>

<?php include'includes/footer.php'; ?>
