<?php 

require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';
$result = is_logged_in();
if ($result == '0' ) {
	login_error_redirect();
}


include'includes/header.php'; 
$brandquery = $db->query("SELECT * FROM brands ORDER BY id DESC");

?>

		<ul class="nav" id="nav">
	                <li>
	                    <a href="dashboard.php">
	                        <i class="material-icons">dashboard</i>
	                        <p>Dashboard</p>
	                    </a>
	                </li>
	                <li class="active">
	                    <a href="brands.php">
	                        <i class="material-icons">content_paste</i>
	                        <p>Brands</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="categories.php">
	                        <i class="material-icons">library_books</i>
	                        <p>Categories</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="products.php">
	                        <i class="material-icons">bubble_chart</i>
	                        <p>Products</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="archieve.php">
	                        <i class="material-icons">location_on</i>
	                        <p>Archieve</p>
	                    </a>
	                </li>
	                <?php  $result = has_permission(); 
					if( $result == '1'): ?>
						<li>
		                    <a href="vendors.php">
		                        <i class="material-icons text-gray">notifications</i>
		                        <p>Vendors</p>
		                    </a>
	                	</li>
					<?php endif;?>
					<?php  $result = has_permission(); 
					if( $result == '1'): ?>
					<li>
	                    <a href="frontui.php">
	                        <i class="material-icons text-gray">notifications</i>
	                        <p>Front UI</p>
	                    </a>
	                </li>
	                <?php endif;?>
					
	            </ul>
	    	</div>
	    </div>
	    
	    <div class="main-panel">
			<?php include'includes/navbar.php'; ?>


	        <div class="content list-card-xs">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="card">
	                            <div class="card-header text-center" data-background-color="purple">
	                                <h4 class="title">Add Brand</h4>
									<p class="category">enter brand name and select image of brand</p>
	                            </div>
	                            <section class="message"></section>
	                            <div class="card-content">
	                                    <div class="row">
	                                    	<div class="col-md-12 text-center">
			                                    <button class="btn btn-primary brand-btn">Add Brand</button>
			                                    <div class="clearfix"></div>
		                                    </div>
		                                    <form id="brandform" method="post" enctype="multipart/form-data">
		                                        <div class="row brand-form">
			                                    	<div class="col-sm-12 col-md-12">
			                                    		<div class="col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 ">
															<div class="form-group label-floating">
																<label class="control-label">Enter Brand</label>
																<input type="text" name="brand_name" id="brand_name" class="form-control" >
															</div>
														</div>
			                                        </div>
			                                        <div class="col-sm-8 col-md-6 col-lg-6 col-sm-offset-2 col-md-offset-3 col-lg-offset-3 text-center margin-t-10-sm">
				                                    	<div class="upload-btn text-center">
					                                   			<input type="file" name="brand_image" id="brand_image" class="brand-img">
					                                   			<label class="upload-label-brand">Brand Images (520 x 380)</label>
				                                    	</div>
				                                    </div>
			                                        <div class="col-sm-12 col-md-12 text-center">
			                                        	<hr class="btn-upper-hr-brand">
					                                    <button type="submit" class="btn btn-primary" id="add-brand-btn">Add</button>
					                                    <input type="addbrand" name="addbrand" id="addbrand" class="hidden" value="0">
					                                </div>
					                            </div>
				                            </form>
										</div>                                   
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-md-12">
	                    	<div class="col-sm-12 list-card-xs">
								<div class="card">
		                            <div class="card-header text-center" data-background-color="orange">
		                                <h4 class="title">Brand List</h4>
		                                <p class="category">all brands in database</p>
		                            </div>
		                            <div class="card-content table-responsive">
		                                <table class="table table-hover table-stiped">
		                                    <thead class="text-warning">
		                                        <tr>
			                                        <th class="text-center">Id</th>
			                                    	<th class="text-center">Brand Image</th>
			                                    	<th class="text-center">Brand Name</th>
			                                    	<th class="text-center">Edit / Remove</th>
			                                    </tr>
		                                    </thead>
		                                    <tbody class="text-center" id="data-row">
		                                        <?php while($brand = mysqli_fetch_assoc($brandquery)): ?>

			                                        <tr class="tablerow" id="<?=$brand['id'];?>">
			                                        	<td><?=$brand['id'];?></td>
			                                        	<td><img style="height: 70px; width: 70px; border-radius: 50%;" src="<?=$brand['brand_image'];?>" class="img-responsive profile_image"></td>
			                                        	<td><?=$brand['brand_name'];?></td>
			                                        	<td><button class="btn btn-xs btn-danger" onclick="brandmodal(<?=$brand['id'];?>)"><i class="material-icons">create</i></span></i></button>
			                                        	<button class="btn btn-xs btn-danger" onclick="removebrand(<?=$brand['id'];?>)"><i class="material-icons">delete_sweep</i></button>
			                                        	</td>
			                                        </tr>
			                                    <?php endwhile; ?>

		                                    </tbody>
		                                </table>
		                            </div>
		                        </div>
							</div>
	                    </div>
	                </div>
	            </div>
	        </div>

<?php include'includes/footer.php'; ?>
<script type="text/javascript">
	$('.brand-btn').click(function(){
		$('.brand-form').slideToggle();
	});
</script>
<script type="text/javascript">

	$("form#brandform").submit(function(e){
		 e.preventDefault();
		 		$('input[name=addbrand]').attr('value','1');
			
			    var formData = new FormData(this);
				
    	 	jQuery.ajax({
				url : '/multivendor/dashboard/parser/ajax.php',
				method : 'POST',
				data : formData,
				async: true,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data){ 
						// alert(data);
						var result = JSON.parse(data);

						if ( result.status == "success" ) {
							$('.brand-form').slideUp();
							$("form#brandform")[0].reset();
				        	$(result.data).prependTo("#data-row");
				           	// location.reload();
				           		//alert("insert");
				        
				        }else if ( result.status == "fail" ){
						
							$('.message').html(result.data);
						}else{
							alert("Something went wrong with database");
						}		
		                        	//alert-close
		                        	$('#alert-close').click(function(){
										$('.message').html(" ");
									});
				 },
				
				error : function(){ alert("something went wrong");}   
			});  
    	 });
  
			
</script>
<script type="text/javascript">
	
	function removebrand(brand_id){
		
		var removebrand = 0;
		 
	    $("#"+ brand_id).fadeOut("slow", function() { $(this).remove(); });
	    jQuery.ajax({
				url : '/multivendor/dashboard/parser/ajax.php',
				method : 'POST',
				data : {brand_id: brand_id, removebrand : 1},
				async : true,
				success : function(data){
//				alert(data); 
				},
				error : function(){ alert("something went wrong");}   
			}); 
	}

	function brandmodal(id){

		var data ={"id" : id};
		jQuery.ajax({
			url :'/multivendor/dashboard/parser/brandmodal.php',
			method : "post",
			data : data,
			async: true,
			success:function(data){
				jQuery('body').append(data);
				jQuery('#details-modal').modal('toggle');
			 },
			error:function(){
				alert("something went wrong");
			}
		});
		
	}

</script>
