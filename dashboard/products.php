<?php 
require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';
$result = is_logged_in();
if ($result == '0' ) {
	login_error_redirect();
}


include'includes/header.php'; 

$brandquery =$db->query("SELECT * FROM brands ORDER BY brand_name");
$parentQuery = $db->query("SELECT * FROM categories WHERE parent = 'parent' ORDER BY category_name");
$productquery = $db->query("SELECT * FROM products WHERE deleted = '0' ORDER BY id DESC");

$ven_id = $_SESSION['SBvendor'];
$venproductquery = $db->query("SELECT * FROM products WHERE vendor_id = '$ven_id' AND deleted = '0' ");

?>
		<ul class="nav" id="nav">
	                <li>
	                    <a href="dashboard.php">
	                        <i class="material-icons">dashboard</i>
	                        <p>Dashboard</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="brands.php">
	                        <i class="material-icons">content_paste</i>
	                        <p>Brands</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="categories.php">
	                        <i class="material-icons">library_books</i>
	                        <p>Categories</p>
	                    </a>
	                </li>
	                <li class="active">
	                    <a href="products.php">
	                        <i class="material-icons">bubble_chart</i>
	                        <p>Products</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="archieve.php">
	                        <i class="material-icons">location_on</i>
	                        <p>Archieve</p>
	                    </a>
	                </li>
					<?php  $result = has_permission(); 
					if( $result == '1'): ?>
						<li>
		                    <a href="vendors.php">
		                        <i class="material-icons text-gray">notifications</i>
		                        <p>Vendors</p>
		                    </a>
	                	</li>
					<?php endif;?>
					<?php  $result = has_permission(); 
					if( $result == '1'): ?>
					<li>
	                    <a href="frontui.php">
	                        <i class="material-icons text-gray">notifications</i>
	                        <p>Front UI</p>
	                    </a>
	                </li>
	                <?php endif;?>
					
	            </ul>
	    	</div>
	    </div>
	    
	    <div class="main-panel">
			<?php include'includes/navbar.php'; ?>


	        <div class="content list-card-xs">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="card">
	                            <div class="card-header text-center" data-background-color="purple">
	                                <h4 class="title">Add Product</h4>
									<p class="category">add full product details and related images with given sizes</p>
	                            </div>
	                            <section class="message"></section>
	                            <div class="card-content">
	                            		<div class="row">
	                                    	<div class="col-sm-12 text-center">
	                                    		<button class="btn btn-primary add-btn-slide">Add Product</button>
	                                    	</div>
	                                    </div>
	                                    <div class="row slidedown">
	                                    	<div class="col-md-12">
	                                    	<form id="addproductform" method="post" enctype="multipart/form-data">
		                                        <div class="row">
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">Title</label>
															<input type="text" class="form-control" name="title" id="title" >
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
			                                        	<div class="form-group label-floating">
															<select class="form-control" name="brand" id="brand">
															  	<option value="">Brand</option>
																<?php while($b = mysqli_fetch_assoc($brandquery)): ?>	
																<option value="<?=$b['id'];?>"> <?=$b['brand_name'];?></option>
																<?php endwhile; ?>
															</select>
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<select class="form-control" id="parent" name="parent">
																<option value="">Parent category</option>
																<?php while($p = mysqli_fetch_assoc($parentQuery)): ?>
																	<option value="<?=$p['id'];?>"><?=$p['category_name'] ;?></option>
																<?php endwhile; ?>
															</select>
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<div class="child-select">
																<select class="form-control" id="child-clone" name="child-clone">
																	<option value="">Child category</option>
																</select>
															</div>
														</div>
			                                        </div>
			                                    </div>
			                                    <div class="row">
			                                        <div class="col-sm-6 col-md-3 margin-t--1">
														<div class="form-group label-floating margin-t-10">
															<button class="btn btn-danger button-tab" onclick="jQuery('#sizesModal').modal('toggle');return false;">Quantity & Sizes</button>
														</div>
													</div>
													<div class="col-sm-6 col-md-3">
														<div class="form-group label-floating" id="float">
															<label class="control-label">Sizes & Qty Preview</label>
															<input type="text" class="form-control" name="sizes" id="sizes" readonly>
														</div>
			                                        </div>
			                                         <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating margin-t-10">
															<button class="btn btn-danger button-tab" onclick="jQuery('#colorModal').modal('toggle');return false;">Color code (#ff00ff)</button>
														</div>
													</div>
													<div class="col-sm-6 col-md-3">
														<div class="form-group label-floating " id="float1">
															<label class="control-label">Color code Prev</label>
															<input type="text" class="form-control" id="colors" name="colors" readonly>
														</div>
			                                        </div>
			                                    </div>
			                                    <div class="row">
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">Price</label>
															<input type="text" class="form-control" name="price" id="price" >
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">List Price</label>
															<input type="text" class="form-control" name="list-price" id="list-price">
														</div>
			                                        </div>
			                                        <div class="col-sm-12 col-md-6">
														<div class="form-group label-floating">
															<label class="control-label">Tagline</label>
															<input type="text" class="form-control" name="tagline" id="tagline">
														</div>
			                                        </div>
			                                    </div>
			                                    <div class="row">
			                                    	<div class="col-sm-12 col-md-12">
														<div class="form-group label-floating">
															<label class="control-label">Product full description & add features also</label>
															<textarea rows="5" class="form-control" name="description" id="description" ></textarea>
															<input type="text" name="addproduct" id="addproduct" class="hidden" value="0">
														</div>
			                                        </div>
			                                    </div>
			                                    <div class="row">
			                                    	<div class="col-sm-12 col-md-6 text-center margin-t-10-sm">
			                                    		<div class="upload-btn">
				                                    			<input type="file" multiple name="product-image[]" id="product-image" class="product-img">
				                                    			<label class="upload-label product-label">Product Images (868 x 1110)</label>
			                                    		</div>
			                                    	</div>
			                                    	<div class="col-sm-12 text-center">
				                                    	<hr class="btn-upper-hr">
				                                    	<button type="submit" class="btn btn-primary">Add</button>
				                                    </div>
			                                    </div>
			                                </form>
			                                </div>
	                                    </div>
	                            </div>
	                        </div>
	                    </div>
                        <div class="col-md-12">
	                    	<div class="col-sm-12 list-card-xs">
								<div class="card">
		                            <div class="card-header text-center" data-background-color="orange">
		                                <h4 class="title">Products List</h4>
		                                <p class="category">all products in database</p>
		                            </div>
		                            <div class="card-content table-responsive">

		                            	<?php if($_SESSION['SBvendorpermission'] == "admin,vendor"): ?>
				                                    		
		                                <table class="table table-hover table-stiped">
		                                    <thead class="text-warning">
		                                        <tr>
			                                        <th class="text-center">Id</th>
			                                        <th class="text-center">Vendor Id</th>
			                                    	<th class="text-center">Shop Name</th>
			                                    	<th class="text-center">Title</th>
			                                    	<th class="text-center">Price</th>
			                                    	<th class="text-center">Brand</th>
			                                    	<th class="text-center">Category</th>
			                                    	<th class="text-center">Image</th>
			                                    	<th class="text-center">Edit / Remove</th>
			                                    </tr>
		                                    </thead>
		                                    <tbody class="text-center" id="data-row">
		                                        <?php while($product = mysqli_fetch_assoc($productquery)): 
		                                        
		                                        $vendorid = $product['vendor_id'];
		                                        $shopnamequery = $db->query("SELECT * FROM vendors WHERE vendor_id = $vendorid");
		                                        $shopname = mysqli_fetch_assoc($shopnamequery);

		                                        $brandid = $product['brand_id'];
		                                        $brandname = $db->query("SELECT * FROM brands WHERE id = '$brandid' ");
		                                        $brand = mysqli_fetch_assoc($brandname);

		                                       	$childID = $product['category_id'];
												$result = $db->query("SELECT * FROM categories Where id = $childID");
												$child = mysqli_fetch_assoc($result);
												$parentID = $child['parent'];
												$presult = $db->query("SELECT * FROM categories WHERE id = '$parentID' ");
												$parent = mysqli_fetch_assoc($presult);
												$category = $parent['category_name'].'-'.$child['category_name'];


		                                        ?>
		                                        <tr class="tablerow" id="pro<?=$product['id'];?>">
		                                        	<td><?=$product['id'];?></td>
		                                        	<td><?=$product['vendor_id'];?></td>
		                                        	<td><?=$shopname['shop_name'];?></td>
		                                        	<td><?=$product['title'];?></td>
		                                        	<td><?=money($product['price']);?></td>
		                                        	<td><?=$brand['brand_name'];?></td>
		                                        	<td><?=$category;?></td>
		                                        	<?php $photos = explode(',',$product['product_image']);?>
		                                        	<td><img style="height: 70px; width: 70px; border-radius: 50%;" src="<?=$photos[0];?>" class="img-responsive"></td>
		                                        	<td>
		                                        	<button class="btn btn-xs btn-danger" onclick="productmodal(<?=$product['id'];?>);return false;"><i class="material-icons">create</i></span></i></button>
		                                        	<button class="btn btn-xs btn-danger" onclick="removeproduct(<?=$product['id'];?>);return false;"><i class="material-icons">delete_sweep</i></button>
		                                        	</td>
		                                        </tr>
		                                    	<?php endwhile; ?>
		                                    </tbody>
		                                </table>

		                                <?php else: ?>
										
										<table class="table table-hover table-stiped">
		                                    <thead class="text-warning">
		                                        <tr>
			                                        <th class="text-center">Id</th>
			                                        <th class="text-center">Vendor Id</th>
			                                    	<th class="text-center">Shop Name</th>
			                                    	<th class="text-center">Title</th>
			                                    	<th class="text-center">Price</th>
			                                    	<th class="text-center">Brand</th>
			                                    	<th class="text-center">Category</th>
			                                    	<th class="text-center">Image</th>
			                                    	<th class="text-center">Edit / Remove</th>
			                                    </tr>
		                                    </thead>
		                                    <tbody class="text-center" id="data-row">
		                                        <?php while($product = mysqli_fetch_assoc($venproductquery)): 
		                                        
	                                            $vendorid = $product['vendor_id'];
		                                        $shopnamequery = $db->query("SELECT * FROM vendors WHERE vendor_id = $vendorid");
		                                        $shopname = mysqli_fetch_assoc($shopnamequery);

		                                        $brandid = $product['brand_id'];
		                                        $brandname = $db->query("SELECT * FROM brands WHERE id = '$brandid' ");
		                                        $brand = mysqli_fetch_assoc($brandname);

		                                       	$childID = $product['category_id'];
												$result = $db->query("SELECT * FROM categories Where id = $childID");
												$child = mysqli_fetch_assoc($result);
												$parentID = $child['parent'];
												$presult = $db->query("SELECT * FROM categories WHERE id = '$parentID' ");
												$parent = mysqli_fetch_assoc($presult);
												$category = $parent['category_name'].'-'.$child['category_name'];


		                                        ?>
		                                        <tr class="tablerow" id="pro<?=$product['id'];?>">
		                                        	<td><?=$product['id'];?></td>
		                                        	<td><?=$product['vendor_id'];?></td>
		                                        	<td><?=$shopname['shop_name'];?></td>
		                                        	<td><?=$product['title'];?></td>
		                                        	<td><?=money($product['price']);?></td>
		                                        	<td><?=$brand['brand_name'];?></td>
		                                        	<td><?=$category;?></td>
		                                        	<?php $photos = explode(',',$product['product_image']);?>
		                                        	<td><img style="height: 70px; width: 70px; border-radius: 50%;" src="<?=$photos[0];?>" class="img-responsive"></td>
		                                        	<td>
		                                        	<button class="btn btn-xs btn-danger" onclick="productmodal(<?=$product['id'];?>);return false;"><i class="material-icons">create</i></span></i></button>
		                                        	<button class="btn btn-xs btn-danger" onclick="removeproduct(<?=$product['id'];?>);return false;"><i class="material-icons">delete_sweep</i></button>
		                                        	</td>
		                                        </tr>
		                                    	<?php endwhile; ?>
		                                    </tbody>
		                                </table>
		                               	
		                               	<?php endif; ?>		
		                            </div>
		                        </div>
							</div>
	                    </div>
	                    
					</div>
	            </div>
	        </div>

<?php include'includes/footer.php'; ?>
					<!-- modal -->
						<div class="modal fade" id="sizesModal" tabindex="-1" role="dialog" aria-labelledby="sizesModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-md">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="sizesModalLabel">Size & Quantity</h4>
									</div>
									<div class="modal-body size-modal-body">
										<div class="container-fluid">
										<?php for($i=1;$i<=8;$i++): ?>
											<div class="form-group col-md-2">
												<input type="text" id="size<?=$i;?>" class="form-control"  name="size<?=$i;?>" placeholder="Size <?=$i;?>.." >
											</div>
											<div class="form-group col-md-2">
												<input type="number" id="qty<?=$i;?>" name="qty<?=$i;?>" min="0" placeholder="Quantity.." class="form-control">
											</div>
											<div class="form-group col-md-2">
												<input type="number" id="threshold<?=$i;?>" name="threshold<?=$i;?>" placeholder="Threshold.." min="0" class="form-control">
											</div>
										<?php endfor; ?>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-raised btn-default" data-dismiss="modal" >Close</button>
										<button type="button" class="btn btn-raised btn-danger" onclick="updateSizes();jQuery('#sizesModal').modal('toggle');return false;">Save Changes</button>
									</div>
								</div>
							</div>
						</div>
						<!--color modal -->
						<div class="modal fade" id="colorModal" tabindex="-1" role="dialog" aria-labelledby="colorModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-md">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="sizesModalLabel">Color Code (#ff00ff)</h4>
									</div>
									<div class="modal-body size-modal-body">
										<div class="container-fluid">
										<?php for($i=1;$i<=8;$i++): ?>
											<div class="form-group col-md-3">
												<input type="text" id="color<?=$i;?>" class="form-control"  name="color<?=$i;?>" placeholder="Color code <?=$i;?>.." >
											</div>
										<?php endfor; ?>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-raised btn-default" data-dismiss="modal" >Close</button>
										<button type="button" class="btn btn-raised btn-danger" onclick="updateColor();jQuery('#colorModal').modal('toggle');return false;">Save Changes</button>
									</div>
								</div>
							</div>
						</div>


<script type="text/javascript">
	$('.add-btn-slide').click(function(){
		 $(".slidedown").slideToggle();
	});
</script>
<script type="text/javascript">

		function updateSizes(){
			$('#float').addClass("is-focused");
			var sizeString = '';
			for (var i = 1 ; i <= 8; i++) {
			 	if (jQuery('#size'+i).val()!= '') {
			 		sizeString += jQuery('#size'+i).val()+':'+jQuery('#qty'+i).val()+':'+jQuery('#threshold'+i).val()+',';
			 	}
			 } 
			 jQuery('#sizes').val(sizeString);
		}

		function updateColor(){
			$('#float1').addClass("is-focused");
			var sizeString = '';
			for (var i = 1 ; i <= 8; i++) {
			 	if (jQuery('#color'+i).val()!= '') {
			 		sizeString += jQuery('#color'+i).val()+',';
			 	}
			 } 
			 jQuery('#colors').val(sizeString);
		}

		function get_child(parentid){
			var parameters = {"parentid": parentid};
			jQuery.ajax({
				url : '/multivendor/dashboard/parser/getchild.php',
				method : 'POST',
				data : parameters,
				async: true,
				cache: false,
				success : function(data){ 
					$("#child-clone").fadeOut("slow", function() { $(this).remove(); });
			        $(".child-select").html(data).fadeIn('slow');//we refer to our div by its class

				},
				error:function(data){
			    	alert("something went wrong");
			    }
			});
		}	

		
		$('#parent').on('change', function() {
			parentid = $(this).val();
			get_child(parentid);
		});

	$("form#addproductform").submit(function(e){
		 	e.preventDefault();
		 		$('input[name=addproduct]').attr('value','1');
			    var formData = new FormData(this);
				
    	 	jQuery.ajax({
				url : '/multivendor/dashboard/parser/ajax.php',
				method : 'POST',
				data : formData,
				async: true,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data){ 
						// alert(data);
						var result = JSON.parse(data);

						if ( result.status == "success" ) {
							$('.slidedown').slideUp();
							$("form#addproductform")[0].reset();
				        	$(result.data).prependTo("#data-row");
				           	// location.reload();
				           		//alert("insert");
				        
				        }else if ( result.status == "fail" ){
							$('.message').html(result.data);
						}else{
							alert("Something went wrong with database");
						}		
						//alert-close
		                $('#alert-close').click(function(){
							$('.message').html(" ");
						});
				 },
				
				error : function(){ alert("something went wrong");}   
			});  
    });

  		function productmodal(product_id){
  			var data ={"id" : product_id};
			jQuery.ajax({
				async: true,
				url :'/multivendor/dashboard/parser/productmodal.php',
				method : "post",
				data : data,
				
				success:function(data){
					jQuery('body').append(data);
					jQuery('#details-modal').modal('toggle');
				 },
				error:function(){
					alert("something went wrong");
				}
			});
  		}

		function removeproduct(product_id){
			var removeproduct = '';
		    jQuery.ajax({
					url : '/multivendor/dashboard/parser/ajax.php',
					method : 'POST',
					data : {product_id: product_id, removeproduct : 1},
		    		success : function(data){
					
					// alert(data); 
					//  var result = data;
					 var result = JSON.parse(data);
					 	if( result.status == "success"){
					 	    $("#pro"+ result.product_id).fadeOut("slow", function() { $(this).remove(); });
					 	}else{
					 		alert("something went row");
					 	}
					 },
					// error : function(){ alert("something went wrong");}   
				}); 
		}

</script>