<?php 
require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';
$result = is_logged_in();
if ($result == '0' ) {
	login_error_redirect();
}


include'includes/header.php'; 

$categoryquery = $db->query("SELECT * FROM categories ORDER BY id DESC");
$parentresult = $db->query("SELECT * FROM categories WHERE parent ='parent'");

?>

		<ul class="nav" id="nav">
	                <li>
	                    <a href="dashboard.php">
	                        <i class="material-icons">dashboard</i>
	                        <p>Dashboard</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="brands.php">
	                        <i class="material-icons">content_paste</i>
	                        <p>Brands</p>
	                    </a>
	                </li>
	                <li class="active">
	                    <a href="categories.php">
	                        <i class="material-icons">library_books</i>
	                        <p>Categories</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="products.php">
	                        <i class="material-icons">bubble_chart</i>
	                        <p>Products</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="archieve.php">
	                        <i class="material-icons">location_on</i>
	                        <p>Archieve</p>
	                    </a>
	                </li>
   	                <?php  $result = has_permission(); 
					if( $result == '1'): ?>
						<li>
		                    <a href="vendors.php">
		                        <i class="material-icons text-gray">notifications</i>
		                        <p>Vendors</p>
		                    </a>
	                	</li>
					<?php endif;?>

	                <?php  $result = has_permission(); 
					if( $result == '1'): ?>
					<li>
	                    <a href="frontui.php">
	                        <i class="material-icons text-gray">notifications</i>
	                        <p>Front UI</p>
	                    </a>
	                </li>
	                <?php endif;?>
					
	            </ul>
	    	</div>
	    </div>
	    
	    <div class="main-panel">
			<?php include'includes/navbar.php'; ?>
	        <div class="content list-card-xs">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="card">
	                            <div class="card-header text-center" data-background-color="purple">
	                                <h4 class="title">Add Category</h4>
									<p class="category">enter category name and select image of category</p>
	                            </div>
	                            <section class="message"></section>
	                            <div class="card-content">
	                                    <div class="row">
	                                    	<div class="col-md-12 text-center">
			                                    <button type="submit" class="btn btn-primary category-btn">Add Category</button>
			                                    <div class="clearfix"></div>
		                                    </div>
       	                                    <form id="categoryform" method="post" enctype="multipart/form-data">
		                                    <div class="row category-form">
		                                    	<div class="col-sm-12 col-md-12">
		                                    		<div class="col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 ">
														<div class="form-group label-floating">
															<div class="select">
																<select class="form-control" name="parent_category" id="parent_category">
																	<option value="parent">Parent</option>
																	<?php while ($parent = mysqli_fetch_assoc($parentresult)) : ?>
																	<option id="<?=$parent['id'];?>" value="<?=$parent['id'];?>"><?=$parent['category_name']; ?>
																	</option>	
																	<?php endwhile; ?>
																</select>
															</div>
														</div>
													</div>
		                                        </div>
		                                    	<div class="col-sm-12 col-md-12">
		                                    		<div class="col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 ">
														<div class="form-group label-floating">
															<label class="control-label">Enter Category</label>
															<input type="text" class="form-control" name="category_name" id="category_name">
							                                <input type="text" name="addcategory" value="0" id="addcategory" class="hidden">
														</div>
													</div>
		                                        </div>
		                                        <div class="col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 text-center margin-t-10-sm">
			                                    	<div class="upload-btn text-center">
				                                   			<input type="file" multiple name="category_image" id="category_image" class="product-img">
				                                   			<label class="upload-label-category">Category Images (520 x 380)</label>
			                                    	</div>
			                                    </div>
		                                        <div class="col-sm-12 col-md-12 text-center">
		                                        	<hr class="btn-upper-hr-category">
				                                    <button type="submit" class="btn btn-primary">Add</button>
				                                </div>
	                                        </div>
	                                       	</form>
										</div>                                   
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-md-12">
	                    	<div class="col-sm-12 list-card-xs">
								<div class="card">
		                            <div class="card-header text-center" data-background-color="orange">
		                                <h4 class="title">Category List</h4>
		                                <p class="category">all category in database</p>
		                            </div>
		                            <div class="card-content table-responsive">
		                                <table class="table table-hover table-stiped">
		                                    <thead class="text-warning">
		                                        <tr>
			                                        <th class="text-center">Id</th>
			                                    	<th class="text-center">Category Name</th>
			                                    	<th class="text-center">Parent</th>
			                                    	<th class="text-center">Category Image</th>
			                                    	<th class="text-center">Edit / Remove</th>
			                                    </tr>
		                                    </thead>
		                                    <tbody class="text-center" id="data-row">
													<?php 
													$sql = "SELECT * FROM categories WHERE parent ='parent' ";
													$result = $db->query($sql);

													while ($parent =mysqli_fetch_assoc($result) ): 
													
													$parent_id = (int)$parent['id'];
													$sql2 = "SELECT * FROM categories WHERE parent = '$parent_id' ";
													$cresult = $db->query($sql2); 
													?>
														<tr class="tablerow tr-parent" id="table<?=$parent['id'];?>">
															<td><?=$parent['id'];?></td>
				                                        	<td><?=$parent['category_name'];?></td>
				                                        	<td><?=$parent['parent'];?></td>
				                                        	<td><img style="height: 70px; width: 70px;" class="profile_image" src="<?=$parent['image'];?>"></td>
				                                        	<td>
				                                        	<button class="btn btn-xs btn-danger" onclick="categorymodal(<?=$parent['id'];?>)"><i class="material-icons">create</i></button>
					                                        <button class="btn btn-xs btn-danger" onclick="removecategory(<?=$parent['id'];?>)"><i class="material-icons">delete_sweep</i></button>
					                                        </td>
														</tr>
														<?php while($child = mysqli_fetch_assoc($cresult)): ?>
														<tr class="tablerow tr-child" id="table<?=$child['id'];?>">
															<td><?=$child['id'];?></td>
				                                        	<td><?=$child['category_name'];?></td>
				                                        	<td><?=$parent['category_name'];?></td>
				                                        	<td><img style="height: 70px; width: 70px;" class="profile_image" src="<?=$child['image'];?>"></td>
				                                        	<td>
				                                        	<button class="btn btn-xs btn-danger" onclick="categorymodal(<?=$child['id'];?>)"><i class="material-icons">create</i></button>
					                                        <button class="btn btn-xs btn-danger" onclick="removecategory(<?=$child['id'];?>)"><i class="material-icons">delete_sweep</i></button>
					                                        </td>
														</tr>
														<?php endwhile; ?>
													<?php endwhile; ?>
													</tbody>

		                                    </tbody>
		                                </table>
		                            </div>
		                        </div>
							</div>
	                    </div>
	                </div>
	            </div>
	        </div>

<?php include'includes/footer.php'; ?>

<script type="text/javascript">
	$('.category-btn').click(function(){
		$('.category-form').slideToggle();
	});
</script>
<script type="text/javascript">

	$("form#categoryform").submit(function(e){
		 e.preventDefault();

		 		$('input[name=addcategory]').attr('value','1');
			    var formData = new FormData(this);
				
    	 	jQuery.ajax({
				url : '/multivendor/dashboard/parser/ajax.php',
				method : 'POST',
				data : formData,
				async: true,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data){ 

//						alert(data);
						var result = JSON.parse(data);

							if ( result.status == "success" ) {
								$('.category-form').slideUp();
								$("form#categoryform")[0].reset();
								
								if(result.parent_id == 'parent'){
						        	$(result.data).prependTo("#data-row");
						        	$(result.option).insertAfter("option[value=parent]");
					           	}else{
					           		$(result.data).insertAfter("#table"+ result.parent_id);
					           	}
						    }else if ( result.status == "fail" ){
								$('.message').html(result.data);
							}else{
								alert("Something went wrong with database");
							}		
		                        	//alert-close
		                $('#alert-close').click(function(){
							$('.message').html(" ");
						});
				},
				error : function(){ alert("something went wrong");}   
			});  
    	 });
  		

  		function categorymodal(id){
  			var data ={"id" : id};
			jQuery.ajax({
				async: true,
				url :'/multivendor/dashboard/parser/categorymodal.php',
				method : "post",
				data : data,
				
				success:function(data){
					jQuery('body').append(data);
					jQuery('#details-modal').modal('toggle');
				 },
				error:function(){
					alert("something went wrong");
				}
			});
  		}
		function removecategory(cat_id){
			var removecategory = '';
		    jQuery.ajax({
					url : '/multivendor/dashboard/parser/ajax.php',
					method : 'POST',
					data : {cat_id: cat_id, removecategory : 1},
		    		success : function(data){
					//  alert(data); 
					//  var result = data;
					var result = JSON.parse(data);
						if ( result.status == "parentdelete" ) {
							//alert(result.count);	
							location.reload(true);
						}else if( result.status == "childdelete"){
						    $("#table"+ cat_id).fadeOut("slow", function() { $(this).remove(); });
						}else{
							alert("something went row");
						}
					},
					error : function(){ alert("something went wrong");}   
				}); 
		}
	
</script>

