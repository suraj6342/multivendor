<?php  

require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';

$id = $_POST['id'];
$id1 = (int)$id;

$sql = "SELECT * FROM products WHERE id = '$id1'";
$result = $db->query($sql);
$product = mysqli_fetch_assoc($result);


$brandquery =$db->query("SELECT * FROM brands ORDER BY brand_name");

$parentQuery = $db->query("SELECT * FROM categories WHERE parent = 'parent' ORDER BY category_name");


$category_id = $product['category_id'];

$childselect = $db->query("SELECT * FROM categories WHERE id = '$category_id'");
$childdata = mysqli_fetch_assoc($childselect);
$parentid = $childdata['parent'];
$childname = $childdata['category_name'];

?>


<?php ob_start();?>
<style type="text/css">
	.form-group{
		margin: 0px 0 0 0;
	}
	.form-group label.control-label{
		    margin: 5px 0 0 0;
	}
</style>
<div class="modal fade detail-1" id="details-modal" tabindex="-1" role="dialog"> 
	<div class="modal-dialog modal-lg edit-modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button class="btn btn-default edit-modal-close" type="button" onclick="closeModal()" aria-label="Close">
				<span area-hidden="true">&times;</span>
				</button>
				<div class="container-fluid">
					<section class="edit-message"></section>
					<form id="producteditform" method="post" enctype="multipart/form-data">
												<div class="row margin-t-40">
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">Title</label>
															<input type="text" class="form-control" name="edit_p_title" id="edit_p_title" value="<?=$product['title'];?>" >
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
			                                        	<div class="form-group label-floating">
															<select class="form-control" name="edit_p_brand" id="edit_p_brand">
															  	<option value="">Brand</option>
																<?php while($b = mysqli_fetch_assoc($brandquery)): ?>	
																<option value="<?=$b['id'];?>" <?=(($product['brand_id'] == $b['id'])?' selected':'');?>> <?=$b['brand_name'];?></option>
																<?php endwhile; ?>
															</select>
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<select class="form-control" id="editparent" name="editparent">
																<option value="">Parent category</option>
																<?php while($p = mysqli_fetch_assoc($parentQuery)): ?>
																	<option value="<?=$p['id'];?>"  <?=(($p['id'] == $parentid)?' selected':'');?>><?=$p['category_name'] ;?></option>
																<?php endwhile; ?>
															</select>
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<div class="edit_child-select">
																<select class="form-control" id="editchild" name="editchild">
																	<option value="<?=$product['category_id'];?>"><?=$childname;?></option>
																</select>
															</div>
														</div>
			                                        </div>
			                                    </div>
			                                    <div class="row margin-t-25">
			                                        <div class="col-sm-6 col-md-3 margin-t--1">
														<div class="form-group label-floating margin-t-10 pad-b-0">
															<button class="btn btn-danger button-tab margin-tb-0" onclick="jQuery('#editsizesModal').modal('toggle');return false;">Update size</button>
														</div>
													</div>
													<div class="col-sm-6 col-md-3 margin-t-40-xs">
														<div class="form-group label-floating" id="float">
															<label class="control-label siz-col-lab-mar-t">Sizes & Qty Preview</label>
															<input type="text" value="<?=$product['sizes'];?>" class="form-control" name="edit_p_sizes" id="edit_p_sizes" readonly>
														</div>
			                                        </div>
			                                         <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating margin-t-10 pad-b-0">
															<button class="btn btn-danger button-tab margin-tb-0" onclick="jQuery('#editcolorModal').modal('toggle');return false;">Update color</button>
														</div>
													</div>
													<div class="col-sm-6 col-md-3 margin-t-40-xs">
														<div class="form-group label-floating " id="float1">
															<label class="control-label siz-col-lab-mar-t">Color code Prev</label>
															<input type="text" class="form-control" id="edit_p_colors" value="<?=$product['color'];?>" name="edit_p_colors" readonly>
														</div>
			                                        </div>
			                                    </div>
			                                    <div class="row margin-t-25">
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">Price</label>
															<input type="text" class="form-control" value="<?=$product['price'];?>" name="edit_p_price" id="edit_p_price" >
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3 margin-t-40-xs">
														<div class="form-group label-floating">
															<label class="control-label">List Price</label>
															<input type="text" class="form-control" value="<?=$product['list_price'];?>" name="edit_p_list-price" id="edit_p_list-price">
														</div>
			                                        </div>
			                                        <div class="col-sm-12 col-md-6 siz-col-lab-mar-t margin-t-40-xs">
														<div class="form-group label-floating">
															<label class="control-label">Tagline</label>
															<input type="text" class="form-control" value="<?=$product['tag_line'];?>" name="edit_p_tagline" id="edit_p_tagline">
														</div>
			                                        </div>
			                                    </div>
			                                    <div class="row margin-t-25">
			                                    	<div class="col-sm-12 col-md-12">
														<div class="form-group label-floating">
															<label class="control-label">Product full description & add features also</label>
															<textarea rows="4" class="form-control" name="edit_p_description" id="edit_p_description" ><?=$product['description'];?></textarea>
															<input type="text" name="editproduct" id="editproduct" class="hidden" value="0">
															<input type="text" name="edit_p_product_id" id="edit_p_product_id" value="<?=$product['id'];?>" class="hidden">
														</div>
			                                        </div>
			                                    </div>
			                                    <div class="row margin-t-25">
			                                    	<div class="product-picture">
				                                    	<div class="col-sm-12 col-md-12 text-center product-picture-inner">
				                                    		<?php if($product['product_image'] != ''): ?>
				                                    			<?php 
				                                    			$imgi = 1;
				                                    			$images = explode(',', $product['product_image']);
																
																foreach($images as $image): ?>
																<div class="product-edit-img col-xs-4 col-sm-3 col-md-3 text-center" id="product_image<?=$imgi;?>">
																	<img class="img-responsive" style="margin: auto; height: 80px;border-radius: 100%;" src="<?=$image;?>"  ><br> 
																	<button class="btn btn-danger button-tab margin-tb-0 delete-btn-xs" onclick="deleteimage(<?=$imgi;?>,<?=$product['id'];?>);return false;"><i class="material-icons">delete_sweep</i></button>
																</div>
																<?php $imgi++; 
																endforeach; ?>
															<?php else: ?>
															<div class="blank_picture"></div>
															<?php endif; ?>	
				                                    	</div>
			                                    	</div>
			                                    	<div class="upload-btn col-xs-12 col-sm-12 col-md-6 col-md-offset-3 margin-t-25">
				                                    	<input type="file" multiple name="edit_product_image[]" id="edit_product_image" class="product-img edit-product-image">
				                                    	<input type="text" name="newImageString" id="newImageString" value="<?=$product['product_image'];?>" class="hidden">
				                                    	<label class="upload-label product-label edit-product-label">Insert More Images (868 x 1110)</label>
			                                    	</div>
			                                    	
			                                    </div>
			                                   
			                                								
							
							<div class="row">
								<div class="col-sm-12 col-md-12 text-center">
				                    <hr class="btn-upper-hr margin-t-25">
					                <button class="form_submit-btn btn btn-primary" id="form_submit">Edit</button>
					                <button class="btn btn-default" onclick="closeModal();return false;"> Close </button>
								</div>
							</div>						
				                                    		
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- modal -->
						<div class="modal fade" id="editsizesModal" tabindex="-1" role="dialog" aria-labelledby="editsizesModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-md">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="editsizesModalLabel">Size & Quantity</h4>
									</div>
									<div class="modal-body size-modal-body">
										<div class="container-fluid">
										<?php for($i=1;$i<=8;$i++): ?>
											<div class="form-group col-md-2">
												<input type="text" id="editsize<?=$i;?>" class="form-control"  name="editsize<?=$i;?>" placeholder="Size <?=$i;?>.." >
											</div>
											<div class="form-group col-md-2">
												<input type="number" id="editqty<?=$i;?>" name="editqty<?=$i;?>" min="0" placeholder="Quantity.." class="form-control">
											</div>
											<div class="form-group col-md-2">
												<input type="number" id="editthreshold<?=$i;?>" name="editthreshold<?=$i;?>" placeholder="Threshold.." min="0" class="form-control">
											</div>
										<?php endfor; ?>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-raised btn-default" data-dismiss="modal" >Close</button>
										<button type="button" class="btn btn-raised btn-danger" onclick="editupdateSizes();jQuery('#editsizesModal').modal('toggle');return false;">Save Changes</button>
									</div>
								</div>
							</div>
						</div>
						<!--color modal -->
						<div class="modal fade" id="editcolorModal" tabindex="-1" role="dialog" aria-labelledby="editcolorModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-md">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="sizesModalLabel">Color Code (#ff00ff)</h4>
									</div>
									<div class="modal-body size-modal-body">
										<div class="container-fluid">
										<?php for($i=1;$i<=8;$i++): ?>
											<div class="form-group col-md-3">
												<input type="text" id="editcolor<?=$i;?>" class="form-control"  name="editcolor<?=$i;?>" placeholder="Color code <?=$i;?>.." >
											</div>
										<?php endfor; ?>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-raised btn-default" data-dismiss="modal" >Close</button>
										<button type="button" class="btn btn-raised btn-danger" onclick="editupdateColor();jQuery('#editcolorModal').modal('toggle');return false;">Save Changes</button>
									</div>
								</div>
							</div>
						</div>


<script type="text/javascript">
		
		function get_edit_child(parentid){
			var parameters = {"parentid": parentid};
			jQuery.ajax({
				url : '/multivendor/dashboard/parser/geteditchild.php',
				method : 'POST',
				data : parameters,
				async: true,
				cache: false,
				success : function(data){ 
					$("#edit_child-clone").fadeOut("slow", function() { $(this).remove(); });
			        $(".edit_child-select").html(data);//we refer to our div by its class

				},
				error:function(data){
			    	alert("something went wrong");
			    }
			});
		}	
		
		$('#editparent').on('change', function() {
			parentid = $(this).val();
			get_edit_child(parentid);
		});

</script>


<script type="text/javascript">
	
	function editupdateSizes(){
		$('#float').addClass("is-focused");
		var sizeString = '';
		for (var i = 1 ; i <= 8; i++) {
		 	if (jQuery('#editsize'+i).val()!= '') {
		 		sizeString += jQuery('#editsize'+i).val()+':'+jQuery('#editqty'+i).val()+':'+jQuery('#editthreshold'+i).val()+',';
		 	}
		 } 
		 jQuery('#edit_p_sizes').val(sizeString);
	}
	function editupdateColor(){
		$('#float1').addClass("is-focused");
		var sizeString = '';
		for (var i = 1 ; i <= 8; i++) {
		 	if (jQuery('#editcolor'+i).val()!= '') {
		 		sizeString += jQuery('#editcolor'+i).val()+',';
		 	}
		 } 
		 jQuery('#edit_p_colors').val(sizeString);
	}

	function deleteimage(imageno, product_id){
			var parameters = {"imgi": imageno, "product_id": product_id};
			jQuery.ajax({
				url : '/multivendor/dashboard/parser/deleteproductimage.php',
				method : 'POST',
				data : parameters,
				async: true,
				cache: false,
				success : function(data){ 
					var result = JSON.parse(data);
					if ( result.status == "success" ) {
					     	 $("#product_image"+ imageno).remove();
					     	 $('input[name=newImageString]').attr('value',result.newImageString);
			
				  	}else{
				  		alert("something went wrong with ajax");
				  	}
				},
				error:function(data){
			    	alert("something went wrong");
			    }
			});
	}

	function closeModal() {
			$('#details-modal').modal('hide');
			 setTimeout(function(){
			 	$("form#producteditform").remove();
			 	jQuery("#details-modal").remove();
				$(".modal-backdrop").removeClass("in");
			 	$(".modal-backdrop").each(function(){
			 		$(this).addClass("out");
			 	});
			 	$(".modal-backdrop").each(function(){
			 		$(this).addClass("hidden");
			 	});
		 	
		// },500)
	});
}

</script>
<script type="text/javascript">
		
		$("form#producteditform").submit(function(e){
			e.preventDefault();
			$('input[name=editproduct]').attr('value','1');
			var editformData = new FormData(this);
			jQuery.ajax({
				url : '/multivendor/dashboard/parser/ajax.php',
				method : 'POST',
				data : editformData,
				async: true,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data){ 
						// alert(data);
						var result = JSON.parse(data);

						if ( result.status == "success" ) {
				        	var element = document.getElementById("pro"+ result.id);
				        	element.remove();
				        	$(result.data).prependTo("#data-row");
				        	closeModal();
				        }else if ( result.status == "fail" ){
						
							$('.edit-message').html(result.data);
						}else{
							alert("Something went wrong with database");
						}		
		                     
		                    //alert-close
		                    $('#alert-close').click(function(){
								$('.edit-message').html(" ");
							});    	
				 },
				
				error : function(){ alert("something went wrong");}   
			});  
    	 });
   
</script>
<?php echo ob_get_clean();?>