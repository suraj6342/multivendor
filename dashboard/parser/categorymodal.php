<?php  

require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';

$id = $_POST['id'];
$id1 = (int)$id;

$sql = "SELECT * FROM categories WHERE id = '$id1'";
$result = $db->query($sql);
$category = mysqli_fetch_assoc($result);


$parent = $db->query("SELECT * FROM categories WHERE parent = 'parent' ");

?>


<?php ob_start();?>
<style type="text/css">
	.form-group{
		margin: 0px 0 0 0;
	}
	.form-group label.control-label{
		    margin: 5px 0 0 0;
	}
</style>
<div class="modal fade detail-1" id="details-modal" tabindex="-1" role="dialog"> 
	<div class="modal-dialog modal-md edit-modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<button class="btn btn-default edit-modal-close" type="button" onclick="closeModal()" aria-label="Close">
			<span area-hidden="true">&times;</span>
			</button>
			<div class="container-fluid">
				<section class="edit-message"></section>
				<form id="categoryeditform" method="post" enctype="multipart/form-data">
		                                    <div class="row">
		                                        <div class="col-sm-6 col-md-10 col-sm-offset-3 col-md-offset-1 text-center">
		                                        	<img style="height: 140px; width: 140px; margin-bottom: 10px; " class="profile_image" src="<?=$category['image'];?>"> 
		                                        </div>
		                                    	<div class="col-sm-12 col-md-12">
		                                    		<div class="col-sm-12 col-md-10 col-md-offset-1">
														<div class="form-group">
															<select class="form-control" name="parent_category" id="parent_category">
																<option value="parent">Parent</option>
																<?php while ($parentresult = mysqli_fetch_assoc($parent)) : ?>
																<option value="<?=$parentresult['id'];?>" <?=(($parentresult['id'] == $category['parent'])?' selected="selected"':'');?> ><?=$parentresult['category_name']; ?>
																</option>	
																<?php endwhile; ?>
															</select>
														</div>
													</div>
		                                        </div>
		                                    	<div class="col-sm-12 col-md-12">
		                                    		<div class="col-sm-12 col-md-10 col-md-offset-1">
														<div class="form-group">
															<label class="control-label">Enter Category</label>
															<input type="text" class="form-control" name="category_name" value="<?=$category['category_name'];?>" id="category_name">
							                                <input type="text" name="editcategory" value="0" id="editcategory" class="hidden">
							                                <input type="text" name="editcatid" value="<?=$category['id'];?>" id="editcatid" class="hidden">
							                                
														</div>
													</div>
		                                        </div>
		                                        <div class="col-sm-12 col-md-10 col-md-offset-1 text-center margin-t-10-sm">
			                                    	<div class="upload-btn text-center">
				                                   			<input type="file" multiple name="category_image" id="category_image" class="product-img ">
				                                   			<label class="upload-label-category edit-cat-upload-label">Update Images (520 x 380)</label>
			                                    	</div>
			                                    </div>
		                                        <div class="col-sm-12 col-md-12 text-center">
		                                        	<hr class="btn-upper-hr-category">
				                                    <button type="submit" class="btn btn-primary">Edit</button>
				                                    <button class="btn btn-default" onclick="closeModal();return false;"> Close </button>
				                                </div>
	                                        </div>
				</form>
			</div>
		</div>
		
	</div>
	</div>
</div>
<script type="text/javascript">

	function closeModal() {
			$('#details-modal').modal('hide');
			 setTimeout(function(){
			 	$("form#brandeditform").remove();
			 	jQuery("#details-modal").remove();
				$(".modal-backdrop").removeClass("in");
			 	$(".modal-backdrop").each(function(){
			 		$(this).addClass("out");
			 	});
			 	$(".modal-backdrop").each(function(){
			 		$(this).addClass("hidden");
			 	});
		 	
		// },500)
	});
}

</script>
<script type="text/javascript">
		$(document).ready(function(){
		    load_child();//loading the document updates it
		});

		function load_child(){
		var parameters = {"action":"ajax"};
		$.ajax({
		    url:'parser/getselect.php', //we call our php file that calls the data
		    type: "POST",
		    data: parameters,
		    beforeSend: function(object){
		    },
		    success:function(data){
		        $(".select").html(data).fadeIn('slow');//we refer to our div by its class
		    }
		});
		}


		$("form#categoryeditform").submit(function(e){
			e.preventDefault();
			$('input[name=editcategory]').attr('value','1');
			var editformData = new FormData(this);
			jQuery.ajax({
				url : '/multivendor/dashboard/parser/ajax.php',
				method : 'POST',
				data : editformData,
				async: true,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data){ 
						// alert(data);
						var result = JSON.parse(data);

						if ( result.status == "success" && result.parent != "parent" ) {
				        	closeModal();
							var element = document.getElementById("table"+ result.id);
				        	element.remove();
				        	$(result.data).insertAfter("#table"+ result.parent);
				        	 load_select();
				        }else if(result.status == "success" && result.parent == "parent"){
				        	closeModal();
							var table = document.getElementById("table"+ result.id);
				        	table.remove();
				        	$(result.data).prependTo("#data-row");
				        	 load_select();
				        	//alert(element)

				        }else if ( result.status == "fail" ){
						
							$('.edit-message').html(result.data);
						}else{
							alert("Something went wrong with database");
						}		
		                    //alert-close
		                    $('#alert-close').click(function(){
								$('.edit-message').html(" ");
							});    	
				 },
				
				error : function(){ alert("something went wrong");}   
			});  
    	 });
   
</script>
<?php echo ob_get_clean();?>