<?php	

require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';


if (!empty($_POST['add_ven']) && empty($_POST['edit_ven'])) {
//vendor ajax
$name = isset($_POST['ven_name'])?sanitize($_POST['ven_name']):'';
$email = isset($_POST['ven_email'])?sanitize($_POST['ven_email']):'';
$pass = isset($_POST['ven_pass'])?sanitize($_POST['ven_pass']):'';
$repass = isset($_POST['ven_repass'])?sanitize($_POST['ven_repass']):'';
$mobile = isset($_POST['ven_mobile'])?sanitize($_POST['ven_mobile']):'';
$phone = isset($_POST['ven_phone'])?sanitize($_POST['ven_phone']):'';
$address = isset($_POST['ven_address'])?sanitize($_POST['ven_address']):'';
$shop_name = isset($_POST['ven_shop_name'])?sanitize($_POST['ven_shop_name']):'';
$shop_phone = isset($_POST['ven_shop_phone'])?sanitize($_POST['ven_shop_phone']):'';
$shop_add = isset($_POST['ven_shop_address'])?sanitize($_POST['ven_shop_address']):'';
$permissions = isset($_POST['ven_perm'])?sanitize($_POST['ven_perm']):'';
$gst_no = isset($_POST['ven_gst_no'])?sanitize($_POST['ven_gst_no']):'';



//var_dump($ven_img);
$errors = array();
$login_count = 0;	

				if(empty($name) || empty($email) || empty($pass) || empty($repass) || empty($mobile) || empty($phone) || empty($address) || empty($shop_name) || empty($shop_phone) || empty($shop_add) || empty($permissions) || empty($gst_no) ){
					$errors[] = 'All fields are required...';
				}
				//VALIDATE EMAIL

				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$errors[] = 'You must enter a valid email address'; 
				}

// 				//password is more than 6 charackter
				if (strlen($pass) < 6 ) {
					$errors[] = 'Password must be atleast 6 character';
				}

				if ($pass != $repass) {
					$errors[] = 'password and repassword doesnt match';	
				}

// 				//check if vendor exist in database

				$query = $db->query("SELECT * FROM vendors WHERE email = '$email'");
				$user = mysqli_fetch_assoc($query);
				$usercount = mysqli_num_rows($query);
				//echo $usercount;
				if ($usercount > 0){

					$errors[] = 'That email exist in our database'; 
				}
						
					$ven_img =  isset($_FILES['ven_img']['name'])?($_FILES['ven_img']['name']):'';
					//echo $name;
					//echo($name);
					$namearray = explode('.', $ven_img);
					$fileName = isset($namearray[0]) ? $namearray[0] : '';
					$fileExt = 	isset($namearray[1]) ? $namearray[1] : '';
					// echo ",".$fileName;
					// echo ",".$fileExt;
					$type = isset($_FILES['ven_img']['type'])?($_FILES['ven_img']['type']):'';
					$mime = explode('/',$type);
					$mimeType = isset($mime[0]) ? $mime[0] : '';
					$mimeExt = isset($mime[1]) ? $mime[1] : '';
					$tmpLoc = isset($_FILES['ven_img']['tmp_name'])?($_FILES['ven_img']['tmp_name']):'';
					$fileSize = isset($_FILES['ven_img']['size'])?($_FILES['ven_img']['size']):'';
					$uploadName = md5(microtime()).'.'.$fileExt;
					$uploadPath =  BASEURL.'/multivendor/img/vendorprofile/'.$uploadName;
					
					$dbpath = '/multivendor/img/vendorprofile/'.$uploadName;

					if ($mimeType != 'image') {
						$errors[] = 'The file must be an image';
					}
					$allowed = array('png','jpg','jpeg','gif');
			
					if (!in_array($fileExt, $allowed)) {
						$errors[] = 'The file extension must be a png, jpg, jgep, or gif';
					}
					if ($fileSize > '8388608') {
						$errors[] = 'The File size must be below 8Mb';
					}

// 				//check for errors
				if(!empty($errors)){
					$mes = wrongmessage($errors);
					echo json_encode( array( 'status' => 'fail', 'data' => $mes ) );
					
				}
 				else{
// 					//start session  user to db
// 							$user_id = $user['idate(format)'];
// 							login($user_id);
							
// 							$login_count = 1;							
// 							echo $login_count;
						move_uploaded_file($tmpLoc, $uploadPath);

						$hashed = password_hash($pass, PASSWORD_DEFAULT);
							
							$db->query("INSERT INTO vendors (`full_name`,`email`,`password`,`mobile`,`phone`,`address`,`shop_name`,`shop_phone`,`shop_address`,`gst_no`,`permissions`,`profile_image`) VALUES ('$name','$email','$hashed','$mobile','$phone','$address','$shop_name','$shop_phone','$shop_add','$gst_no','$permissions','$dbpath')");
							$table_row = get_table_row($email);
							echo json_encode( array( 'status' => 'success','data' => $table_row ) );
						
 				 }
}

if (!empty($_POST['addbrand'])) {

$brandname = isset($_POST['brand_name'])?sanitize($_POST['brand_name']):'';
$errors = array();
			
			if(empty($brandname)){
				$errors[] = 'All fields are required...';
			}
 				//check if vendor exist in database

				$query = $db->query("SELECT * FROM brands WHERE brand_name = '$brandname'");
				$brand = mysqli_fetch_assoc($query);
				$brandcount = mysqli_num_rows($query);
				//echo $usercount;
				if ($brandcount > 0){

					$errors[] = 'That brand exist in our database'; 
				}

						
					$brand_img =  isset($_FILES['brand_image']['name'])?($_FILES['brand_image']['name']):'';
					//echo $name;
					//echo($name);
					$namearray = explode('.', $brand_img);
					$fileName = isset($namearray[0]) ? $namearray[0] : '';
					$fileExt = 	isset($namearray[1]) ? $namearray[1] : '';
					// echo ",".$fileName;
					// echo ",".$fileExt;
					$type = isset($_FILES['brand_image']['type'])?($_FILES['brand_image']['type']):'';
					$mime = explode('/',$type);
					$mimeType = isset($mime[0]) ? $mime[0] : '';
					$mimeExt = isset($mime[1]) ? $mime[1] : '';
					$tmpLoc = isset($_FILES['brand_image']['tmp_name'])?($_FILES['brand_image']['tmp_name']):'';
					$fileSize = isset($_FILES['brand_image']['size'])?($_FILES['brand_image']['size']):'';
					$uploadName = md5(microtime()).'.'.$fileExt;
					$uploadPath =  BASEURL.'/multivendor/img/brands/'.$uploadName;
					
					$dbpath = '/multivendor/img/brands/'.$uploadName;

					if ($mimeType != 'image') {
						$errors[] = 'The file must be an image';
					}
					$allowed = array('png','jpg','jpeg','gif');
			
					if (!in_array($fileExt, $allowed)) {
						$errors[] = 'The file extension must be a png, jpg, jgep, or gif';
					}
					if ($fileSize > '8388608') {
						$errors[] = 'The File size must be below 8Mb';
					}

// 				//check for errors
				if(!empty($errors)){
					$mes = wrongmessage($errors);
					echo json_encode( array( 'status' => 'fail', 'data' => $mes ) );
					
				}
 				else{

						move_uploaded_file($tmpLoc, $uploadPath);
						$vendor_id = $_SESSION['SBvendor'];
						$db->query("INSERT INTO brands (`vendor_id`,`brand_name`,`brand_image`) VALUES ('$vendor_id','$brandname','$dbpath')");
							$table_row = get_brand_row($brandname);
							echo json_encode( array( 'status' => 'success','data' => $table_row ) );
						
 				 }
	

}



if (!empty($_POST['addcategory'])) {
//vendor ajax
$parent = isset($_POST['parent_category'])?sanitize($_POST['parent_category']):'';
$category = isset($_POST['category_name'])?sanitize($_POST['category_name']):'';
// echo("add category");

$errors = array();
$login_count = 0;	

				if(empty($parent) || empty($category)){
					$errors[] = 'All fields are required...';
				}
	  			//check if category exist in database
				$query = $db->query("SELECT * FROM categories WHERE category_name = '$category'");
				$user = mysqli_fetch_assoc($query);
				$usercount = mysqli_num_rows($query);
				if ($usercount > 0){
					$errors[] = 'That category exist in our database'; 
				}
					$cat_img =  isset($_FILES['category_image']['name'])?($_FILES['category_image']['name']):'';
					$namearray = explode('.', $cat_img);
					$fileName = isset($namearray[0]) ? $namearray[0] : '';
					$fileExt = 	isset($namearray[1]) ? $namearray[1] : '';
					$type = isset($_FILES['category_image']['type'])?($_FILES['category_image']['type']):'';
					$mime = explode('/',$type);
					$mimeType = isset($mime[0]) ? $mime[0] : '';
					$mimeExt = isset($mime[1]) ? $mime[1] : '';
					$tmpLoc = isset($_FILES['category_image']['tmp_name'])?($_FILES['category_image']['tmp_name']):'';
					$fileSize = isset($_FILES['category_image']['size'])?($_FILES['category_image']['size']):'';
					$uploadName = md5(microtime()).'.'.$fileExt;
					$uploadPath =  BASEURL.'/multivendor/img/categories/'.$uploadName;
					
					$dbpath = '/multivendor/img/categories/'.$uploadName;

					if ($mimeType != 'image') {
						$errors[] = 'The file must be an image';
					}
					$allowed = array('png','jpg','jpeg','gif');
			
					if (!in_array($fileExt, $allowed)) {
						$errors[] = 'The file extension must be a png, jpg, jgep, or gif';
					}
					if ($fileSize > '8388608') {
						$errors[] = 'The File size must be below 8Mb';
					}

	 				//check for errors
					if(!empty($errors)){
						$mes = wrongmessage($errors);
						echo json_encode( array( 'status' => 'fail', 'data' => $mes ) );
					}
 				else{
 					$vendor_id = $_SESSION['SBvendor'];
					move_uploaded_file($tmpLoc, $uploadPath);
					$db->query("INSERT INTO categories (`vendor_id`,`category_name`,`parent`,`image`) VALUES ('$vendor_id','$category','$parent','$dbpath')");
					$table_row = get_category_row($category);


					$optionsql = $db->query("SELECT * FROM categories WHERE category_name = '$category' ");
					$options = mysqli_fetch_assoc($optionsql); 

					$option = '<option value="'.$options['id'].'">'.$options['category_name'].'</option>';
					echo json_encode( array( 'status' => 'success','data' => $table_row,'parent_id' =>$parent, 'option' => $option ));
 				 }
}

if (!empty($_POST['addproduct'])) {

	$title = isset($_POST['title'])?sanitize($_POST['title']):'';
	$brand_id = isset($_POST['brand'])?sanitize($_POST['brand']):'';
	$parent_id = isset($_POST['parent'])?sanitize($_POST['parent']):'';
	$child_id = isset($_POST['child'])?sanitize($_POST['child']):'';
	$size = isset($_POST['sizes'])?sanitize($_POST['sizes']):'';
	$sizes = rtrim($size,',');
	$color = isset($_POST['colors'])?sanitize($_POST['colors']):'';
	$colors = rtrim($color,',');
	$price = isset($_POST['price'])?sanitize($_POST['price']):'';
	$list_price = isset($_POST['list-price'])?sanitize($_POST['list-price']):'';
	$tagline = isset($_POST['tagline'])?sanitize($_POST['tagline']):'';
	$description = isset($_POST['description'])?sanitize($_POST['description']):'';

	$errors = array();
	$allowed = array('png','jpg','jpeg','gif');
	$puploadPath = array();
	$pdbpath = '';
			
			if(empty($title) || empty($brand_id) || empty($parent_id) || empty($child_id) || empty($sizes) || empty($colors) || empty($price) || empty($list_price) || empty($tagline) || empty($description)){
				$errors[] = 'All fields are required...';
			}

			// if ( !empty($_FILES['product-image']['name']) ) {
			// 	$errors[] = 'please select product image...';
			// }

			$photoCount = count($_FILES['product-image']['name']);
			//echo $photoCount;


			if($photoCount > 0) {
				for($i=0; $i < $photoCount; $i++) { 
					
					$pimagename = isset($_FILES['product-image']['name'][$i])?$_FILES['product-image']['name'][$i]:'';
					//echo $name;
					//echo($name);
					$pnamearray = explode('.', $pimagename);
					$pfileName = isset($pnamearray[0]) ? $pnamearray[0] : '';
					$pfileExt = isset($pnamearray[1]) ? $pnamearray[1] : '';
					// echo ",".$fileName;
					// echo ",".$fileExt;
					
					$ptype = isset($_FILES['product-image']['type'][$i])?$_FILES['product-image']['type'][$i]:'';
					$pmime = explode('/',$ptype);
					$pmimeType = isset($pmime[0]) ? $pmime[0] : '';
					$pmimeExt = isset($pmime[1]) ? $pmime[1] : '';
					$ptmpLoc[] = isset($_FILES['product-image']['tmp_name'][$i])?$_FILES['product-image']['tmp_name'][$i]:'';
					$pfileSize = $_FILES['product-image']['size'][$i];
					$puploadName = md5(microtime().$i).'.'.$pfileExt;
					$puploadPath[] =  BASEURL.'/multivendor/img/product/'.$puploadName;
					
					if ($i != 0 ) {
						$pdbpath .= ',';
					}
					$pdbpath .= '/multivendor/img/product/'.$puploadName;
					
					if ($pmimeType != 'image') {
						$errors[] = 'please select product image';
					}

				}
			}
			
 				//check for errors
				if(!empty($errors)){
					$mes = wrongmessage($errors);
					echo json_encode( array( 'status' => 'fail', 'data' => $mes ));
				}else{
 					for ($i=0; $i < $photoCount ; $i++) { 
						move_uploaded_file($ptmpLoc[$i], $puploadPath[$i]);
					}
					$vendor_id = $_SESSION['SBvendor'];
					$db->query("INSERT INTO products (`vendor_id`,`title`,`price`,`list_price`,`brand_id`,`category_id`,`sizes`,`color`,`product_image`,`tag_line`,`description`) VALUES ('$vendor_id','$title','$price','$list_price','$brand_id','$child_id','$sizes','$colors','$pdbpath','$tagline','$description') ");
					$table_row = get_product_row($title);
					echo json_encode( array( 'status' => 'success', 'data' => $table_row ) );
	 			}

}

if (isset($_POST['removevendor'])) {

		$delete_id = sanitize($_POST['vendor_id']);
		
		$imgquery = $db->query("SELECT * FROM vendors WHERE id = '$delete_id'");
		$img = mysqli_fetch_assoc($imgquery);
		$image_url = $_SERVER['DOCUMENT_ROOT'].$img['profile_image'];
		unlink($image_url);

	$delete = "DELETE FROM vendors WHERE vendor_id = '$delete_id' ";
	$db->query($delete);

}

if (isset($_POST['removebrand'])) {

		$delete_id = sanitize($_POST['brand_id']);

		$imgquery = $db->query("SELECT * FROM brands WHERE id = '$delete_id'");
		$img = mysqli_fetch_assoc($imgquery);
		$image_url = $_SERVER['DOCUMENT_ROOT'].$img['brand_image'];
		unlink($image_url);

		$delete = "DELETE FROM brands WHERE id = '$delete_id' ";
		$db->query($delete);


//echo "removebrand";
}

if (isset($_POST['removecategory'])) {

		$delete_id = sanitize($_POST['cat_id']);

		$imgquery = $db->query("SELECT * FROM categories WHERE id = '$delete_id'");
		$img = mysqli_fetch_assoc($imgquery);

		if ($img['parent'] == 'parent') {
				
				$cimagequery = $db->query("SELECT * FROM categories WHERE parent = '$delete_id'");
				//$count = mysqli_num_rows($cimagequery);
				
				// for ($i=0; $i < $count ; $i++) { 
					while ($cdelimg = mysqli_fetch_assoc($cimagequery)) {
						$cimage_url = $_SERVER['DOCUMENT_ROOT'].$cdelimg['image'];
						unlink($cimage_url);
					}
				// }

				$image_url = $_SERVER['DOCUMENT_ROOT'].$img['image'];
				unlink($image_url);

				$sql = "DELETE FROM categories WHERE parent = '$delete_id'";
				$db->query($sql);

				$dsql = "DELETE FROM categories WHERE id = '$delete_id' ";
				$db->query($dsql);

				echo json_encode( array( 'status' => 'parentdelete'));
					
		}else{
				$dsql = "DELETE FROM categories WHERE id = '$delete_id' ";
				$db->query($dsql);

				$image_url = $_SERVER['DOCUMENT_ROOT'].$img['image'];
				unlink($image_url);

				echo json_encode( array( 'status' => 'childdelete'));
			
		}



}


if (isset($_POST['removeproduct'])) {

		$delete_id = sanitize($_POST['product_id']);
		
		$db->query("UPDATE products SET deleted = 1 WHERE id = '$delete_id'");

		echo json_encode( array( 'status' => 'success' ,'product_id' => $delete_id));

//echo "removebrand";
}

if (isset($_POST['restore_product'])) {

		$restore_id = sanitize($_POST['product_id']);
		
		$db->query("UPDATE products SET deleted = 0 WHERE id = '$restore_id'");

		echo json_encode( array( 'status' => 'success' ,'product_id' => $restore_id));

//echo "removebrand";
}

if (!empty($_POST['edit_ven']) && empty($_POST['add_ven'])) {
	
$editname = isset($_POST['edit_ven_name'])?sanitize($_POST['edit_ven_name']):'';
$editemail = isset($_POST['edit_ven_email'])?sanitize($_POST['edit_ven_email']):'';
$editmobile = isset($_POST['edit_ven_mobile'])?sanitize($_POST['edit_ven_mobile']):'';
$editphone = isset($_POST['edit_ven_phone'])?sanitize($_POST['edit_ven_phone']):'';
$editaddress = isset($_POST['edit_ven_address'])?sanitize($_POST['edit_ven_address']):'';
$editshop_name = isset($_POST['edit_shop_name'])?sanitize($_POST['edit_shop_name']):'';
$editshop_phone = isset($_POST['edit_shop_phone'])?sanitize($_POST['edit_shop_phone']):'';
$editshop_add = isset($_POST['edit_shop_address'])?sanitize($_POST['edit_shop_address']):'';
$editpermissions = isset($_POST['edit_ven_perm'])?sanitize($_POST['edit_ven_perm']):'';
$editgst_no = isset($_POST['edit_gst_no'])?sanitize($_POST['edit_gst_no']):'';
$editven_id = isset($_POST['ven_id'])?sanitize($_POST['ven_id']):'';


$errorss = array();

				if(empty($editname) || empty($editemail) || empty($editmobile) || empty($editphone) || empty($editaddress) || empty($editshop_name) || empty($editshop_phone) || empty($editshop_add) || empty($editpermissions) || empty($editgst_no) ){
					$errorss[] = 'All fields are required...';
				}
				//VALIDATE EMAIL

				// password value
				$newpass = '';

				if (!empty($_POST['edit_new_pass'])) {
					//password is more than 6 charackter
					if (strlen($_POST['edit_new_pass']) < 6 ) {
						$errorss[] = 'New password must be atleast 6 character';
					}
					$newpass = password_hash(sanitize($_POST['edit_new_pass']), PASSWORD_DEFAULT);
								
				}else{
					$passquery = $db->query("SELECT * FROM vendors WHERE vendor_id = '$editven_id'");
					$oldpass = mysqli_fetch_assoc($passquery);
					$newpass = $oldpass['password'];
				}


				if (!filter_var($editemail, FILTER_VALIDATE_EMAIL)) {
					$errorss[] = 'You must enter a valid email address'; 
				}

// 				
				// $ven_img =  isset($_FILES['edit_ven_img']['name'])?$_FILES['edit_ven_img']['name']:'';
				
				// $photoCount = count($ven_img);	
				// if ($photoCount >0) {

				// }
				$edittmpLoc = '';
				$edituploadPath = '';

				if (!empty($_FILES['edit_ven_img']['name'])) {
					
					$namearray = explode('.', $_FILES['edit_ven_img']['name']);
					$fileName = isset($namearray[0]) ? $namearray[0] : '';
					$fileExt = 	isset($namearray[1]) ? $namearray[1] : '';
					// echo ",".$fileName;
					// echo ",".$fileExt;
					$type = isset($_FILES['edit_ven_img']['type'])?($_FILES['edit_ven_img']['type']):'';
					$mime = explode('/',$type);
					$mimeType = isset($mime[0]) ? $mime[0] : '';
					$mimeExt = isset($mime[1]) ? $mime[1] : '';
					$edittmpLoc = isset($_FILES['edit_ven_img']['tmp_name'])?($_FILES['edit_ven_img']['tmp_name']):'';
					$fileSize = isset($_FILES['edit_ven_img']['size'])?($_FILES['edit_ven_img']['size']):'';
					$uploadName = md5(microtime()).'.'.$fileExt;
					$edituploadPath =  BASEURL.'/multivendor/img/vendorprofile/'.$uploadName;
					
					$editdbpath = '/multivendor/img/vendorprofile/'.$uploadName;

					if ($mimeType != 'image') {
						$errorss[] = 'The file must be an image';
					}
					$allowed = array('png','jpg','jpeg','gif');
			
					if (!in_array($fileExt, $allowed)) {
						$errorss[] = 'The file extension must be a png, jpg, jgep, or gif';
					}
					
					if ($fileSize > '8388608') {
						$errorss[] = 'The File size must be below 8Mb';
					}
					$query = $db->query("SELECT * FROM vendors WHERE vendor_id = '$editven_id'");
					$venimg = mysqli_fetch_assoc($query);
		
						$image_url = $_SERVER['DOCUMENT_ROOT'].$venimg['profile_image'];
						unlink($image_url);
	
					move_uploaded_file($edittmpLoc, $edituploadPath);



				
				}else{

					$query = $db->query("SELECT * FROM vendors WHERE vendor_id = '$editven_id'");
					$vendorimg = mysqli_fetch_assoc($query);
					$editdbpath = $vendorimg['profile_image'];
				}
					// 				//check for errors
				if(!empty($errorss)){
					$mes = wrongmessage($errorss);
					echo json_encode( array('status' => 'fail', 'data' => $mes) );
				}
		 		else{
		// 					//start session  user to db
		// 							$user_id = $user['idate(format)'];
		// 							login($user_id);
									
		// 							$login_count = 1;							
		// 							echo $login_count;
								
									
									$db->query("UPDATE vendors SET `full_name`='$editname',`email`='$editemail',`password`='$newpass',`mobile`='$editmobile',`phone`='$editphone',`address`='$editaddress',`shop_name`='$editshop_name',`shop_phone`='$editshop_phone',`shop_address`='$editshop_add',`gst_no`='$editgst_no',`permissions`='$editpermissions',`profile_image`='$editdbpath' WHERE vendor_id = '$editven_id' ");
									$table_row = get_table_row($editemail);
									echo json_encode( array( 'status' => 'success','data' => $table_row, 'venid' => $editven_id ) );
		 			}

}

if (!empty($_POST['editbrand'])) {	

	$brandname = isset($_POST['brand_name'])?sanitize($_POST['brand_name']):'';
	$brandid = isset($_POST['brand_id'])?sanitize($_POST['brand_id']):'';
	$errors = array();

				if(empty($brandname)){
					$errors[] = 'All fields are required...';
				}

				$edittmpLoc = '';
				$edituploadPath = '';

				if (!empty($_FILES['brand_image']['name'])) {
					
					$namearray = explode('.', $_FILES['brand_image']['name']);
					$fileName = isset($namearray[0]) ? $namearray[0] : '';
					$fileExt = 	isset($namearray[1]) ? $namearray[1] : '';
					// echo ",".$fileName;
					// echo ",".$fileExt;
					$type = isset($_FILES['brand_image']['type'])?($_FILES['brand_image']['type']):'';
					$mime = explode('/',$type);
					$mimeType = isset($mime[0]) ? $mime[0] : '';
					$mimeExt = isset($mime[1]) ? $mime[1] : '';
					$edittmpLoc = isset($_FILES['brand_image']['tmp_name'])?($_FILES['brand_image']['tmp_name']):'';
					$fileSize = isset($_FILES['brand_image']['size'])?($_FILES['brand_image']['size']):'';
					$uploadName = md5(microtime()).'.'.$fileExt;
					$edituploadPath =  BASEURL.'/multivendor/img/brands/'.$uploadName;
					
					$editdbpath = '/multivendor/img/brands/'.$uploadName;

					if ($mimeType != 'image') {
						$errors[] = 'The file must be an image';
					}
					
					if ($fileSize > '8388608') {
						$errors[] = 'The File size must be below 8Mb';
					}
					$query = $db->query("SELECT * FROM brands WHERE id = '$brandid'");
					$img = mysqli_fetch_assoc($query);
		
						$image_url = $_SERVER['DOCUMENT_ROOT'].$img['brand_image'];
						unlink($image_url);
	
					move_uploaded_file($edittmpLoc, $edituploadPath);

				}else{

					$query = $db->query("SELECT * FROM brands WHERE id = '$brandid'");
					$img = mysqli_fetch_assoc($query);
					$editdbpath = $img['brand_image'];
				}
					// 				//check for errors
				if(!empty($errors)){
					$mes = wrongmessage($errors);
					echo json_encode( array('status' => 'fail', 'data' => $mes) );
				}
		 		else{
									
					$db->query("UPDATE brands SET `brand_name`='$brandname',`brand_image`='$editdbpath' WHERE id = '$brandid' ");
					$table_row = get_brand_row($brandname);
					echo json_encode( array( 'status' => 'success','data' => $table_row, 'id' => $brandid ) );
		 			
		 			}

}

if (!empty($_POST['editcategory'])) {	

	$catname = isset($_POST['category_name'])?sanitize($_POST['category_name']):'';
	$parentcat = isset($_POST['parent_category'])?sanitize($_POST['parent_category']):'';
	$catid = isset($_POST['editcatid'])?sanitize($_POST['editcatid']):'';
	$errors = array();

				if(empty($catname)){
					$errors[] = 'All fields are required...';
				}
				// $results1 = $db->query("SELECT * FROM categories WHERE category_name = '$catname' ");
				// $count = mysqli_num_rows($results1);
				// if($count > 0)
				// {
				// 	$errors[] = 'That category allready exists enter another category';
				// }

				$edittmpLoc = '';
				$edituploadPath = '';

				if (!empty($_FILES['category_image']['name'])) {
					
					$namearray = explode('.', $_FILES['category_image']['name']);
					$fileName = isset($namearray[0]) ? $namearray[0] : '';
					$fileExt = 	isset($namearray[1]) ? $namearray[1] : '';
					// echo ",".$fileName;
					// echo ",".$fileExt;
					$type = isset($_FILES['category_image']['type'])?($_FILES['category_image']['type']):'';
					$mime = explode('/',$type);
					$mimeType = isset($mime[0]) ? $mime[0] : '';
					$mimeExt = isset($mime[1]) ? $mime[1] : '';
					$edittmpLoc = isset($_FILES['category_image']['tmp_name'])?($_FILES['category_image']['tmp_name']):'';
					$fileSize = isset($_FILES['category_image']['size'])?($_FILES['category_image']['size']):'';
					$uploadName = md5(microtime()).'.'.$fileExt;
					$edituploadPath =  BASEURL.'/multivendor/img/categories/'.$uploadName;
					
					$editdbpath = '/multivendor/img/categories/'.$uploadName;

					if ($mimeType != 'image') {
						$errors[] = 'The file must be an image';
					}
					
					if ($fileSize > '8388608') {
						$errors[] = 'The File size must be below 8Mb';
					}
					$query = $db->query("SELECT * FROM categories WHERE id = '$catid'");
					$img = mysqli_fetch_assoc($query);
		
						$image_url = $_SERVER['DOCUMENT_ROOT'].$img['image'];
						unlink($image_url);


				}else{

					$query = $db->query("SELECT * FROM categories WHERE id = '$catid'");
					$img = mysqli_fetch_assoc($query);
					$editdbpath = $img['image'];
				}

 				//check for errors
				if(!empty($errors)){
					$mes = wrongmessage($errors);
					echo json_encode( array('status' => 'fail', 'data' => $mes) );
				}
		 		else{
					move_uploaded_file($edittmpLoc, $edituploadPath);

					$db->query("UPDATE categories SET `category_name`='$catname',`parent` = '$parentcat', `image`='$editdbpath' WHERE id = '$catid' ");
					$table_row = get_category_row($catname);
					$results1 = $db->query("SELECT * FROM categories WHERE id = '$catid'");
					$parent = mysqli_fetch_assoc($results1);
					$childparent = $parent['parent'];
					$option = '<option id="'.$catid.'" value="'.$catid.'">'.$catname.'</option>';
					echo json_encode( array( 'status' => 'success', 'data' => $table_row, 'id' => $catid, 'option' =>$option , 'parent' => $childparent ));
		 			
		 			}

}

if (!empty($_POST['editproduct'])) {	

	$edit_title = isset($_POST['edit_p_title'])?sanitize($_POST['edit_p_title']):'';
	$edit_brand_id = isset($_POST['edit_p_brand'])?sanitize($_POST['edit_p_brand']):'';
	$edit_parent_id = isset($_POST['editparent'])?sanitize($_POST['editparent']):'';
	$edit_child_id = isset($_POST['editchild'])?sanitize($_POST['editchild']):'';
	$edit_size = isset($_POST['edit_p_sizes'])?sanitize($_POST['edit_p_sizes']):'';
	$edit_sizes = rtrim($edit_size,',');
	$edit_color = isset($_POST['edit_p_colors'])?sanitize($_POST['edit_p_colors']):'';
	$edit_colors = rtrim($edit_color,',');
	$edit_price = isset($_POST['edit_p_price'])?sanitize($_POST['edit_p_price']):'';
	$edit_list_price = isset($_POST['edit_p_list-price'])?sanitize($_POST['edit_p_list-price']):'';
	$edit_tagline = isset($_POST['edit_p_tagline'])?sanitize($_POST['edit_p_tagline']):'';
	$edit_description = isset($_POST['edit_p_description'])?sanitize($_POST['edit_p_description']):'';

	$edit_product_id = isset($_POST['edit_p_product_id'])?sanitize($_POST['edit_p_product_id']):'';
	$err = array();
	$edit_uploadPath = array();
	$edit_dbpath = '';


				if(empty($edit_title) || empty($edit_brand_id) || empty($edit_parent_id) || empty($edit_child_id) || empty($edit_sizes) || empty($edit_colors) || empty($edit_price) || empty($edit_list_price) || empty($edit_tagline) || empty($edit_description) ){

					$err[] = 'All fields are required...';
				}

				if (!empty($_FILES['edit_product_image']['name'])) {

					$edit_photoCount = count($_FILES['edit_product_image']['name']);
					
					if($edit_photoCount > 0) {
						for($i=0; $i < $edit_photoCount; $i++) { 
							
							$edit_imagename = isset($_FILES['edit_product_image']['name'][$i])?$_FILES['edit_product_image']['name'][$i]:'';
							//echo $name;
							//echo($name);
							$edit_namearray = explode('.', $edit_imagename);
							$edit_fileName = isset($edit_namearray[0]) ? $edit_namearray[0] : '';
							$edit_fileExt = isset($edit_namearray[1]) ? $edit_namearray[1] : '';
							// echo ",".$fileName;
							// echo ",".$fileExt;
							
							$edit_type = isset($_FILES['edit_product_image']['type'][$i])?$_FILES['edit_product_image']['type'][$i]:'';
							$edit_mime = explode('/',$edit_type);
							$edit_mimeType = isset($edit_mime[0]) ? $edit_mime[0] : '';
							$edit_mimeExt = isset($edit_mime[1]) ? $edit_mime[1] : '';
							$edit_tmpLoc[] = isset($_FILES['edit_product_image']['tmp_name'][$i])?$_FILES['edit_product_image']['tmp_name'][$i]:'';
							$edit_fileSize = $_FILES['edit_product_image']['size'][$i];
							$edit_uploadName = md5(microtime().$i).'.'.$edit_fileExt;
							$edit_uploadPath[] =  BASEURL.'/multivendor/img/product/'.$edit_uploadName;
							
							if ($i != 0 ) {
								$edit_dbpath .= ',';
							}
							$edit_dbpath .= '/multivendor/img/product/'.$edit_uploadName;
						
						}
						$finaldbpath = $edit_dbpath.",".$_POST['newImageString'];
					

					}else{

						$finaldbpath = $_POST['newImageString']; 
					}
				}
									
 				//check for errors
				if(!empty($err)){
					$mes = wrongmessage($err);
					echo json_encode( array('status' => 'fail', 'data' => $mes) );
				}
		 		else{
 					for ($i=0; $i < $edit_photoCount ; $i++) { 
						move_uploaded_file($edit_tmpLoc[$i], $edit_uploadPath[$i]);
					}

					$db->query("UPDATE products SET `title`='$edit_title',`price`='$edit_price',`list_price`='$edit_list_price',`brand_id`='$edit_brand_id',`category_id`='$edit_child_id',`sizes`='$edit_sizes',`color`='$edit_colors',`product_image`='$finaldbpath',`tag_line`='$edit_tagline',`description`='$edit_description' WHERE id = '$edit_product_id' ");
					$table_row = get_product_row($edit_title);
					echo json_encode( array( 'status' => 'success','data' => $table_row, 'id' => $edit_product_id ) );
		 			
		 		}
}

if (!empty($_POST['loginven'])) {

				$email = ((isset($_POST['login-email']))?sanitize($_POST['login-email']):'');
				$email = trim($email);
				$password = ((isset($_POST['login-password']))?sanitize($_POST['login-password']):'');
				$password = trim($password);

				$errors = array();

				if(empty($_POST['login-email']) || empty($_POST['login-password'])){
					$errors[] = 'You must provide email and password.';
				}

				//VALIDATE EMAIL

				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$errors[] = 'You must enter a valid email address'; 
				}

				//password is more than 6 charackter
				if (strlen($password) < 6 ) {
					$errors[] = 'Password must be atleast 6 character';
				}

				//check if user exist in database

				$query = $db->query("SELECT * FROM vendors WHERE email = '$email'");
				$user = mysqli_fetch_assoc($query);
				$usercount = mysqli_num_rows($query);
				//echo $usercount;
				if ($usercount < 1){

					$errors[] = 'That email does not exist in our database'; 
				}

				// if(!password_verify($password, $user['password'])){
				// 		$errors[] = 'The password does not match our records. Please try again';
				// }

				//check for errors
				if(!empty($errors)){
					$mes = wrongmessage($errors);
					echo json_encode( array('status' => 'fail', 'data' => $mes) );
				}else{
					//user login 
					//echo "log user in";

					$user_id = $user['vendor_id'];	
					$user_name = $user['full_name'];
					$user_email = $user['email'];
					$user_mobile = $user['mobile'];
					$user_shop_name = $user['shop_name'];
					$user_permission = $user['permissions'];
						//echo $user_id;
					$count = vendorlogin($user_id, $user_name, $user_email, $user_mobile, $user_shop_name, $user_permission );

					if ($count == 1) {
							$date = date("y-m-d H:i:s");
							$db->query("UPDATE vendors SET last_login = '$date', current_login = '1' WHERE vendor_id = '$user_id' ");
					}
					$errors[] = 'You are successfully login'; 
					$mes = rightmessage($errors);
					echo json_encode( array( 'status' => 'success','data' => $mes) );
				}
		
}


?>