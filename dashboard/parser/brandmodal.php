<?php  

require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';

$id = $_POST['id'];
$id1 = (int)$id;

$sql = "SELECT * FROM brands WHERE id = '$id1'";
$result = $db->query($sql);
$brands = mysqli_fetch_assoc($result);

?>


<?php ob_start();?>
<style type="text/css">
	.form-group{
		margin: 0px 0 0 0;
	}
	.form-group label.control-label{
		    margin: 5px 0 0 0;
	}
</style>
<div class="modal fade detail-1" id="details-modal" tabindex="-1" role="dialog"> 
	<div class="modal-dialog modal-md edit-modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<button class="btn btn-default edit-modal-close" type="button" onclick="closeModal()" aria-label="Close">
			<span area-hidden="true">&times;</span>
			</button>
			<div class="container-fluid">
				<section class="edit-message"></section>
				<form id="brandeditform" method="post" enctype="multipart/form-data">
						<div class="row">
   			                                        <div class="col-sm-6 col-md-10 col-sm-offset-3 col-md-offset-1 text-center">
   			                                        	<img style="height: 140px; width: 140px; margin: auto; margin-top: 10px; border-radius: 100%;" src="<?=$brands['brand_image'];?>" class="img-responsive">
   			                                        </div>
			                                        <div class="col-sm-12 col-md-12">
		                                    			<div class="col-sm-12 col-md-10 col-md-offset-1">
															<div class="form-group is-empty">
																<label class="control-label">Brand Name</label>
																<input type="text" class="form-control" name="brand_name" id="brand_name" value="<?=$brands['brand_name'];?>">
																<span class="material-input"></span>
															</div>
				                                        </div>
				                                    </div>
					                                <div class="col-sm-12 col-md-10 col-md-offset-1 text-center margin-t-10-sm">
				                                    	<div class="upload-btn text-center">
					                                   			<input type="file" multiple name="brand_image" id="brand_image" class="product-img brand-edit-img">
					                                   			<label class="upload-label-category edit-brand-upload-label">Update image (400 x 400)</label>
					                                   			<input type="text" name="editbrand" class="hidden" value="0">
																<input type="text" name="brand_id" class="hidden" value="<?=$brands['id'];?>">
				                                    	</div>
				                                    </div>
			                                    	<div class="col-sm-12 col-md-12 text-center">
		                                        		<hr class="btn-upper-hr">
			                                    		<button class="form_submit-btn btn btn-primary" id="form_submit">Edit brand</button>
			                                    		<button class="btn btn-default" onclick="closeModal()"> Close </button>
			
			                                    	</div>
			                                    </div>						
			                                    		
				</form>
			</div>
		</div>
		
	</div>
	</div>
</div>
<script type="text/javascript">

	function closeModal() {
			$('#details-modal').modal('hide');
			 setTimeout(function(){
			 	$("form#brandeditform").remove();
			 	jQuery("#details-modal").remove();
				$(".modal-backdrop").removeClass("in");
			 	$(".modal-backdrop").each(function(){
			 		$(this).addClass("out");
			 	});
			 	$(".modal-backdrop").each(function(){
			 		$(this).addClass("hidden");
			 	});
		 	
		// },500)
	});
}

</script>
<script type="text/javascript">
		
		$("form#brandeditform").submit(function(e){
			e.preventDefault();
			$('input[name=editbrand]').attr('value','1');
			var editformData = new FormData(this);
			jQuery.ajax({
				url : '/multivendor/dashboard/parser/ajax.php',
				method : 'POST',
				data : editformData,
				async: true,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data){ 
						// alert(data);
						var result = JSON.parse(data);

						if ( result.status == "success" ) {
				        	var element = document.getElementById(result.id);
				        	element.remove();
				        	$(result.data).prependTo("#data-row");
				        	//alert(element);
				        	closeModal();
//				           	location.reload();
				  //      	alert(result.data);
				        }else if ( result.status == "fail" ){
						
							$('.edit-message').html(result.data);
						}else{
							alert("Something went wrong with database");
						}		
		                     
		                    //alert-close
		                    $('#alert-close').click(function(){
								$('.edit-message').html(" ");
							});    	
				 },
				
				error : function(){ alert("something went wrong");}   
			});  
    	 });
   
</script>
<?php echo ob_get_clean();?>