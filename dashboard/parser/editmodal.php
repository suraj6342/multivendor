
<?php  

require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';

$id = $_POST['id'];
$id1 = (int)$id;

$sql = "SELECT * FROM vendors WHERE vendor_id = '$id1'";
$result = $db->query($sql);
$vendor = mysqli_fetch_assoc($result);


?>


<?php ob_start();?>
<style type="text/css">
	.form-group{
		margin: 0px 0 0 0;
	}
	.form-group label.control-label{
		    margin: 5px 0 0 0;
	}
</style>
<div class="modal fade detail-1" id="details-modal" tabindex="-1" role="dialog"> 
	<div class="modal-dialog modal-lg edit-modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<button class="btn btn-default edit-modal-close" type="button" onclick="closeModal()" aria-label="Close">
			<span area-hidden="true">&times;</span>
			</button>
			<div class="container-fluid">
				<section class="edit-message"></section>
				<form id="editdata" method="post" enctype="multipart/form-data">
	                                   			<div class="row">
	                                   				<div class="col-sm-12">
														<img src="<?=$vendor['profile_image'];?>" class="img-responsive profile-image">
	                                   				</div>
	                                   			</div>
	                                   			<hr class="btn-upper-hr btn-upper-hr-edit">
												<div class="row">
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group is-empty">
															<label class="control-label">Full Name</label>
															<input type="text" class="form-control" name="edit_ven_name" id="edit_ven_name" value="<?=$vendor['full_name'];?>">
														<span class="material-input"></span></div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group is-empty">
															<label class="control-label">Email address</label>
															<input type="email" class="form-control" name="edit_ven_email" id="edit_ven_email" value="<?=$vendor['email'];?>">
														<span class="material-input"></span></div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group is-empty">
															<label class="control-label">New Password</label>
															<input type="password" class="form-control" value="" name="edit_new_pass" id="edit_new_pass">
														<span class="material-input"></span></div>
			                                        </div>
			                                         <div class="col-sm-6 col-md-3">
														<div class="form-group is-empty">
															<label class="control-label">GST No</label>
															<input type="text" class="form-control" value="<?=$vendor['gst_no'];?>" name="edit_gst_no" id="edit_gst_no">
														<span class="material-input"></span></div>
			                                        </div>
			                                    </div>
			                                    <div class="row">
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group is-empty">
															<label class="control-label">Mobile</label>
															<input type="text" class="form-control" value="<?=$vendor['mobile'];?>" name="edit_ven_mobile" id="edit_ven_mobile">
														<span class="material-input"></span></div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group is-empty">
															<label class="control-label">Phone</label>
															<input type="text" class="form-control" value="<?=$vendor['phone'];?>" name="edit_ven_phone" id="edit_ven_phone">
														<span class="material-input"></span></div>
			                                        </div>
			                                         <div class="col-sm-12 col-md-6">
														<div class="form-group is-empty">
															<label class="control-label">Address</label>
															<input type="text" class="form-control" value="<?=$vendor['address'];?>" name="edit_ven_address" id="edit_ven_address">
														<span class="material-input"></span></div>
			                                        </div>
			                                    </div>
			                                    <div class="row">
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group is-empty">
															<label class="control-label">Shop Name</label>
															<input type="text" class="form-control" value="<?=$vendor['shop_name'];?>" name="edit_shop_name" id="edit_shop_name">
														<span class="material-input"></span></div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group is-empty">
															<label class="control-label">Shop Phone</label>
															<input type="text" class="form-control" value="<?=$vendor['shop_phone'];?>" name="edit_shop_phone" id="edit_shop_phone">
														<span class="material-input"></span></div>
			                                        </div>
			                                        <div class="col-sm-12 col-md-6">
														<div class="form-group is-empty">
															<label class="control-label">Shop Address</label>
															<input type="text" class="form-control" value="<?=$vendor['address'];?>" name="edit_shop_address" id="edit_shop_address">
														<span class="material-input"></span></div>
			                                        </div>
			                                    </div>
			                                    <div class="row">
			                                       
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group is-empty">
															<select class="form-control" id="edit_ven_perm" name="edit_ven_perm">
															  <option value="<?=$vendor['permissions'];?>" selected><?=$vendor['permissions'];?></option>
															  <option value="admin,vendor">Admin</option>
															  <option value="vendor">Vendor</option>
															</select>
														<span class="material-input"></span></div>
			                                        </div>
			                                        <div class="col-sm-8 col-md-6">
				                                        <div class="upload-btn vendor-upload-btn text-center">
					                                   		<input type="file" id="edit_ven_img" name="edit_ven_img" class="product-img vendor-img">
					                                   		<label class="upload-label-brand upload-label-vendor edit-ven-label">Update Profile (400 x 400)</label>
				                                    	</div>
				                                    	<input type="text" name="add_ven" id="add_ven" class="hidden" value="">
														<input type="text" name="edit_ven" id="add_ven" class="hidden" value="">
														<input type="text" name="ven_id" class="hidden" value="<?=$vendor['vendor_id'];?>">
				                                    </div>
				                                </div>
			                                    <div class="row">
			                                    	<div class="col-md-12 text-center">
			                                    		<hr class="btn-upper-hr">
			                                    		<button class="form_submit-btn btn btn-primary" id="form_submit">Edit Vendor</button>
			                                    		<button class="btn btn-default" onclick="closeModal()"> Close </button>
			
			                                    	</div>
			                                    </div>
			                              				
				</form>
			</div>
		</div>
		
	</div>
	</div>
</div>
<script type="text/javascript">

	function closeModal() {
			$('#details-modal').modal('hide');
			 setTimeout(function(){
			 	$("form#editdata").remove();
			 	jQuery("#details-modal").remove();
				$(".modal-backdrop").removeClass("in");
			 	$(".modal-backdrop").each(function(){
			 		$(this).addClass("out");
			 	});
			 	$(".modal-backdrop").each(function(){
			 		$(this).addClass("hidden");
			 	});
		 	
		// },500)
	});
}

</script>
<script type="text/javascript">
		$("form#editdata").submit(function(e){
				 e.preventDefault();
			$('input[name=add_ven]').attr('value','');
			$('input[name=edit_ven]').attr('value','1');
			
			    var editformData = new FormData(this);
				
    	 	jQuery.ajax({
				url : '/multivendor/dashboard/parser/ajax.php',
				method : 'POST',
				data : editformData,
				async: false,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data){ 
						// alert(data);
						var result = JSON.parse(data);

						if ( result.status == "success" ) {
							$("form#editdata")[0].reset();
				        	var element = document.getElementById(result.venid);
				        	element.remove();
				        	$(result.data).prependTo("#vendor-data-row");
				        	//alert(element);
				        	closeModal();
//				           	location.reload();
				  //      	alert(result.data);
				        }else if ( result.status == "fail" ){
						
							$('.edit-message').html(result.data);
						}else{
							alert("Something went wrong with database");
						}		
		                     
		                    //alert-close
		                    $('#alert-close').click(function(){
								$('.edit-message').html(" ");
							});    	
				 },
				
				error : function(){ alert("something went wrong");}   
			});  
    	 });
   
</script>
<?php echo ob_get_clean();?>