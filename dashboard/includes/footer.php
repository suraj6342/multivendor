
			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">
						<ul>
							<li>
								<a href="/multivendor/">
									Website
								</a>
							</li>
							<li>
								<a href="https://icons8.com">Icon pack by Icons8</a>
							</li>
						</ul>
					</nav>
					<p class="copyright pull-right">
						&copy; <script><?php echo date("Y"); ?></script> <a href="/multivendor/">Multivendor</a>,
					</p>
				</div>
			</footer>
		</div>
	</div>

</body>
	
<!--   Core JS Files   -->
	<script src="assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/material.min.js" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

	<!--  Notifications Plugin    -->
	<script src="assets/js/bootstrap-notify.js"></script>

	<!-- Material Dashboard javascript methods -->
	<script src="assets/js/material-dashboard.js"></script>

	<!-- Material Dashboard DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>
	<script src="assets/js/custom-file-input.js"></script>
    <script type="text/javascript">
	$('.add-vendor-btn').click(function(){
		$('.vendor-form').slideToggle();
	});
	</script>
	<script type="text/javascript">
			$('#alert-close').click(function(e){
				 e.preventDefault();
				$('.alert-back').addClass("hidden");
			});

	</script>

</html>
