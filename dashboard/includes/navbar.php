<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';
?>
<nav class="navbar navbar-transparent navbar-absolute">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Dashboard</a>
					</div>
					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
									<i class="material-icons">dashboard</i>
									<p class="hidden-lg hidden-md">Dashboard</p>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="material-icons">notifications</i>
									<span class="notification">5</span>
									<p class="hidden-lg hidden-md">Notifications</p>
								</a>
								<ul class="dropdown-menu">
									<li><a href="#">Mike John responded to your email</a></li>
									<li><a href="#">You have 5 new tasks</a></li>
									<li><a href="#">You're now friend with Andrew</a></li>
									<li><a href="#">Another Notification</a></li>
									<li><a href="#">Another One</a></li>
								</ul>
							</li>
							<li>
								<a style="background: cornflowerblue; cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown">
		 							   <i class="material-icons">person</i>
		 							   <i style="margin-left: -5px;" class="caret hidden-xs hidden-sm"></i>
		 							   <p class="hidden-lg hidden-md">Profile</p>
		 						</a>
		 						   <ul class="dropdown-menu">
		 						   	<?php if($_SESSION['SBvendorpermission'] == "admin,vendor"): ?>
		 						   	<li><a>Admin</a></li>
								    <?php else: ?>
									<li><a>Vendor</a></li>
								    <?php endif; ?>
								    <li><a href="#"><?=$_SESSION['SBvendorname'];?></a></li>
								    <li><a href="#"><?=$_SESSION['SBvendorshopname'];?></a></li>
								    <li><a href="#"><?=$_SESSION['SBvendoremail'];?></a></li>
								    <?php if($_SESSION['SBvendorpermission'] == "admin,vendor"): ?>
		 						   	<li class="hidden"><a></a></li>
								    <?php else: ?>
									<li><a onclick="updateprofile(<?=$_SESSION['SBvendor'];?>);return false;">Edit Prodile</a></li>
								    <?php endif; ?>
								    <li><a href="logout.php">Logout</a></li>
								  </ul>
								
							</li>
						</ul>

						<form class="navbar-form navbar-right" role="search">
							<div class="form-group  is-empty">
	                        	<input type="text" class="form-control" placeholder="Search">
	                        	<span class="material-input"></span>
							</div>
							<button type="submit" class="btn btn-white btn-round btn-just-icon">
								<i class="material-icons">search</i><div class="ripple-container"></div>
							</button>
	                    </form>
					</div>
				</div>
			</nav>
<script type="text/javascript">
function updateprofile(id){

		var data ={"id" : id};
		jQuery.ajax({
			url :'/multivendor/dashboard/parser/editvendor.php',
			method : "post",
			data : data,
			success:function(data){
				jQuery('body').append(data);
				jQuery('#details-modal').modal('toggle');
			 },
			error:function(){
				alert("something went wrong");
			}
		});
		
	}
function closeModal() {
	$('#details-modal').modal('hide');
		 setTimeout(function(){
		 	$("form#editdata").remove();
		 	jQuery("#details-modal").remove();
			$(".modal-backdrop").removeClass("in");
		 	$(".modal-backdrop").each(function(){
		 		$(this).addClass("out");
		 	});
		 	$(".modal-backdrop").each(function(){
		 		$(this).addClass("hidden");
		 	});
		 	
		// },500)
	});
}

</script>
