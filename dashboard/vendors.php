<?php 
require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';
$result = is_logged_in();
if ($result == '0' ) {
	login_error_redirect();
}


include'includes/header.php'; 

$vendorsquery = $db->query("SELECT * FROM vendors ORDER BY vendor_id DESC");
	

?>
		<ul class="nav" id="nav">
	                <li>
	                    <a href="dashboard.php">
	                        <i class="material-icons">dashboard</i>
	                        <p>Dashboard</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="brands.php">
	                        <i class="material-icons">content_paste</i>
	                        <p>Brands</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="categories.php">
	                        <i class="material-icons">library_books</i>
	                        <p>Categories</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="products.php">
	                        <i class="material-icons">bubble_chart</i>
	                        <p>Products</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="archieve.php">
	                        <i class="material-icons">location_on</i>
	                        <p>Archieve</p>
	                    </a>
	                </li>
   	                <?php  $result = has_permission(); 
					if( $result == '1'): ?>
						<li class="active">
		                    <a href="vendors.php">
		                        <i class="material-icons text-gray">notifications</i>
		                        <p>Vendors</p>
		                    </a>
	                	</li>
					<?php endif;?>

   	               <?php  $result = has_permission(); 
					if( $result == '1'): ?>
					<li>
	                    <a href="frontui.php">
	                        <i class="material-icons text-gray">notifications</i>
	                        <p>Front UI</p>
	                    </a>
	                </li>
	                <?php endif;?>
					
	            </ul>
	    	</div>
	    </div>
	    
	    <div class="main-panel">
			<?php include'includes/navbar.php'; ?>
	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="card">
	                            <div class="card-header text-center" data-background-color="orange">
	                                <h4 class="title">Add Vendors</h4>
									<p class="category">add every details carefully</p>
										<section class="message">
				
										</section>
	                            </div>
	                            <div class="card-content">
	                                    <div class="row">
	                                    	<div class="col-md-12 text-center">
	                                    		<button class="btn btn-primary add-vendor-btn">Add Vendors</button>
	                                    	</div>
	                                    </div>
	                                    <div class="row vendor-form">
	                                    <form id="data" method="post" enctype="multipart/form-data">
	                                    	<div class="col-sm-12 col-md-12">
			                                    <div class="row">
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">Full Name</label>
															<input type="text" class="form-control" name="ven_name" id="ven_name" value="" >
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">Email address</label>
															<input type="email" class="form-control" name="ven_email" id="ven_email" value=""  >
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">Password</label>
															<input type="password" class="form-control" value="" name="ven_pass" id="ven_pass" >
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">Re-password</label>
															<input type="password" class="form-control" value="" name="ven_repass" id="ven_repass"  >
														</div>
			                                        </div>
			                                    </div>
			                                    <div class="row">
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">Mobile</label>
															<input type="text" class="form-control" value="" name="ven_mobile" id="ven_mobile" >
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">Phone</label>
															<input type="text" class="form-control" value="" name="ven_phone" id="ven_phone"  >
														</div>
			                                        </div>
			                                         <div class="col-sm-12 col-md-6">
														<div class="form-group label-floating">
															<label class="control-label">Address</label>
															<input type="text" class="form-control" value="" name="ven_address" id="ven_address" >
														</div>
			                                        </div>
			                                    </div>
			                                    <div class="row">
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">Shop Name</label>
															<input type="text" class="form-control" value="" name="ven_shop_name" id="ven_shop_name" >
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">Shop Phone</label>
															<input type="text" class="form-control" value="" name="ven_shop_phone" id="ven_shop_phone" >
														</div>
			                                        </div>
			                                        <div class="col-sm-12 col-md-6">
														<div class="form-group label-floating">
															<label class="control-label">Shop Address</label>
															<input type="text" class="form-control"  value="" name="ven_shop_address" id="ven_shop_address" >
														</div>
			                                        </div>
			                                    </div>
			                                    <div class="row">
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<label class="control-label">GST No</label>
															<input type="text" class="form-control" value="" name="ven_gst_no" id="ven_gst_no" >
														</div>
			                                        </div>
			                                        <div class="col-sm-6 col-md-3">
														<div class="form-group label-floating">
															<select class="form-control" id="ven_perm" name="ven_perm" >
															  <option value="" selected>Select Permissions</option>
															  <option value="admin,vendor">Admin</option>
															  <option value="vendor">Vendor</option>
															</select>
														</div>
			                                        </div>
			                                        <div class="col-sm-8 col-md-6">
				                                        <div class="upload-btn vendor-upload-btn text-center">
					                                   		<input type="file" id="ven_img" name="ven_img" class="product-img vendor-img" >
					                                   		<label class="upload-label-brand upload-label-vendor">Profile Images (400 x 400)</label>
				                                    	</div>
				                                    	<input type="text" name="add_ven" id="add_ven" class="hidden" value="0">
														<input type="text" name="edit_ven" id="add_ven" class="hidden" value="0">
				                                    </div>
			                                    </div>
			                                    <div class="row">
			                                    	<div class="col-md-12 text-center">
			                                    		<hr class="btn-upper-hr">
			                                    		<button class="form_submit-btn btn btn-primary" id="form_submit" >Add</button>
			                                    	</div>
			                                    </div>
			                                </div>
			                            </form>
	                                    </div>
	                                
	                            </div>
	                        </div>
	                    </div>
	                     <div class="col-md-12">
	                    	<div class="col-sm-12 no-padding-lf">
								<div class="card">
		                            <div class="card-header text-center" data-background-color="orange">
		                                <h4 class="title">Vendors List</h4>
		                                <p class="category">all vendors in database</p>
		                            </div>
		                            <div class="card-content table-responsive">
		                                <table class="table table-hover table-stiped">
		                                    <thead class="text-warning">
		                                        <tr>
			                                        <th class="text-center">Id</th>
			                                        <th class="text-center">Image</th>
			                                    	<th class="text-center">Full Name</th>
			                                    	<th class="text-center">Mobile</th>
			                                    	<th class="text-center">Email</th>
			                                    	<th class="text-center">Shop Name</th>
			                                    	<th class="text-center">Shop Phone</th>
			                                    	<th class="text-center">Permissions</th>
			                                    	<th class="text-center">GST No</th>
			                                    	<th class="text-center">Edit / Remove</th>
			                                    </tr>
		                                    </thead>
		                                    <tbody class="text-center" id="vendor-data-row">
		                                        <?php while($user = mysqli_fetch_assoc($vendorsquery)): ?>

			                                        <tr class="vendortablerow" id="<?=$user['vendor_id'];?>">
			                                        	<td><?=$user['vendor_id'];?></td>
			                                        	<td><img style="height: 70px; width: 70px;" src="<?=$user['profile_image'];?>" class="img-responsive profile_image"></td>
			                                        	<td><?=$user['full_name'];?></td>
			                                        	<td><?=$user['mobile'];?></td>
			                                        	<td><?=$user['email'];?></td>
			                                        	<td><?=$user['shop_name'];?></td>
			                                        	<td><?=$user['shop_phone'];?></td>
			                                        	<td><?=$user['permissions'];?></td>
			                                        	<td><?=$user['gst_no'];?></td>
			                                        	<td>
			                                        	<button class="btn btn-xs btn-danger" onclick="editmodal(<?=$user['vendor_id'];?>)" disabled><i class="material-icons">create</i></span></i></button>
			                                        	<button class="btn btn-xs btn-danger" onclick="removevendor(<?=$user['vendor_id'];?>)" disabled><i class="material-icons">delete_sweep</i></button>
			                                        	</td>
			                                        </tr>
			                                    <?php endwhile; ?>
		                                    </tbody>
		                                </table>
		                            </div>
		                        </div>
							</div>
	                    </div>
					</div>
	            </div>
	        </div>

<?php include'includes/footer.php'; ?>
 <!-- vednor ajax -->
	<script type="text/javascript">
    	 
			$("form#data").submit(function(e){
				 e.preventDefault();
			$('input[name=add_ven]').attr('value','1');
			$('input[name=edit_ven]').attr('value','');
			
			    var formData = new FormData(this);
				
    	 	jQuery.ajax({
				url : '/multivendor/dashboard/parser/ajax.php',
				method : 'POST',
				data : formData,
				async: false,
				cache: false,
		        contentType: false,
		        processData: false,
				success : function(data){ 
						// alert(data);
						var result = JSON.parse(data);

						if ( result.status == "success" ) {
							$('.vendor-form').slideUp();
							$("form#data")[0].reset();
				        	$(result.data).prependTo("#vendor-data-row");
				           	// location.reload();
				           		//alert("insert");
				        
				        }else if ( result.status == "fail" ){
						
							$('.message').html(result.data);
						}else{
							alert("Something went wrong with database");
						}		
		        //                 	var logincount = data;
		        //                 	if (logincount == 1) {
										// $("#result").html(ajax_load).load(loadUrl);
		        //                 	}else{
		        //                 		$('.message').html(logincount);	
		        //                 	}
		                        	//alert-close
		                        	$('#alert-close').click(function(){
										$('.message').html(" ");
									});
				 },
				
				error : function(){ alert("something went wrong");}   
			});  
    	 });
    </script>
<script type="text/javascript">
	
	function removevendor(vendor_id){
		
		var removevendor = 0;
		 
	    $("#"+ vendor_id).fadeOut("slow", function() { $(this).remove(); });
	    jQuery.ajax({
				url : '/multivendor/dashboard/parser/ajax.php',
				method : 'POST',
				data : {vendor_id: vendor_id, removevendor : 1},
				success : function(data){ 
				},
				error : function(){ alert("something went wrong");}   
			}); 
	}

	function editmodal(id){

		var data ={"id" : id};
		jQuery.ajax({
			url :'/multivendor/dashboard/parser/editmodal.php',
			method : "post",
			data : data,
			success:function(data){
				jQuery('body').append(data);
				jQuery('#details-modal').modal('toggle');
			 },
			error:function(){
				alert("something went wrong");
			}
		});
		
	}

</script>

<!-- end -->
<script type="text/javascript">

function closeModal() {
	$('#details-modal').modal('hide');
		 setTimeout(function(){
		 	$("form#editdata").remove();
		 	jQuery("#details-modal").remove();
			$(".modal-backdrop").removeClass("in");
		 	$(".modal-backdrop").each(function(){
		 		$(this).addClass("out");
		 	});
		 	$(".modal-backdrop").each(function(){
		 		$(this).addClass("hidden");
		 	});
		 	
		// },500)
	});
}

</script>



