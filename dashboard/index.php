<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';

?>

<!DOCTYPE html>
<html lang="en">
  <head>
  	<title>Dashboard - Login</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="assets/img/logo.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet"> 
    <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="assets/css/demo.css">
  </head>
  <body>
    <div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 blur-back">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 main-panel">
				<div class="col-xs-12 col-sm-12 col-md-12 box-shadow">
					<section class="message"></section>
					<div class="col-sm-6 col-md-6 left-panel">
						<div class="col-sm-12 opacity-tab">
							<div class="col-xs-8 col-sm-8 col-xs-offset-2 col-sm-offset-2 xs-tab">
								<img src="assets/img/stats.png" class="img-responsive">
							</div>
							<div class="col-xs-12 col-sm-12 wel-mes">
								<h3>Welcome To Your Store Dashboard Login</h3>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-6 right-panel no-padding-lf">
					<div class="col-sm-12 vertical-middle no-padding-lf">                    	
					<form id="loginform" method="post" enctype="multipart/form-data">
	                	<h3 class="login-head">Login</h3>
						<div class="col-sm-12">
							  <input required='' type='email' autofocus="" name="login-email" id="user-email" placeholder="enter email address" class="input" id="user-email">
						</div>
						<div class="col-sm-12">
							  <input required='' type='password' name="login-password" id="user-pass" class="input" placeholder="enter password" id="user-pass">
						</div>
						<input type="text" name="loginven" id="loginven" value="0" class="hidden">
						<div class="col-sm-12 text-center">
							<button class="btn btn-danger login-btn">Login</button>
						</div>
						<div style="margin-top: 30px; color: azure;" class="col-sm-12 demo-login text-center">
							<h5>For Demo Login</h5>
							<p>email : admin@gmail.com</p>
							<p>pass : 123456 </p>
						</div>
					</form>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


	<script src="assets/js/jquery-3.1.0.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	 <script>
			$("form#loginform").submit(function(e){
				e.preventDefault();
		        $('input[name=loginven]').attr('value','1');
				
			    var formData = new FormData(this);
				
    	          
                    $.ajax({
                        	url : '/multivendor/dashboard/parser/ajax.php',
							method : 'POST',
							data : formData,
							async: true,
							cache: false,
		        			contentType: false,
		        			processData: false,
							success : function(data){ 
							var result = JSON.parse(data);

                        	if (result.status == "success") {
								window.location.href = 'dashboard.php';

                        	}else if (result.status == "fail"){
                        		$('.message').html(result.data);	
                        	}else{
                        		alert("something went wrong with database");
                        	}
                        	//alert-close
                        	$('#alert-close').click(function(){
								$('.message').html(" ");
							});
                        },
                        error: function(data){
                        	alert(data);
                        }

                    });
                    
                });
</script>
<script type="text/javascript">
			$('#alert-close').click(function(e){
				 e.preventDefault();
				$('.alert-back').addClass("hidden");
			});

		</script>
  </body>
</html>