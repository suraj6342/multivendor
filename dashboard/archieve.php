<?php 
require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';
$result = is_logged_in();
if ($result == '0' ) {
	login_error_redirect();
}


$archievequery = $db->query("SELECT * FROM products WHERE deleted = '1' ORDER BY title");

$ven_id = $_SESSION['SBvendor'];
$venproductquery = $db->query("SELECT * FROM products WHERE vendor_id = '$ven_id' AND deleted = '1' ");

include'includes/header.php'; ?>
		<ul class="nav" id="nav">
	                <li>
	                    <a href="dashboard.php">
	                        <i class="material-icons">dashboard</i>
	                        <p>Dashboard</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="brands.php">
	                        <i class="material-icons">content_paste</i>
	                        <p>Brands</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="categories.php">
	                        <i class="material-icons">library_books</i>
	                        <p>Categories</p>
	                    </a>
	                </li>
	                <li>
	                    <a href="products.php">
	                        <i class="material-icons">bubble_chart</i>
	                        <p>Products</p>
	                    </a>
	                </li>
	                <li class="active">
	                    <a href="archieve.php">
	                        <i class="material-icons">location_on</i>
	                        <p>Archieve</p>
	                    </a>
	                </li>
   	                <?php  $result = has_permission(); 
					if( $result == '1'): ?>
						<li>
		                    <a href="vendors.php">
		                        <i class="material-icons text-gray">notifications</i>
		                        <p>Vendors</p>
		                    </a>
	                	</li>
					<?php endif;?>
					<?php  $result = has_permission(); 
					if( $result == '1'): ?>
					<li>
	                    <a href="frontui.php">
	                        <i class="material-icons text-gray">notifications</i>
	                        <p>Front UI</p>
	                    </a>
	                </li>
	                <?php endif;?>
					
					
	            </ul>
	    	</div>
	    </div>
	    
	    <div class="main-panel">
			<?php include'includes/navbar.php'; ?>


	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-12">
	                    	<div class="col-sm-12">
								<div class="card">
		                            <div class="card-header text-center" data-background-color="orange">
		                                <h4 class="title">Archieve Products List</h4>
		                                <p class="category">all archieve products in database</p>
		                            </div>
		                            <div class="card-content table-responsive">
		                               	<?php if($_SESSION['SBvendorpermission'] == "admin,vendor"): ?>
				                                    		
		                                <table class="table table-hover table-stiped">
		                                    <thead class="text-warning">
		                                        <tr>
			                                        <th class="text-center">Id</th>
			                                        <th class="text-center">Vendor Id</th>
			                                    	<th class="text-center">Shop Name</th>
			                                    	<th class="text-center">Title</th>
			                                    	<th class="text-center">Price</th>
			                                    	<th class="text-center">Brand</th>
			                                    	<th class="text-center">Category</th>
			                                    	<th class="text-center">Image</th>
			                                    	<th class="text-center">Restore</th>
			                                    </tr>
		                                    </thead>
		                                    <tbody class="text-center" id="data-row">
		                                        <?php while($product = mysqli_fetch_assoc($archievequery)): 
		                                        
		                                        $vendorid = $product['vendor_id'];
		                                        $shopnamequery = $db->query("SELECT * FROM vendors WHERE vendor_id = $vendorid");
		                                        $shopname = mysqli_fetch_assoc($shopnamequery);

		                                        $brandid = $product['brand_id'];
		                                        $brandname = $db->query("SELECT * FROM brands WHERE id = '$brandid' ");
		                                        $brand = mysqli_fetch_assoc($brandname);

		                                       	$childID = $product['category_id'];
												$result = $db->query("SELECT * FROM categories Where id = $childID");
												$child = mysqli_fetch_assoc($result);
												$parentID = $child['parent'];
												$presult = $db->query("SELECT * FROM categories WHERE id = '$parentID' ");
												$parent = mysqli_fetch_assoc($presult);
												$category = $parent['category_name'].'-'.$child['category_name'];

		                                        ?>
		                                        <tr class="tablerow" id="pro<?=$product['id'];?>">
		                                        	<td><?=$product['id'];?></td>
		                                        	<td><?=$product['vendor_id'];?></td>
		                                        	<td><?=$shopname['shop_name'];?></td>
		                                        	<td><?=$product['title'];?></td>
		                                        	<td><?=money($product['price']);?></td>
		                                        	<td><?=$brand['brand_name'];?></td>
		                                        	<td><?=$category;?></td>
		                                        	<?php $photos = explode(',',$product['product_image']);?>
		                                        	<td><img style="height: 70px; width: 70px; border-radius: 50%;" src="<?=$photos[0];?>" class="img-responsive"></td>
		                                        	<td>
		                                        	<button class="btn btn-xs btn-danger" onclick="restoreproduct(<?=$product['id'];?>);return false;"><i class="material-icons">autorenew</i></button>
		                                        	</td>
		                                        </tr>
		                                    	<?php endwhile; ?>
		                                    </tbody>
		                                </table>

		                                <?php else: ?>
										
										<table class="table table-hover table-stiped">
		                                    <thead class="text-warning">
		                                        <tr>
			                                        <th class="text-center">Id</th>
			                                        <th class="text-center">Vendor Id</th>
			                                    	<th class="text-center">Shop Name</th>
			                                    	<th class="text-center">Title</th>
			                                    	<th class="text-center">Price</th>
			                                    	<th class="text-center">Brand</th>
			                                    	<th class="text-center">Category</th>
			                                    	<th class="text-center">Image</th>
			                                    	<th class="text-center">Edit / Remove</th>
			                                    </tr>
		                                    </thead>
		                                    <tbody class="text-center" id="data-row">
		                                        <?php while($product = mysqli_fetch_assoc($venproductquery)): 
		                                        
	                                            $vendorid = $product['vendor_id'];
		                                        $shopnamequery = $db->query("SELECT * FROM vendors WHERE vendor_id = $vendorid");
		                                        $shopname = mysqli_fetch_assoc($shopnamequery);

		                                        $brandid = $product['brand_id'];
		                                        $brandname = $db->query("SELECT * FROM brands WHERE id = '$brandid' ");
		                                        $brand = mysqli_fetch_assoc($brandname);

		                                       	$childID = $product['category_id'];
												$result = $db->query("SELECT * FROM categories Where id = $childID");
												$child = mysqli_fetch_assoc($result);
												$parentID = $child['parent'];
												$presult = $db->query("SELECT * FROM categories WHERE id = '$parentID' ");
												$parent = mysqli_fetch_assoc($presult);
												$category = $parent['category_name'].'-'.$child['category_name'];


		                                        ?>
		                                        <tr class="tablerow" id="pro<?=$product['id'];?>">
		                                        	<td><?=$product['id'];?></td>
		                                        	<td><?=$product['vendor_id'];?></td>
		                                        	<td><?=$shopname['shop_name'];?></td>
		                                        	<td><?=$product['title'];?></td>
		                                        	<td><?=money($product['price']);?></td>
		                                        	<td><?=$brand['brand_name'];?></td>
		                                        	<td><?=$category;?></td>
		                                        	<?php $photos = explode(',',$product['product_image']);?>
		                                        	<td><img style="height: 70px; width: 70px; border-radius: 50%;" src="<?=$photos[0];?>" class="img-responsive"></td>
		                                        	<td>
		                                        	<button class="btn btn-xs btn-danger" onclick="restoreproduct(<?=$product['id'];?>);return false;"><i class="material-icons">create</i></span></i></button>
		                                        	</td>
		                                        </tr>
		                                    	<?php endwhile; ?>
		                                    </tbody>
		                                </table>
		                               	
		                               	<?php endif; ?>		
		                            </div>
		                        </div>
							</div>
	                    </div>
	                </div>
	            </div>
	        </div>

<?php include'includes/footer.php'; ?>

<script type="text/javascript">

	function restoreproduct(product_id){

		    jQuery.ajax({
					url : '/multivendor/dashboard/parser/ajax.php',
					method : 'POST',
					data : {product_id: product_id, restore_product : 1},
		    		success : function(data){
					
					// alert(data); 
					//  var result = data;
					 var result = JSON.parse(data);
					 	if( result.status == "success"){
					 	    $("#pro"+ result.product_id).fadeOut("slow", function() { $(this).remove(); });
					 	}else{
					 		alert("something went row");
					 	}
					 },
					// error : function(){ alert("something went wrong");}   
				}); 
		}

</script>