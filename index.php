<?php 
require'core/db.php';
include'includes/header.php';

?>	
					
					<!-- main-slider -->
					<div class="container-fluid no-padding-lf">
						<div class="row">
							<div class="col-md-12 no-padding-lf">
								<div class="col-md-12 no-padding-lf">
									<div id="myCarousel" class="carousel slide main-slider" data-ride="carousel" data-interval="3000">
      									<!-- Indicators -->
								      <div class="carousel-inner main-carousel-inner" role="listbox">
								        <div class="item active">
								          <img class="first-slide main-img" src="img/images/5.jpg" alt="First slide">
								          <div class="container">
								            <div class="carousel-caption carousel-caption-main">
								              <h1 class="caption-small-head">Small heading</h1>
								              <h1 class="caption-head">Example headline</h1>
								              <button class="button button--shikoba button--border-thin"><i class="button__icon icon fa fa-rocket" aria-hidden="true"></i><span> &nbsp EXPLORE</span></button>
								            </div>
								          </div>
								        </div>
								        <div class="item">
								          <img class="second-slide main-img" src="img/images/6.jpg" alt="Second slide">
								          <div class="container">
								            <div class="carousel-caption carousel-caption-main">
								              <h1 class="caption-small-head">Small heading</h1>
								              <h1 class="caption-head">Another headline</h1>
								              <button class="button button--shikoba button--round-s button--border-thin"><i class="button__icon icon fa fa-rocket" aria-hidden="true"></i><span> EXPLORE</span></button>
								            </div>
								          </div>
								        </div>
								        <div class="item">
								          <img class="third-slide main-img" src="img/images/7.jpg" alt="Third slide">
								          <div class="container">
								            <div class="carousel-caption carousel-caption-main">
								              <h1 class="caption-small-head">Small heading</h1>
								              <h1 class="caption-head">Another measure</h1>
								              <button class="button button--shikoba button--round-s button--border-thin"><i class="button__icon icon fa fa-rocket" aria-hidden="true"></i><span> &nbsp EXPLORE</span></button>
								            </div>
								          </div>
								        </div>
								         <div class="item">
								          <img class="third-slide main-img" src="img/images/8.jpg" alt="Third slide">
								          <div class="container">
								            <div class="carousel-caption carousel-caption-main">
								              <h1 class="caption-small-head">Small heading</h1>
								              <h1 class="caption-head">Another Heading</h1>
								              <button class="button button--shikoba button--round-s button--border-thin"><i class="button__icon icon fa fa-rocket" aria-hidden="true"></i><span> &nbsp EXPLORE</span></button>
								            </div>
								          </div>
								        </div>
								        <div class="item">
								          <img class="third-slide main-img" src="img/images/9.jpg" alt="Third slide">
								          <div class="container">
								            <div class="carousel-caption carousel-caption-main">
								              <h1 class="caption-small-head">Small heading</h1>
								              <h1 class="caption-head">Another Heading</h1>
								              <button class="button button--shikoba button--round-s button--border-thin"><i class="button__icon icon fa fa-rocket" aria-hidden="true"></i><span> &nbsp EXPLORE</span></button>
								            </div>
								          </div>
								        </div>
								      </div>
								    </div>
								</div>
							</div>
						</div>
					</div>
					<!-- end -->

					<!-- category slider -->
					<section>
							<div class="wrapper">
					            <div class="jcarousel-wrapper">
					                <div class="jcarousel">
					                    <ul>
					                        <li>
					                        	<figure class="effect-layla">
													<img src="img/images/category/1.jpg" class="img-responsive" alt="img06"/>
													<figcaption>
														<h4>Mobile</h4>
														<p>When Layla appears, she brings an eternal summer along.</p>
														<a href="">View more</a>
													</figcaption>			
												</figure>	
											</li>
					                        <li>
					                        	<figure class="effect-layla">
													<img src="img/images/category/2.jpg" class="img-responsive" alt="img06"/>
													<figcaption>
														<h4>Watches</h4>
														<p>When Layla appears, she brings an eternal summer along.</p>
														<a href="javascript:void()">View more</a>
													</figcaption>			
												</figure>	
											</li>
					                        <li>
					                        	<figure class="effect-layla">
													<img src="img/images/category/3.jpg" class="img-responsive" alt="img06"/>
													<figcaption>
														<h4>Camera</h4>
														<p>When Layla appears, she brings an eternal summer along.</p>
														<a href="javascript:void()">View more</a>
													</figcaption>			
												</figure>	
											</li>
					                        <li>
					                        	<figure class="effect-layla">
													<img src="img/images/category/4.jpg" class="img-responsive" alt="img06"/>
													<figcaption>
														<h4>Shoes</h4>
														<p>When Layla appears, she brings an eternal summer along.</p>
														<a href="javascript:void()">View more</a>
													</figcaption>			
												</figure>	
											</li>
					                        <li>
					                        	<figure class="effect-layla">
													<img src="img/images/category/5.jpg" class="img-responsive" alt="img06"/>
													<figcaption>
														<h4>Bluetooth</h4>
														<p>When Layla appears, she brings an eternal summer along.</p>
														<a href="javascript:void()">View more</a>
													</figcaption>			
												</figure>	
											</li>
					                        <li>
					                        	<figure class="effect-layla">
													<img src="img/images/category/6.jpg" class="img-responsive" alt="img06"/>
													<figcaption>
														<h4>Tablets</h4>
														<p>When Layla appears, she brings an eternal summer along.</p>
														<a href="javascript:void()">View more</a>
													</figcaption>			
												</figure>	
											</li>
											<li>
					                        	<figure class="effect-layla">
													<img src="img/images/category/7.jpg" class="img-responsive" alt="img06"/>
													<figcaption>
														<h4>Smartwatches</h4>
														<p>When Layla appears, she brings an eternal summer along.</p>
														<a href="javascript:void()">View more</a>
													</figcaption>			
												</figure>	
											</li>
											<li>
					                        	<figure class="effect-layla">
												<img src="img/images/category/8.jpg" class="img-responsive" alt="img06"/>
												<figcaption>
													<h4>Fitness</h4>
													<p>When Layla appears, she brings an eternal summer along.</p>
													<a href="javascript:void()">View more</a>
												</figcaption>			
											</figure>	
											</li>
											<li>
					                        	<figure class="effect-layla">
												<img src="img/images/category/9.jpg" class="img-responsive" alt="img06"/>
												<figcaption>
													<h4>Laptop</h4>
													<p>When Layla appears, she brings an eternal summer along.</p>
													<a href="javascript:void()">View more</a>
												</figcaption>			
											</figure>	
											</li>
											<li>
					                        	<figure class="effect-layla">
												<img src="img/images/category/10.jpg" class="img-responsive" alt="img06"/>
												<figcaption>
													<h4>Travel</h4>
													<p>When Layla appears, she brings an eternal summer along.</p>
													<a href="javascript:void()">View more</a>
												</figcaption>			
											</figure>	
											</li>
											<li>
					                        	<figure class="effect-layla">
												<img src="img/images/category/11.jpg" class="img-responsive" alt="img06"/>
												<figcaption>
													<h4>Kids</h4>
													<p>When Layla appears, she brings an eternal summer along.</p>
													<a href="javascript:void()">View more</a>
												</figcaption>			
											</figure>	
											</li>
											<li>
					                        	<figure class="effect-layla">
												<img src="img/images/category/12.jpg" class="img-responsive" alt="img06"/>
												<figcaption>
													<h4>Speaker</h4>
													<p>When Layla appears, she brings an eternal summer along.</p>
													<a href="javascript:void()">View more</a>
												</figcaption>			
											</figure>	
											</li>
											<li>
					                        	<figure class="effect-layla">
												<img src="img/images/category/13.jpg" class="img-responsive" alt="img06"/>
												<figcaption>
													<h4>Gaming</h4>
													<p>When Layla appears, she brings an eternal summer along.</p>
													<a href="javascript:void()">View more</a>
												</figcaption>			
											</figure>	
											</li>
											<li>
					                        	<figure class="effect-layla">
												<img src="img/images/category/14.jpg" class="img-responsive" alt="img06"/>
												<figcaption>
													<h4>Accessories</h4>
													<p>When Layla appears, she brings an eternal summer along.</p>
													<a href="javascript:void()">View more</a>
												</figcaption>			
											</figure>	
											</li>
											<li>
											<figure class="effect-layla">
												<img src="img/images/category/15.jpg" class="img-responsive" alt="img06"/>
												<figcaption>
													<h4>Computer Accessories</h4>
													<p>When Layla appears, she brings an eternal summer along.</p>
													<a href="javascript:void()">View more</a>
												</figcaption>			
											</figure>
											</li>
					                    </ul>
					                </div>
					                <a href="#" class="jcarousel-control-prev " id="prev-one">&lsaquo;</a>
					                <a href="#" class="jcarousel-control-next " id="next-one">&rsaquo;</a>
					            </div>
					        </div>
					</section>
					
					<!-- end -->
					
					
					<div class="container-fluid big-product-section">
						<div class="row">
							<div class="col-xs-12 col-sm-12 showcase-main">
								<div class="col-xs-12 col-sm-6 landscape-width">
									<div class="col-xs-12 col-sm-12 col-md-11 col-lg-8 col-lg-offset-2 big-product">
										<div class="col-xs-12 col-xs-12 col-sm-12 big-product-inner">
											<img src="img/images/product-showcase/1.png" class="img-responsive">
											<div class="big-product-description-outer">
												<div class="big-product-description-inner">
													<h5>Product heading</h5>
													<h6>$60 - $120</h6>
													<div class="star-div"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></div>
													<h6>Lorem ipsum dolor sit amet, vix nemore laoreet an, mea et meliore insolens</h6>
												</div>
											</div>
											<div class="big-product-btn">
												<a class="button button--shikoba button--border-thin" href="categoryproduct.php"><i class="button__icon icon fa fa-shopping-cart" aria-hidden="true"></i><span> SHOP</span></a>
											</div>
										</div>
									</div>	
								</div>
								<div class="col-xs-12 col-sm-6 blog-box">
									<h1>Any Blogging heading</h1>
									<p>Lorem ipsum dolor sit amet, nec quas virtute ei, no amet quaerendum necessitatibus vis, errem definitiones ut per. Ei nec audiam reprimique, phaedrum</p>
									<div class="blog-image pull-right col-sm-12 col-md-9">
										<img src="img/images/emily-sea-198689.jpg" class="img-responsive">
									</div>
									<div class="blog-detail">
										<h5>Blog Heading</h5>
										<p>Lorem ipsum dolor sit amet, nec quas virtute ei, no amet quaerendum necessitatibus vis, errem</p>
										<img class="round img-responsive" src="http://0.gravatar.com/avatar/81b58502541f9445253f30497e53c280?s=50&amp;d=identicon&amp;r=G" alt="Sara Soueidan">
										<h6>Blogger name</h6>
										<a href="" class="button button--shikoba button--border-thin pull-right"><span class="font-14">Read More</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<section class="hero-section-main">
						<section>
								<div class="container-fluid ">
							<div class="row">
								<div class="col-xs-12 col-sm-12 hero-section">
								  	<h1>hit this summer by our new collection</h1>
								  	<p>Lorem ipsum dolor sit amet, his id simul partem legendos, suscipit expetenda consequuntur te nam. Brute labores eu sea.</p>
								  	<div class="col-xs-12 col-sm-12 text-center hero-buttons">
									  	<a class="button button--shikoba button--border-thin" href="categoryproduct.php"><i class="button__icon icon fa fa-shopping-cart" aria-hidden="true"></i><span> &nbsp MEN</span></a>
									  	<a class="button button--shikoba button--border-thin" href="categoryproduct.php" ><i class="button__icon icon fa fa-shopping-cart" aria-hidden="true"></i><span> &nbsp WOMEN</span></a>
									</div>
								</div>
							</div>
								</div>
								<div class="container-fluid">
							<div class="row">
								 <div class="wrapper">
						            <div class="jcarousel-wrapper">
						                <div class="jcarousel">
						                    <ul>
						                        <li>
						                        	<a href="singleproduct.php">
							                        	<img src="img/products/laptop/3.jpg" alt="Image 1">
							                        	<div class="product-details">
								                        	<h3> ₹ 12,000 </h3>
								                        	<h4> Apple Laptop </h4>
								                        	<h5> Electronics - Laptops </h5>
							                        	</div>
						                        	</a>
						                        </li>
						                        <li>
						                        	<a href="singleproduct.php">
						                        	<img src="img/products/datacable/1.jpg" alt="Image 2">
						                        	<div class="product-details">
							                        	<h3> ₹ 650 </h3>
							                        	<h4> Mobile Datacable </h4>
							                        	<h5> Mobile - Accessories  </h5>
						                        	</div>
						                        	</a>

						                        </li>
						                        <li>
						                        	<a href="singleproduct.php">

						                        	<img src="img/products/headphone/1.jpg" alt="Image 3">
						                        	<div class="product-details">
							                        	<h3> ₹ 3,000 </h3>
							                        	<h4> Beats Headphone </h4>
							                        	<h5> Electronics - Headphones </h5>
						                        	</div>
													</a>
						                        </li>
						                        <li>
						                        	<a href="singleproduct.php">
						                        	<img src="img/products/phonecover/5/1.jpg" alt="Image 4">
													<div class="product-details">
							                        	<h3> ₹ 900 </h3>
							                        	<h4> Iphone 7 Cover </h4>
							                        	<h5> Mobile - Accessories </h5>
						                        	</div>
						                        </a>
						                        </li>
						                        <li>
						                        	<img src="img/products/shoes/4/12.jpg" alt="Image 5">
						                        	<div class="product-details">
							                        	<h3> ₹ 9,000 </h3>
							                        	<h4> Nike Air </h4>
							                        	<h5> Fitness ~ Shoes </h5>
						                        	</div>
						                        </li>
						                        <li>
						                        	<a href="singleproduct.php">
						                        	<img src="img/products/xbox/1.jpg" alt="Image 6">
						                        	<div class="product-details">
							                        	<h3> ₹ 37,000 </h3>
							                        	<h4> Xbox - One </h4>
							                        	<h5> Electronics - Games </h5>
						                        	</div>
						                        	</a>
						                        </li>
						                    </ul>
						                </div>
						                <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
						                <a href="#" class="jcarousel-control-next">&rsaquo;</a>
						            </div>
						            <div class="explore-btn col-sm-12">
						            	<a class="button button--shikoba button--border-thin pull-right" href="categoryproduct.php"><i class="button__icon icon fa fa-shopping-cart" aria-hidden="true"></i><span> &nbsp EXPLORE MORE</span></a>
						            </div>
						        </div>
							</div>
								</div>
						</section>
					</section>
					<section class="servive-and-slider-box">
						<section class="male-female-slider">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="col-xs-12 col-sm-5 col-md-6 col-lg-3 no-pad-r pad-lf-15 width-30 men-box-xs">
										<div class="col-xs-12 col-sm-12 col-md-12 text-center new-in-header">
											<p class="new-in-label">MEN</p>
											<h4><i class="fa fa-minus" aria-hidden="true"></i><strong> NEW IN </strong><i class="fa fa-minus" aria-hidden="true"></i></h4>
										</div>
										<img src="img/images/2.jpg" class="img-responsive">
									</div>
									<div class="col-xs-12 col-sm-7 col-md-6 col-lg-9 no-pad-l pad-lf-15 margin-minus-xs male-female-land women-box-lower-xs">
										<div class="wrapper azure-back">
								            <div class="jcarousel-wrapper margin-0">
								                <div class="jcarousel1">
								                    <ul>
							                        <?php for($i = 1; $i <= 7; $i++): ?>
								                        <li class="border-left">
							                        	<a href="singleproduct.php">
								                        	<img src="img/products/men/<?=$i;?>.jpg" alt="Image 1" class="img-responsive">
								                        	<div class="new-in-product-details">
									                        	<h3> ₹ 12,000 </h3>
									                        	<h4> Mens Fashion </h4>
									                        	<div class="new-in-star-div"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></div>
									                        	<h5> Mens - Fashion </h5>
								                        	</div>
								                    	</a>
								                        </li>
								                    <?php endfor; ?>
								                    </ul>
								                </div>
								                <a href="#" class="jcarousel-control-prev prev1">&lsaquo;</a>
								                <a href="#" class="jcarousel-control-next next1">&rsaquo;</a>
								            </div>
								        </div>
									</div>
									<a class="button button--shikoba button--border-thin new-in-explore-btn" href="categoryproduct.php"><i class="button__icon icon fa fa-shopping-cart" aria-hidden="true"></i><span> &nbsp EXPLORE MORE</span></a>
								</div>
							</div>
						</section>
						<section class="male-female-slider">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
									<div class="col-xs-12 col-sm-7 col-md-6 col-lg-9 no-pad-r pad-lf-15 male-female-land women-box-lower-xs">
										<div class="wrapper azure-back">
								            <div class="jcarousel-wrapper margin-0">
								                <div class="jcarousel11">
								                    <ul>
								                    <?php for($i = 1; $i <= 8; $i++): ?>
								                        <li>
								                        	<a href="singleproduct.php">
								                        	<img src="img/products/women/<?=$i;?>.jpg" alt="Image 1" class="img-responsive">
								                        	<div class="new-in-product-details">
									                        	<h3> ₹ 3,000 </h3>
									                        	<h4>  Women Fashion </h4>
									                        	<div class="new-in-star-div"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></div>
									                        	<h5> Female ~ Fashion </h5>
								                        	</div>
								                        	</a>
								                        </li>
								                    <?php endfor; ?>
								                    </ul>
								                </div>
								                <a href="#" class="jcarousel-control-prev prev1">&lsaquo;</a>
								                <a href="#" class="jcarousel-control-next next1">&rsaquo;</a>
								            </div>
								        </div>
									</div>
									<div class="col-xs-12 col-sm-5 col-md-6 col-lg-3 no-pad-l pad-lf-15 margin-t-20 width-30 women-box-xs">
										<div class="col-xs-12 col-sm-12 col-md-12 text-center new-in-header">
											<p class="new-in-label">WOMEN</p>
											<h4><i class="fa fa-minus" aria-hidden="true"></i><strong> NEW IN </strong><i class="fa fa-minus" aria-hidden="true"></i></h4>
										</div>
										<img src="img/images/1.jpg" class="img-responsive">
									</div>
									<a class="button button--shikoba button--border-thin new-in-explore-btn-female" href="categoryproduct.php"><i class="button__icon icon fa fa-shopping-cart" aria-hidden="true"></i><span> &nbsp EXPLORE MORE</span></a>
								</div>
							</div>
						</section>
							<div class="container-fluid service-box-main">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12">
										<div class="col-xs-12 col-sm-6 col-md-3 service-box width-50">
												<div class="col-xs-4 no-padding-l col-sm-5 col-md-4">
													<img src="img/free-delivery.png" class="img-responsive">
												</div>
												<h3 class="col-xs-8 col-sm-7 col-md-12">Shipping Worldwide </h3>
												<p class="col-xs-8 col-sm-7 col-md-12">Lorem ipsum dolor sit amet, et sea aliquam tractatos</p>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-3 service-box width-50">
												<div class="col-xs-4 no-padding-l col-sm-5 col-md-4">
													<img src="img/customer-service.png" class="img-responsive">
												</div>
												<h3 class="col-xs-8 col-sm-7 col-md-12">24/7 Support Online</h3>
												<p class="col-xs-8 col-sm-7 col-md-12">Lorem ipsum dolor sit amet, et sea aliquam tractatos</p>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-3 service-box width-50">
												<div class="col-xs-4 no-padding-l col-sm-5 col-md-4">
													<img src="img/credit-card.png" class="img-responsive">
												</div>
												<h3 class="col-xs-8 col-sm-7 col-md-12">Secure Checkout</h3>
												<p class="col-xs-8 col-sm-7 col-md-12">Lorem ipsum dolor sit amet, et sea aliquam tractatos</p>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-3 service-box width-50">
												<div class="col-xs-4 no-padding-l col-sm-5 col-md-4">
													<img src="img/return.png" class="img-responsive">
												</div>
												<h3 class="col-xs-8 col-sm-7 col-md-12">30 Days Return</h3>
												<p class="col-xs-8 col-sm-7 col-md-12">Lorem ipsum dolor sit amet, et sea aliquam tractatos</p>
										</div>
									</div>
								</div>
							</div>
					</section>
					<section class="subs-back">
						<div class="container-fluid no-padding-lf opacity-back">
							<div class="container subscribe-section">
							<div class="row">
								<nav class="navbar trans-back" role="navigation">
								    <!-- Brand and toggle get grouped for better mobile display -->
								    
								    <div class="navbar-header">
								      <button type="button" class="navbar-toggle toggle-btn" data-toggle="collapse" data-target="#navbar-collapse-1">
								        <span class="sr-only">Toggle navigation</span>
								        <span class="icon-bar fa fa-minus"></span>
								        <span class="icon-bar fa fa-minus"></span>
								      </button>
								      
								    </div>
								    <!-- Collect the nav links, forms, and other content for toggling -->
								    <div class="collapse navbar-collapse" id="navbar-collapse-1">
								     
								      <ul class="nav navbar-nav navbar-left">
								        <li><a href="#">HELP</a></li>
								        <li><a href="#">ORDER STATUS</a></li>
								        <li><a href="#">SHIPPING</a></li>
								        <li><a href="#">CONTACT US</a></li>
								      </ul>
								      <ul class="nav navbar-nav navbar-right">
								        <li><a href="#">ABOUT US</a></li>
								        <li><a href="#">VENDORS</a></li>
								        <li><a href="singleproduct.php">SHOP</a></li>
								        <li><a href="#">PRIVACY AND POLICY</a></li>
								      </ul>
								    </div><!-- /.navbar-collapse -->
								</nav>
								<div class="col-xs-2 col-xs-offset-5 line">
									<img src="img/line.png" class="img-responsive">
								</div>
								<div class="col-xs-12 text-center social-tab">
									<i class="fa fa-facebook-square fa-3x white-color" aria-hidden="true"></i>
									<i class="fa fa-pinterest-square fa-3x white-color" aria-hidden="true"></i>
									<i class="fa fa-twitter-square fa-3x white-color" aria-hidden="true"></i>
									<i class="fa fa-youtube-square fa-3x white-color" aria-hidden="true"></i>
									<i class="fa fa-instagram fa-3x white-color" aria-hidden="true"></i> 
								</div>
								<div class="col-xs-2 col-xs-offset-5 line">
									<img src="img/line.png" class="img-responsive">
								</div>
								<div class="col-xs-12 text-center">
									<h3 class="subs-title">SUBSCRIBE FOR NEW PRODUCTS AND NEW NEWSLETTER</h3>
								</div>
								<div class="col-xs-12 text-center">
									<span class="input input--kuro">
										<input class="input__field input__field--kuro" type="text" id="input-7" />
										<label class="input__label input__label--kuro" for="input-7">
											<span class="input__label-content input__label-content--kuro">ENTER EMAIL ADDRESS</span>
										</label>
									</span>
								</div>

							</div>
							</div>
						</div>
					</section>
					<?php include'includes/footer.php';?>
				