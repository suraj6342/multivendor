<div class="panel-group">
  <div class="panel panel-default border-bottom">
    <div class="panel-heading no-border">
      <h4 class="panel-title border" data-toggle="collapse" href="#collapse1">
        Category <i class="caret"></i>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <ul class="list-group list">
        <li class="list-group-item list">Computers </li>
        <li class="list-group-item list">Men's Fashion</li>
        <li class="list-group-item list">Women Fashion</li>
        <li class="list-group-item list">Laptop</li>
        <li class="list-group-item list">Headphones</li>
        <li class="list-group-item list">Kids Fashion</li>
        <li class="list-group-item list">Footwear</li>
      </ul>
    </div>
  </div>
</div>
<h4 class="pad-left-15 no-bor">Filters</h4>
<div class="panel-group">
  <div class="panel panel-default border-bottom">
    <div class="panel-heading no-border ">
      <h4 class="panel-title border no-bor" data-toggle="collapse" href="#collapse2">
        Size <i class="caret"></i>
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
      <ul class="list-group">
        <li class="list-group-item list">XS </li>
        <li class="list-group-item list">SM</li>
        <li class="list-group-item list">lg</li>
        <li class="list-group-item list">xl</li>
        <li class="list-group-item list">xx-xl</li>
        <li class="list-group-item list">xxx-xl</li>
      </ul>
    </div>
  </div>
</div>
<div class="panel-group">
  <div class="panel panel-default border-bottom">
    <div class="panel-heading no-border">
      <h4 class="panel-title border no-bor" data-toggle="collapse" href="#collapse3">
        Colors <i class="caret"></i>
      </h4>
    </div>
    <div id="collapse3" class="panel-collapse collapse">
      <ul class="list-group">
        <li class="list-group-item list">One </li>
        <li class="list-group-item list">Two</li>
        <li class="list-group-item list">Three</li>
      </ul>
    </div>
  </div>
</div>
<div class="panel-group">
  <div class="panel panel-default border-bottom">
    <div class="panel-heading no-border">
      <h4 class="panel-title border no-bor" data-toggle="collapse" href="#collapse4">
        Brand <i class="caret"></i>
      </h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse">
      <ul class="list-group">
        <li class="list-group-item list">One </li>
        <li class="list-group-item list">Two</li>
        <li class="list-group-item list">Three</li>
      </ul>
    </div>
  </div>
</div>
<div class="panel-group">
  <div class="panel panel-default border-bottom">
    <div class="panel-heading no-border">
      <h4 class="panel-title border no-bor" data-toggle="collapse" href="#collapse5">
        Price <i class="caret"></i>
      </h4>
    </div>
    <div id="collapse5" class="panel-collapse collapse">
      <ul class="list-group">
        <li class="list-group-item list">One </li>
        <li class="list-group-item list">Two</li>
        <li class="list-group-item list">Three</li>
      </ul>
    </div>
  </div>
</div>