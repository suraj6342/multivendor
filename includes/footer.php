<footer style="padding:25px 15px; border-top: 1px solid #000; background-color: #2c3e50; color: white;" class="text-center" id="footer">&copy; Copyright 20<?=date("y");?> Multivendor</footer>



			</div><!-- /st-content-inner -->
		</div><!-- /st-content -->
	</div><!-- /st-pusher -->
</div><!-- /st-container -->
<script src="js/ajaxquery.js"></script>
<script src="js/classie.js"></script>
<script src="js/sidebarEffects.js"></script>
<script src="js/selectFx.js"></script>
<!-- <script type="text/javascript">
   	(function(){
       	$(".signup-a").click(function() {
	 		$(".modal-left-container").addClass("transform");
		});
	})();
</script> -->
<script>
	(function() {
		[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
			new SelectFx(el);
		} );
	})();
</script>

<script type="text/javascript">
(function() {
	$(".shop").mouseenter(function(){
		$(".drop-menu-list").slideDown("slow");    
	});
	$("#inner-container").click(function(){
		$(".drop-menu-list").slideUp("slow");    
	});	
})();
</script>
<script type="text/javascript">
					(function() {
				var morphSearch = document.getElementById( 'morphsearch' ),
					input = morphSearch.querySelector( 'input.morphsearch-input' ),
					ctrlClose = morphSearch.querySelector( 'span.morphsearch-close' ),
					isOpen = isAnimating = false,
					// show/hide search area
					toggleSearch = function(evt) {
						// return if open and the input gets focused
						if( evt.type.toLowerCase() === 'focus' && isOpen ) return false;

						var offsets = morphsearch.getBoundingClientRect();
						if( isOpen ) {
							classie.remove( morphSearch, 'open' );

							// trick to hide input text once the search overlay closes 
							// todo: hardcoded times, should be done after transition ends
							if( input.value !== '' ) {
								setTimeout(function() {
									classie.add( morphSearch, 'hideInput' );
									setTimeout(function() {
										classie.remove( morphSearch, 'hideInput' );
										input.value = '';
									}, 300 );
								}, 500);
							}
							
							input.blur();
						}
						else {
							classie.add( morphSearch, 'open' );
						}
						isOpen = !isOpen;
					};

				// events
				input.addEventListener( 'focus', toggleSearch );
				ctrlClose.addEventListener( 'click', toggleSearch );
				// esc key closes search overlay
				// keyboard navigation events
				document.addEventListener( 'keydown', function( ev ) {
					var keyCode = ev.keyCode || ev.which;
					if( keyCode === 27 && isOpen ) {
						toggleSearch(ev);
					}
				} );


				/***** for demo purposes only: don't allow to submit the form *****/
				morphSearch.querySelector( 'button[type="submit"]' ).addEventListener( 'click', function(ev) { ev.preventDefault(); } );
			})();

	</script>
		<script>
		(function() {
			[].slice.call(document.querySelectorAll('.menu')).forEach(function(menu) {
				var menuItems = menu.querySelectorAll('.menu__link'),
					setCurrent = function(ev) {
						ev.preventDefault();

						var item = ev.target.parentNode; // li

						// return if already current
						if (classie.has(item, 'menu__item--current')) {
							return false;
						}
						// remove current
						classie.remove(menu.querySelector('.menu__item--current'), 'menu__item--current');
						// set current
						classie.add(item, 'menu__item--current');
					};

				// [].slice.call(menuItems).forEach(function(el) {
				// 	el.addEventListener('click', setCurrent);
				// });
			});

			// [].slice.call(document.querySelectorAll('.link-copy')).forEach(function(link) {
			// 	link.setAttribute('data-clipboard-text', location.protocol + '//' + location.host + location.pathname + '#' + link.parentNode.id);
			// 	new Clipboard(link);
			// 	link.addEventListener('click', function() {
			// 		classie.add(link, 'link-copy--animate');
			// 		setTimeout(function() {
			// 			classie.remove(link, 'link-copy--animate');
			// 		}, 300);
			// 	});
			// });
		})(window);
		</script>
		<script type="text/javascript">
			(function() {
				    $('.jcarousel11')
				        .jcarousel({
				            // Core configuration goes here
				        })
				        .jcarouselAutoscroll({
				            interval: 1000000,
				            target: '+=1',
				            autostart: true
				        });
				});
		</script>

		<script type="text/javascript">
			(function() {
				    $('.jcarousel')
				        .jcarousel({
				            // Core configuration goes here
				        })
				        .jcarouselAutoscroll({
				            interval: 1000000,
				            target: '+=1',
				            autostart: true
				        });
				});

			(function() {
				    $('.jcarousel1')
				        .jcarousel({
				            // Core configuration goes here
				        })
				        .jcarouselAutoscroll({
				            interval: 1000000,
				            target: '+=1',
				            autostart: true
				        });
				});

		</script>



		<script type="text/javascript">
		(function() {
			$(".sign-button").click(function(){
			  var buttonId = $(this).attr("id");
			  $("#st-container").removeClass("st-menu-open");
			  $("#modal-container").removeAttr("class").addClass(buttonId);
			  $("body").addClass("modal-active");
			})

			$(".close-span").click(function(){
			  $("#modal-container").addClass("out");
			  $("body").removeClass("modal-active");
			});
			$(".close-span1").click(function(){
			  $("#modal-container").addClass("out");
			  $("body").removeClass("modal-active");
			});
		
		})();
		</script>

		<script type="text/javascript">
			$("#change-tab").click(function(){
	        	$(".modal-left-container").css("animation","mymove 1s");
	            $(".modal-left-container").css("animation-fill-mode","forwards");
	            $(".modal-left-container").css("-webkit-animation","mymove 1s");
	            $(".modal-left-container").css("-webkit-animation-fill-mode","forwards");
	            $(".close-span1").addClass("display-b");
				$("#slide-span").addClass("display-b");

	        });
	        $("#slide-span").click(function(){
	        	$(".modal-left-container").css("animation","backmove 1s");
	            $(".modal-left-container").css("animation-fill-mode","forwards");
	            $(".modal-left-container").css("-webkit-animation","backmove 1s");
	            $(".modal-left-container").css("-webkit-animation-fill-mode","forwards");
	            $(".close-span1").removeClass("display-b");
	            $("#slide-span").removeClass("display-b");
	        }); 

	    </script>
		<!-- sidbar menu script -->
		<script type="text/javascript">
			$.sidebarMenu = function(menu) {
			  var animationSpeed = 300;
			  
			  $(menu).on('click', 'li a', function(e) {
			    var $this = $(this);
			    var checkElement = $this.next();

			    if (checkElement.is('.treeview-menu') && checkElement.is(':visible')) {
			      checkElement.slideUp(animationSpeed, function() {
			        checkElement.removeClass('menu-open');
			      });
			      checkElement.parent("li").removeClass("active");
			    }

			    //If the menu is not visible
			    else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
			      //Get the parent menu
			      var parent = $this.parents('ul').first();
			      //Close all open menus within the parent
			      var ul = parent.find('ul:visible').slideUp(animationSpeed);
			      //Remove the menu-open class from the parent
			      ul.removeClass('menu-open');
			      //Get the parent li
			      var parent_li = $this.parent("li");

			      //Open the target menu and add the menu-open class
			      checkElement.slideDown(animationSpeed, function() {
			        //Add the class active to the parent li
			        checkElement.addClass('menu-open');
			        parent.find('li.active').removeClass('active');
			        parent_li.addClass('active');
			      });
			    }
			    //if this isn't a link, prevent the page from being redirected
			    if (checkElement.is('.treeview-menu')) {
			      e.preventDefault();
			    }
			  });
			}

			$.sidebarMenu($('.sidebar-menu'))
		</script>
		<!-- profile dropdown -->
		<script type="text/javascript">
			$('.profile-dropdown-title').click(function(){
				$('.dropdown-ul').slideToggle();
			});
			
		</script>
		<script type="text/javascript">
			$(".signup-btn").on("click", function() {
			    $(".alert").removeClass("in").show();
				$(".alert").delay(200).addClass("in").fadeOut(2000);
			});
		</script>
		<script type="text/javascript">
		$('#user-drop').click(function(e){
				e.preventDefault();
				$('.user-dropdown').slideToggle();
		});
		</script>
		<script type="text/javascript">
			$('#alert-close').click(function(e){
				 e.preventDefault();
				$('.alert-back').addClass("hidden");
			});

		</script>
		<script>
            $(function () {
                $('#signup-btn').click(function () {
                    var fullname = $('#signup-name').val();
					var email = $('#signup-email').val();
					var pass = $('#signup-pass').val();
					var repass = $('#signup-repass').val();
					var mobile = $('#signup-mobile').val();

                    var flag_signup = 1;
                    $.ajax({
                        url: '/multivendor/parsers/signup-ajax.php',
                        data: {"fullname" : fullname, "email": email, "pass": pass, "repass": repass, "mobile": mobile,},
                        type: 'POST',
                        success: function (data) {
                        	
                        	
                        	var count = data;
                        	if (count == 1) {
                        		location.reload();
                        	}else{
                        		$('.message').html(count);	
                        	}
                        	//alert-close
                        	$('#alert-close').click(function(){
								$('.message').html(" ");
							});
                        },
                        error: function(data){
                        	alert(data);
                        }

                    });
                    
                });
            });
</script>
 <script>
            $(function () {
                $('#login-btn').click(function () {
                    var email = $('#user-email').val();
					var pass = $('#user-pass').val();
					
                    
                    $.ajax({
                        url: '/multivendor/parsers/login-ajax.php',
                        data: {"login_email" : email, "login_pass": pass},
                        type: 'POST',
                        success: function (data) {
                        	
                        	
                        	var logincount = data;
                        	if (logincount == 1) {
								location.reload();
                        	}else{
                        		$('.message').html(logincount);	
                        	}
                        	//alert-close
                        	$('#alert-close').click(function(){
								$('.message').html(" ");
							});
                        },
                        error: function(data){
                        	alert(data);
                        }

                    });
                    
                });
            });
</script>

<script type="text/javascript">
    // $('.jcarousel1').jcarousel({

    // })
    // .jcarouselAutoscroll({
    //     interval: 1000000,
    //     target: '+=1',
    //     autostart: true
    // });

    // $('.jcarousel11').jcarousel({
    //     // Core configuration goes here
    // })
    // .jcarouselAutoscroll({
    //     interval: 1000000,
    //     target: '+=1',
    //     autostart: true
    // });
</script>


<script type="text/javascript">
  $(window).bind("load", function () {
      $('#work-in-progress').fadeOut(100);
  });
</script>

</body>
</html>