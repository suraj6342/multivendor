<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Multivendor Store</title>
	<meta name="description" content="" />
	<meta name="keywords" content="Multivendor php theme, ecommerce php script" />
	<meta name="author" content="Suraj Singh Rawat" />
	<link rel="shortcut icon" href="img/logo.png">

	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.2.0/css/font-awesome.min.css" />
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/jcarousel.responsive.css">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="js/tether.min.js" ></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/modernizr.custom.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.min.js"></script>
    <script type="text/javascript" src="js/jcarousel.responsive.js"></script>
    <script type="text/javascript" src="js/jquery.jcarousel-autoscroll.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script type="text/javascript" src="js/InjectedScriptSource.js"></script>
    <script src="js/main.js"></script>

</head>
<body>


<!-- progress bar -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 
<div id="work-in-progress">
  <div class="lds-css ng-scope">
    <div class="lds-eclipse">
      <div></div>
    </div>
  </div>
</div>


<?php 

if (isset($_SESSION['SBUser']))
{
    $user_id = $_SESSION['SBUser'];
    $userquery = $db->query("SELECT * From users WHERE id = '$user_id' ");
    $user = mysqli_fetch_assoc($userquery);

}

?>
<div id="st-container" class="st-container">
    <nav class="st-menu st-effect-2" id="menu-2">
        <?php if (isset($_SESSION['SBUser'])) {?>
        <?php }else{?>     
            <h6 class="icon icon-stack button float-unset sign-button" id="one">SIGN IN OR JOIN</h6>
        <?php } ?>        
        <div class="row margin-lf-15-xs">
            <div class="col-xs-3 profile-img">
                <img src="img/man1.png" class="img-responsive">
            </div>
            <div class="col-xs-9 profile-dropdown-title">
                <?php if (isset($_SESSION['SBUser'])) {?>
                    <h4><?=$user['full_name'];?> </h4> <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                <?php }else{?>     
                    <h4>User Name </h4> <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                <?php } ?> 

            </div>
            <div class="col-xs-3"></div>
            <div class="col-xs-9 profile-dropdown">
                <ul class="dropdown-ul">
                    <li><a href="#">Account</a> <i class="fa fa-cogs pull-right" aria-hidden="true"></i></li>
                    <li><a href="#"> Orders</a><i class="fa fa-shopping-cart" aria-hidden="true"></i></li>
                    <li><a href="#"> Wishlist</a><i class="fa fa-heart" aria-hidden="true"></i></li>
                    <li><a href="logout.php"> Logout</a><i class="fa fa-sign-out" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <ul class="sidebar-menu">
          <li class="treeview">
            <a href="#">
              <span>Charts</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"> ChartJS</a></li>
              <li><a href="#"> Morris</a></li>
              <li><a href="#"> Flot</a></li>
              <li><a href="#"> Inline charts</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
            <span>UI Elements</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"> General</a></li>
              <li><a href="#"> Icons</a></li>
              <li><a href="#"> Buttons</a></li>
              <li><a href="#"> Sliders</a></li>
              <li><a href="#"> Timeline</a></li>
              <li><a href="#"> Modals</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <span>Forms</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"> General Elements</a></li>
              <li><a href="#"> Advanced Elements</a></li>
              <li><a href="#"> Editors</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <span>Tables</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"> Simple tables</a></li>
              <li><a href="#"> Data tables</a></li>
            </ul>
          </li>
            <li><a href="/multivendor/"></i> <span>Articles</span></a></li>
            <li><a href="#"><span>Reviews</span></a></li>
            <li><a href="categoryproduct.php"><span>Shop</span></a></li>
            <li><a href="#"><span>Contact</span></a></li>
            <li><a href="cart.php"><span>Cart</span></a></li>
        </ul>
    </nav>
    





    <!-- content push wrapper -->
    <div class="st-pusher">
        <div class="st-content"><!-- this is the wrapper for the content -->
            <div class="st-content-inner" id="inner-container"><!-- extra div for emulating position:fixed of the menu -->
                    <!-- maincontent -->
                    <div class="text-center top-header hidden-sm hidden-xs">
                        <div class="pull-left"><span><strong> USA EN </strong></span> | <span><strong> STORE LOCATION </strong></span> | <span><strong>HELP <span class="caret"></span></strong></span></div>
                        <span><strong> FREE SHIPPING & RETURNS *</strong></span>
                        <div class="pull-right">
                        <?php if (isset($_SESSION['SBUser'])) {?>
                                <i class="fa fa-user" aria-hidden="true"></i><span id="user-drop" class="pad-tb-0 p-cursor"> <?=$user['full_name'];?></span><span class="caret"></span>
                        <?php }else{?>     
                                <span id="one" class="sign-button pad-tb-0 p-cursor">SIGN IN OR JOIN</span>
                        <?php } ?>
                        
                        </div>
                        <div class="user-dropdown">
                            <ul class="uer-dropdown">
                                <li><a href="#">Account</a> <i class="fa fa-cogs pull-right" aria-hidden="true"></i></li>
                                <li><a href="#"> Orders</a><i class="fa fa-shopping-cart pull-right" aria-hidden="true"></i></li>
                                <li><a href="#"> Wishlist</a><i class="fa fa-heart pull-right" aria-hidden="true"></i></li>
                                <li><a href="logout.php"> Logout</a><i class="fa fa-sign-out pull-right" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="navbar">
                            <aside class="sidebar clearfix">
                                <nav>
                                    <div class="main clearfix hidden-md hidden-lg">
                                        <div id="st-trigger-effects" class="column">
                                            <button data-effect="st-effect-2" class="fa fa-bars bar-btn" aria-hidden="true"></button>
                                        </div>
                                    </div><!-- /main -->
                                    
                                </nav>
                            </aside>
                            <div id="morphsearch" class="morphsearch">
                                <form class="morphsearch-form">
                                    <input class="morphsearch-input" type="search" placeholder="Search..."/>
                                    <button class="morphsearch-submit" type="submit">Search</button>
                                </form>
                                <div class="morphsearch-content">
                                    <div class="dummy-column">
                                        <h2>People</h2>
                                        <a class="dummy-media-object" href="">
                                            <img class="round" src="http://0.gravatar.com/avatar/81b58502541f9445253f30497e53c280?s=50&d=identicon&r=G" alt="Sara Soueidan"/>
                                            <h3>Sara Soueidan</h3>
                                        </a>
                                        <a class="dummy-media-object" href="">
                                            <img class="round" src="http://0.gravatar.com/avatar/48959f453dffdb6236f4b33eb8e9f4b7?s=50&d=identicon&r=G" alt="Rachel Smith"/>
                                            <h3>Rachel Smith</h3>
                                        </a>
                                        <a class="dummy-media-object" href="">
                                            <img class="round" src="http://0.gravatar.com/avatar/06458359cb9e370d7c15bf6329e5facb?s=50&d=identicon&r=G" alt="Peter Finlan"/>
                                            <h3>Peter Finlan</h3>
                                        </a>
                                        <a class="dummy-media-object" href="">
                                            <img class="round" src="http://0.gravatar.com/avatar/48959f453dffdb6236f4b33eb8e9f4b7?s=50&d=identicon&r=G" alt="Rachel Smith"/>
                                            <h3>Rachel Smith</h3>
                                        </a>

                                        
                                    </div>
                                    <div class="dummy-column">
                                        <h2>Popular</h2>
                                        <a class="dummy-media-object" href="">
                                            <img src="img/thumbs/PagePreloadingEffect.png" alt="PagePreloadingEffect"/>
                                            <h3>Page Preloading Effect</h3>
                                        </a>
                                        <a class="dummy-media-object" href="">
                                            <img src="img/thumbs/ArrowNavigationStyles.png" alt="ArrowNavigationStyles"/>
                                            <h3>Arrow Navigation Styles</h3>
                                        </a>
                                        <a class="dummy-media-object" href="">
                                            <img src="img/thumbs/HoverEffectsIdeasNew.png" alt="HoverEffectsIdeasNew"/>
                                            <h3>Ideas for Subtle Hover Effects</h3>
                                        </a>
                                        <a class="dummy-media-object" href="">
                                            <img src="img/thumbs/HoverEffectsIdeasNew.png" alt="HoverEffectsIdeasNew"/>
                                            <h3>Ideas for Subtle Hover Effects</h3>
                                        </a>
                                        
                                    </div>
                                    <div class="dummy-column">
                                        <h2>Recent</h2>
                                        <a class="dummy-media-object" href="">
                                            <img src="img/thumbs/TooltipStylesInspiration.png" alt="TooltipStylesInspiration"/>
                                            <h3>Tooltip Styles Inspiration</h3>
                                        </a>
                                        <a class="dummy-media-object" href="">
                                            <img src="img/thumbs/AnimatedHeaderBackgrounds.png" alt="AnimatedHeaderBackgrounds"/>
                                            <h3>Animated Background Headers</h3>
                                        </a>
                                        <a class="dummy-media-object" href="">
                                            <img src="img/thumbs/OffCanvas.png" alt="OffCanvas"/>
                                            <h3>Off-Canvas Menu Effects</h3>
                                        </a>
                                        <a class="dummy-media-object" href="/">
                                            <img src="img/thumbs/OffCanvas.png" alt="OffCanvas"/>
                                            <h3>Off-Canvas Menu Effects</h3>
                                        </a>
                                        
                                    </div>
                                </div><!-- /morphsearch-content -->
                                <span class="morphsearch-close"></span>
                            </div><!-- /morphsearch -->
                                <header class="codrops-header inline">
                                    <h1><img src="img/logo.png" class="img-responsive "></h1>
                                </header>
                                <span class="link-copy"></span>
                                <nav class="menu menu--viola hidden-xs hidden-sm inline">
                                    <ul class="menu__list">
                                        <li class="menu__item menu__item--current "><a href="/multivendor/" class="menu__link">Home</a></li>
                                        <li class="menu__item"><a href="#" class="menu__link">Articles</a></li>
                                        <li class="menu__item"><a href="#" class="menu__link">Reviews</a></li>
                                        <li class="shop"><a href="categoryproduct.php" class="menu__link">Shop</a></li>
                                        <li class="menu__item"><a href="#" class="menu__link">Contact</a></li>
                                        <li class="menu__item"><a href="cart.php" class="menu__link"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart</a></li>
                                    </ul>
                                </nav>
                            <div class="overlay"></div>
                    </div><!-- /container -->
                    <div class="container-fluid drop-menu-list hidden-sm hidden-xs">
                       <div class="row">
                            <div class="col-md-4 no-padding-lf">
                                <img src="img/images/pete-bellis-262617.jpg" class="img-responsive">
                                <div class="little-product">
                                    <h6>EXPLORE</h6>
                                    <h5>WOMEN FASHION</h5>
                                </div>
                            </div>
                            <div class="col-md-8 product-category-box">
                                <?php for($i=1 ; $i <= 6 ; $i++): ?>
                                <div class="col-md-3">
                                    <br>
                                    <h4 class="textcenter footer-heads"><strong><a href="#" class="anchor-color"> Feauted Product </a></strong></h4>
                                    <li class="divider"></li><br>   
                                    <h6><a href="categoryproduct.php" class="anchor-color">New Arrivals</a></h6>
                                    <h6><a href="categoryproduct.php" class="anchor-color">Unique</a></h6>
                                    <h6><a href="categoryproduct.php" class="anchor-color">Sale</a></h6>
                                    <h6><a href="categoryproduct.php" class="anchor-color">Discounts</a></h6>
                                    <h6><a href="categoryproduct.php" class="anchor-color">Best Deals</a></h6> 
                                </div>
                                <?php endfor; ?>
                            </div>
                       </div>
                    </div>
                        
                    <!-- end -->
                   
                    <!-- login signup modal -->
                    <div id="modal-container">
                      <div class="modal-background">
                        <div class="modal modal-xs">
                        <div class="col-xs-12 modal-logo-img hidden-sm hidden-md hidden-lg">
                            <img src="img/logo.png" class="modal-logo-img">
                        </div>
                          <div class="modal-left-container">
                            <div class="col-sm-12 close-btn-1">
                                <span id="close-login-modal" class="close-span1 pull-right"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                            <div class="col-sm-12 text-center modal-img hidden-xs">
                                <img src="img/logo.png">
                            </div>
                            <div class="col-sm-12 modal-head">
                                <h1 class="modal-welcome-mes-xs">Welcome to Multivendor Shop Login to shop various Products</h1>
                                <button class="btn btn-xs pull-right slide-span" id="slide-span"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
                            </div>
                            
                          </div>
                          <div class="col-sm-6 modal-signup-container no-padding-lf">
                               <div class="col-xs-12 col-sm-12 login-form">
                                    <form id="signupform" method="post">    
                                        <div class="col-sm-10 col-sm-offset-2 login-head">
                                            <h1>Create your acoount</h1>
                                        </div>
                                         <div class="col-xs-12 col-sm-12 modal-input-border border-b-0">
                                            <div class="col-xs-2 col-sm-2 modal-input-label">
                                                <img src="img/user.png" class="img-responsive">
                                            </div>
                                            <div class="col-xs-10 col-sm-10 no-padding-lf">
                                                <input type="text" class="login-input border-b-0" name="signup-name" autocomplete="true" id="signup-name" value="<?=(isset($_SESSION["full_name"])?$_SESSION["full_name"]:'');?>" placeholder="enter full name..." autofocus="true" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 modal-input-border border-b-0">
                                            <div class="col-xs-2 col-sm-2 modal-input-label">
                                                <img src="img/user.png" class="img-responsive">
                                            </div>
                                            <div class="col-xs-10 col-sm-10 no-padding-lf">
                                                <input type="email" class="login-input border-b-0" name="signup-email" autocomplete="true" id="signup-email" value="<?=(isset($_SESSION["email"])?$_SESSION["email"]:'');?>" placeholder="email address..." autofocus="true" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 modal-input-border border-b-0">
                                            <div class="col-xs-2 col-sm-2 modal-input-label">
                                                <img src="img/locked.png" class="img-responsive">
                                            </div>
                                            <div class="col-xs-10 col-sm-10 no-padding-lf">
                                                <input type="password" class="login-input" name="signup-pass" id="signup-pass" autocomplete="true" placeholder="password..." value="<?=(isset($_SESSION["password"])?$_SESSION["password"]:'');?>" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 modal-input-border border-b-0">
                                            <div class="col-xs-2 col-sm-2 modal-input-label">
                                                <img src="img/locked.png" class="img-responsive">
                                            </div>
                                            <div class="col-xs-10 col-sm-10 no-padding-lf">
                                                <input type="password" class="login-input" name="signup-repass" id="signup-repass" autocomplete="true" placeholder="retype password..." value="<?=(isset($_SESSION["repassword"])?$_SESSION["repassword"]:'');?>" />
                                            </div>
                                        </div>
                                         <div class="col-xs-12 col-sm-12 modal-input-border">
                                            <div class="col-xs-2 col-sm-2 modal-input-label">
                                                <img src="img/mobile.png" class="img-responsive">
                                            </div>
                                            <div class="col-xs-10 col-sm-10 no-padding-lf">
                                                <input type="text" class="login-input" name="signup-mobile" id="signup-mobile" autocomplete="true" placeholder="10 digit mobile no" value="<?=(isset($_SESSION["mobile"])?$_SESSION["mobile"]:'');?>" />
                                            </div>
                                        </div>
                                        <div class="col-sm-12 no-padding-lf modal-login-btn">
                                            <button type="button" class="btn btn-danger pull-left signup-btn" id="signup-btn">Sign Up</button>
                                        </div>
                                    </form>
                                </div>
                          </div>
                          <div class="col-sm-6 modal-right-container no-padding-lf">
                            <div class="col-xs-12 col-sm-12 close-btn">
                                <span id="close-login-modal" class="close-span pull-right"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                            <div class="col-sm-12 login-form">
                                <div class="col-sm-10 col-sm-offset-2 login-head">
                                    <h1>Login to your acoount</h1>
                                </div>
                                <div class="col-xs-12 col-sm-12 modal-input-border border-b-0">
                                    <div class="col-xs-2 col-sm-2 modal-input-label">
                                        <img src="img/user.png" class="img-responsive">
                                    </div>
                                    <div class="col-xs-10 col-sm-10 no-padding-lf">
                                        <input type="email" class="login-input border-b-0" autocomplete="true" id="user-email" placeholder="email address..." autofocus="true" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 modal-input-border">
                                    <div class="col-xs-2 col-sm-2 modal-input-label">
                                        <img src="img/locked.png" class="img-responsive">
                                    </div>
                                    <div class="col-xs-10 col-sm-10 no-padding-lf">
                                        <input type="password" class="login-input" id="user-pass" autocomplete="true" placeholder="password..." />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 no-padding-lf modal-login-btn">
                                    <button class="btn btn-danger pull-left login-btn" id="login-btn">Login</button>
                                    <a href="" class="forget-pass-a pull-left"><span>Forget Password? </span></a>
                                </div>
                                <div class="col-xs-10 col-sm-12 col-xs-offset-1">
                                    <p class="text-center signup-p">Don't have an acoount <span class="signup-a" id="change-tab">Sign up</span> here</p>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <section class="message">
                        
                    </section>