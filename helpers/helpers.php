 
<?php 

function sanitize($dirty){
	return htmlentities($dirty);
}

function rightmessage($mes){

// var_dump($errors);


	$display =		      '<div class="container-fluid success-back">';
	$display .= 			'<div class="row">';
	$display .=         	  '<div class="col-md-6 col-md-offset-3">';
	$display .=			    	'<div class="row alert-back">';
	$display .=				    	'<button class="btn btn-primary alert-close" id="alert-close"> x </button>';
	$display .=				    	'<div class="col-md-4">';
	$display .=					   		'<img src="img/alert/1.png" class="img-responsive">';
	$display .=						'</div>';
	$display .=						'<div class="col-md-8 alert-mes">';
	$display .=                            	'<h3>Success !</h3>';
               			           			foreach ($mes as $error) {
									   			 $display .=  '<p>'.$error.'</p>';
											}
	$display .=					    '</div>';
	$display .=				     '</div>';
	$display .=					'</div>';
	$display .=				'</div>';
	return $display;

}
function wrongmessage($mes){

// var_dump($errors);


	$display =	         	 '<div class="col-xs-10 col-sm-8 col-md-3 col-xs-offset-1 col-sm-offset-2 col-md-offset-4 alert-box dash-alert-box">';
	$display .=					'<div class="col-xs-12 alert-back dash-alert-back">';
	$display .=				    	'<button class="btn btn-primary alert-close" id="alert-close"> x </button>';
	$display .=				    	'<div class="col-md-12 alert-image">';
	$display .=					   		'<img src="img/alert/2.png" class="img-responsive">';
	$display .=						'</div>';
	$display .=						'<div class="col-md-12 alert-mes text-center">';
	$display .=                            	'<h3>Alert !</h3>';
               			           			foreach ($mes as $error) {
									   			 $display .=  '<p>'.$error.'</p>';
											}
	$display .=					    '</div>';
	$display .=				     '</div>';
	$display .=					'</div>';
	return $display;

}

function get_table_row($email){
	
	global $db;
	$dbresult = $db->query("SELECT * FROM vendors Where email = '$email'");	
	$tablerow = mysqli_fetch_assoc($dbresult);
	
	$row = '<tr class="vendortablerow" id="'.$tablerow['vendor_id'].'">';
	$row .= '<td>'.$tablerow['vendor_id'].'</td>';
	$row .= '<td><img style="height: 70px; width: 70px;" src="'.$tablerow['profile_image'].'" class="img-responsive profile_image"></td>';
	$row .= '<td>'.$tablerow['full_name'].'</td>';
	$row .= '<td>'.$tablerow['mobile'].'</td>';
	$row .= '<td>'.$tablerow['email'].'</td>';
	$row .= '<td>'.$tablerow['shop_name'].'</td>';
	$row .= '<td>'.$tablerow['shop_phone'].'</td>';
	$row .= '<td>'.$tablerow['permissions'].'</td>';
	$row .= '<td>'.$tablerow['gst_no'].'</td>';
	$row .= '<td><button class="btn btn-xs btn-danger" onclick="editmodal('.$tablerow['vendor_id'].');return false;"><i class="material-icons">create</i></span></i></button><button class="btn btn-xs btn-danger" onclick="removevendor('.$tablerow['vendor_id'].');return false;"><i class="material-icons">delete_sweep</i></button></td>';
	$row .= '</tr>';

	return $row;
}
function get_brand_row($name){
	
	global $db;
	$dbresult = $db->query("SELECT * FROM brands WHERE brand_name = '$name'");	
	$tablerow = mysqli_fetch_assoc($dbresult);
	
	$row = '<tr class="tablerow" id="'.$tablerow['id'].'">';
	$row .= '<td>'.$tablerow['id'].'</td>';
	$row .= '<td><img style="height: 70px; width: 70px; border-radius: 50%;" class="table-img" src="'.$tablerow['brand_image'].'" class="img-responsive profile_image"></td>';
	$row .= '<td>'.$tablerow['brand_name'].'</td>';
	$row .= '<td><button class="btn btn-xs btn-danger" onclick="brandmodal('.$tablerow['id'].');return false;"><i class="material-icons">create</i></span></i></button><button class="btn btn-xs btn-danger" onclick="removebrand('.$tablerow['id'].');return false;"><i class="material-icons">delete_sweep</i></button></td>';
	$row .= '</tr>';

	return $row;
}


function get_category_row($name){
	
	global $db;
	$dbresult = $db->query("SELECT * FROM categories WHERE category_name = '$name'");	
	$tablerow = mysqli_fetch_assoc($dbresult);

		$parent_id = $tablerow['parent'];
		$cresult = '';

	$row = '';
	if ($tablerow['parent'] == 'parent') {
		$row = '<tr class="tablerow tr-parent" id="table'.$tablerow['id'].'">';
		$row .= '<td>'.$tablerow['id'].'</td>';
		$row .= '<td>'.$tablerow['category_name'].'</td>';
		$row .= '<td>'.$tablerow['parent'].'</td>';
		$row .= '<td><img style="height: 70px; width: 70px;" class="table-img" src="'.$tablerow['image'].'" class="img-responsive profile_image"></td>';
		$row .= '<td><button class="btn btn-xs btn-danger" onclick="categorymodal('.$tablerow['id'].');return false;"><i class="material-icons">create</i></span></i></button><button class="btn btn-xs btn-danger" onclick="removecategory('.$tablerow['id'].');return false;"><i class="material-icons">delete_sweep</i></button></td>';
		$row .= '</tr>';
	}else{	

		global $db;
		$child = $db->query("SELECT * FROM categories WHERE id = '$parent_id' ");
		$cresult = mysqli_fetch_assoc($child); 

		$row = '<tr class="tablerow tr-child" id="table'.$tablerow['id'].'">';
		$row .= '<td>'.$tablerow['id'].'</td>';
		$row .= '<td>'.$tablerow['category_name'].'</td>';
		$row .= '<td>'.$cresult['category_name'].'</td>';
		$row .= '<td><img style="height: 70px; width: 70px;" src="'.$tablerow['image'].'" class="img-responsive profile_image"></td>';
		$row .= '<td><button class="btn btn-xs btn-danger" onclick="categorymodal('.$tablerow['id'].');return false;"><i class="material-icons">create</i></span></i></button><button class="btn btn-xs btn-danger" onclick="removecategory('.$tablerow['id'].');return false;"><i class="material-icons">delete_sweep</i></button></td>';
		$row .= '</tr>';
	}

		return $row;

	
}

function get_product_row($title){
	
global $db;
$dbresult = $db->query("SELECT * FROM products WHERE title = '$title'");	
$tablerow = mysqli_fetch_assoc($dbresult);
$childID = $tablerow['category_id'];
	

$vendorid = $tablerow['vendor_id'];
global $db;
$shopnamequery = $db->query("SELECT * FROM vendors WHERE vendor_id = $vendorid");
$shopname = mysqli_fetch_assoc($shopnamequery);

$brandid = $tablerow['brand_id'];
global $db;
$brandquery = $db->query("SELECT * FROM brands WHERE id = '$brandid' ");
$brandname = mysqli_fetch_assoc($brandquery);

global $db;
$result = $db->query("SELECT * FROM categories Where id = '$childID'");
$child = mysqli_fetch_assoc($result);
$parentID = $child['parent'];
global $db;
$presult = $db->query("SELECT * FROM categories WHERE id = '$parentID' ");
$parent = mysqli_fetch_assoc($presult);
$category = $parent['category_name'].'-'.$child['category_name'];
$photos = explode(',',$tablerow['product_image']);
	

	$row = '<tr class="tablerow" id="pro'.$tablerow['id'].'">';
	$row .= '<td>'.$tablerow['id'].'</td>';
	$row .= '<td>'.$tablerow['vendor_id'].'</td>';
	$row .= '<td>'.$shopname['shop_name'].'</td>';
	$row .= '<td>'.$tablerow['title'].'</td>';
	$row .= '<td>'.money($tablerow['price']).'</td>';
	$row .= '<td>'.$brandname['brand_name'].'</td>';
	$row .= '<td>'.$category.'</td>';
	$row .= '<td><img style="height: 70px; width: 70px; border-radius: 50%;" class="table-img" src="'.$photos[0].'" class="img-responsive profile_image"></td>';
	$row .= '<td><button class="btn btn-xs btn-danger" onclick="productmodal('.$tablerow['id'].');return false;"><i class="material-icons">create</i></span></i></button><button class="btn btn-xs btn-danger" onclick="removeproduct('.$tablerow['id'].');return false;"><i class="material-icons">delete_sweep</i></button></td>';
	$row .= '</tr>';

	return $row;
}



function login($user_id){

	$_SESSION['SBUser'] = $user_id; 
	global $db; 
	$date = date("y-m-d H:i:s");
	$db->query("UPDATE users SET last_login = '$date' WHERE id = '$user_id' ");
	//header('Location: index.php');
}

$count = 0;
function vendorlogin($user_id,$user_name,$user_email,$user_mobile,$user_shop_name,$user_permission){

	 $_SESSION['SBvendor'] = $user_id; 
	 $_SESSION['SBvendorname'] = $user_name; 
	 $_SESSION['SBvendoremail'] = $user_email;
	 $_SESSION['SBvendormobile'] = $user_mobile;
	 $_SESSION['SBvendorshopname'] = $user_shop_name;
	 $_SESSION['SBvendorpermission'] = $user_permission;
	 $count = 1;

	 return $count;

	//  $_SESSION['success_flash'] = 'You are now logged in';
	// header('Location: dashborad.php');
}

function is_logged_in(){
	if (isset($_SESSION['SBvendor']) && $_SESSION['SBvendor'] > 0) {
		return 1;
	}
	return 0;
 }
function login_error_redirect(){
	$_SESSION['error_flash'] = 'You must be logged in to access that page';
	header('Location: /multivendor/dashboard/index.php');
}

function permission_error_redirect(){
	$_SESSION['error_flash'] = 'You do not have permission to access that page';
	header('Location: /multivendor/dashboard/dashboard.php');
}


function has_permission($permission = 'admin'){

	if (isset($_SESSION['SBvendor'])) {
		$user_id = $_SESSION['SBvendor'];
		global $db;
		$query = $db->query("SELECT * FROM vendors WHERE vendor_id = '$user_id'");
		$user_data = mysqli_fetch_assoc($query);
	
		$permissions = array();
		//echo $user_data['permissions'];
		$permissions = explode(',',$user_data['permissions']);
		if(in_array($permission, $permissions, true)){
			return 1;
		}
		return 0;	
	}
}


function signup($email){
	global $db; 
	$user_id_query = $db->query("SELECT * FROM users WHERE email = '$email'");
	$user = mysqli_fetch_assoc($user_id_query);
	$user_id = $user['id'];	

	$_SESSION['SBUser'] = $user_id; 
	global $db; 
	$date = date("y-m-d H:i:s");
	$db->query("UPDATE users SET last_login = '$date' WHERE id = '$user_id' ");
	//header('Location: index.php');
}


function get_ip_address() {
    $ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
    foreach ($ip_keys as $key) {
        if (array_key_exists($key, $_SERVER) === true) {
            foreach (explode(',', $_SERVER[$key]) as $ip) {
                // trim for safety measures
                $ip = trim($ip);
                // attempt to validate IP
                if (validate_ip($ip)) {
                    return $ip;
                }
            }
        }
    }
    return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
}
/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
function validate_ip($ip)
{
    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
        return false;
    }
    return true;
}
function deviceInfo()
 {
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $os_platform    = "Unknown OS Platform";
    $os_array       = array('/windows phone 10/i'    =>  'Windows Phone 10',
                            '/windows phone 8/i'    =>  'Windows Phone 8',
                            '/windows phone os 7/i' =>  'Windows Phone 7',
                            '/windows nt 10/i'     =>  'Windows 10',
                            '/windows nt 6.3/i'     =>  'Windows 8.1',
                            '/windows nt 6.2/i'     =>  'Windows 8',
                            '/windows nt 6.1/i'     =>  'Windows 7',
                            '/windows nt 6.0/i'     =>  'Windows Vista',
                            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                            '/windows nt 5.1/i'     =>  'Windows XP',
                            '/windows xp/i'         =>  'Windows XP',
                            '/windows nt 5.0/i'     =>  'Windows 2000',
                            '/windows me/i'         =>  'Windows ME',
                            '/win98/i'              =>  'Windows 98',
                            '/win95/i'              =>  'Windows 95',
                            '/win16/i'              =>  'Windows 3.11',
                            '/macintosh|mac os x/i' =>  'Mac OS X',
                            '/mac_powerpc/i'        =>  'Mac OS 9',
                            '/linux/i'              =>  'Linux',
                            '/ubuntu/i'             =>  'Ubuntu',
                            '/iphone/i'             =>  'iPhone',
                            '/ipod/i'               =>  'iPod',
                            '/ipad/i'               =>  'iPad',
                            '/android/i'            =>  'Android',
                            '/blackberry/i'         =>  'BlackBerry',
                            '/webos/i'              =>  'Mobile');
    $found = false;
    $device = '';
    foreach ($os_array as $regex => $value) 
    { 
        if($found)
         break;
        else if (preg_match($regex, $user_agent)) 
        {
            $os_platform    =   $value;
            $device = !preg_match('/(windows|mac|linux|ubuntu)/i',$os_platform)
                      ?'MOBILE':(preg_match('/phone/i', $os_platform)?'MOBILE':'SYSTEM');
        }
    }
    $device = !$device? 'SYSTEM':$device;
    return array('os'=>$os_platform,'device'=>$device);
 } 



function pretty_date($date){
	return date("M d, Y h:i A",strtotime($date));
}

//money_format//

function money($num){
	return '₹'.number_format($num,2);
}



?>
