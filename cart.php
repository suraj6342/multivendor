<?php include'includes/header.php';?>


<div class="container-fluid cart-back">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-lg-12 text-center cart-top-header">
			<h1>Your Shopping Bag</h1>
			<h4>Review of <strong>3</strong> items <strong>$199.60</strong></h4>	
			<div class="col-xs-12 col-sm-4 col-lg-4 options cart-land-options">
				<h3>Secure shopping</h3>
				<p>Lorem ipsum dolor sit amet, et sea aliquam tractatos</p>
				<div class="col-xs-6 col-sm-8 col-md-6 col-xs-offset-3 col-lg-4 col-md-offset-3 col-sm-offset-2 col-lg-offset-4 option-img cart-option-img-land">
					<img src="img/credit-card.png" class="img-responsive">
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-lg-4 options cart-land-options">
				<h3>Payment Options</h3>
				<p>Lorem ipsum dolor sit amet, et sea aliquam tractatos</p>
				<div class="col-xs-6 col-sm-8 col-md-6 col-xs-offset-3 col-lg-4 col-md-offset-3 col-sm-offset-2 col-lg-offset-4 option-img cart-option-img-land">
					<img src="img/payment-method.png" class="img-responsive">
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-lg-4 options cart-land-options">
				<h3>365 Days returns</h3>
				<p>Lorem ipsum dolor sit amet, et sea aliquam tractatos</p>
				<div class="col-xs-6 col-sm-8 col-md-6 col-xs-offset-3 col-lg-4 col-md-offset-3 col-sm-offset-2 col-lg-offset-4 option-img cart-option-img-land">
					<img src="img/return.png" class="img-responsive">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid cart-back2">
	<div class="container">
		<div class="row">
			<section>
	        <div class="wizard">
	            <div class="wizard-inner">
	                <ul class="nav nav-tabs" role="tablist">

	                    <li role="presentation" class="active">
	                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
	                            <span class="round-tab">
	                                <img src="img/cart.png" class="img-responsive">
	                            </span>
	                            <h3 class="ship-title">Shipping Cart</h3>
	                        </a>
	                        
	                    </li>

	                    <li role="presentation" class="disabled">
	                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
	                            <span class="round-tab">
	                                <img src="img/info.png" class="img-responsive">
	                            </span>
	                            <h3 class="ship-title">Shipping Info</h3>
	                        </a>
	                    </li>
	                    <li role="presentation" class="disabled">
	                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
	                            <span class="round-tab">
	                                <img src="img/payment-method.png" class="img-responsive">
	                            </span>
	                            <h3 class="ship-title">Shipping Method</h3>
	                        </a>
	                    </li>
	                </ul>
	            </div>

	            <form role="form" class="form-xs form">
	                <div class="tab-content">
	                    <div class="tab-pane active" role="tabpanel" id="step1">
	                        <div class="row">
	                        	<div class="col-sm-12 col-lg-12 cart-item-list">
	                        		<div class="col-sm-2 col-lg-2 cart-item-img-land">
	                        			<img src="img/products/laptop/5.jpg" class="img-responsive">
									</div>
	                        		<div class="col-sm-8 col-lg-8 cart-item-desc-land">
	                        			<h3>Product Heading</h3>
	                        			<p>Lorem ipsum dolor sit amet, ius dicat mucius at, mel fierent convenire repudiare an.</p>
	                        			<span>Color</span>&nbsp<span>Size</span>&nbsp<span>Quantity</span>
	                        		</div>
	                        		<div class="col-sm-2 col-lg-2 cart-item-price-land">
	                        			<h2>$65.63</h2>
	                        			<div class="delete-btn">
	                        				<span><i class="fa fa-2x fa-trash-o" aria-hidden="true"></i></span>
	                        			</div>
	                        		</div>
	                        		
	                        	</div>
	                        	<div class="col-sm-12 col-lg-12 cart-item-list">
	                        		<div class="col-sm-2 col-lg-2 cart-item-img-land">
	                        			<img src="img/products/laptop/5.jpg" class="img-responsive">
									</div>
	                        		<div class="col-sm-8 col-lg-8 cart-item-desc-land">
	                        			<h3>Product Heading</h3>
	                        			<p>Lorem ipsum dolor sit amet, ius dicat mucius at, mel fierent convenire repudiare an.</p>
	                        			<span>Color</span>&nbsp<span>Size</span>&nbsp<span>Quantity</span>
	                        		</div>
	                        		<div class="col-sm-2 col-lg-2 cart-item-price-land">
	                        			<h2>$65.63</h2>
	                        			<div class="delete-btn">
	                        				<span><i class="fa fa-2x fa-trash-o" aria-hidden="true"></i></span>
	                        			</div>
	                        		</div>
	                        		
	                        	</div>
	                        	<div class="col-sm-12 col-lg-12 cart-item-list">
	                        		<div class="col-sm-2 col-lg-2 cart-item-img-land">
	                        			<img src="img/products/laptop/5.jpg" class="img-responsive">
									</div>
	                        		<div class="col-sm-8 col-lg-8 cart-item-desc-land">
	                        			<h3>Product Heading</h3>
	                        			<p>Lorem ipsum dolor sit amet, ius dicat mucius at, mel fierent convenire repudiare an.</p>
	                        			<span>Color</span>&nbsp<span>Size</span>&nbsp<span>Quantity</span>
	                        		</div>
	                        		<div class="col-sm-2 col-lg-2 cart-item-price-land">
	                        			<h2>$65.63</h2>
	                        			<div class="delete-btn">
	                        				<span><i class="fa fa-2x fa-trash-o" aria-hidden="true"></i></span>
	                        			</div>
	                        		</div>
	                        	</div>
	                         </div>
	                        <div class="col-sm-12 col-lg-12 cart-controls">
		                        <ul class="list-inline pull-right">
		                            <li><button type="button" class="button button--shikoba button--border-thin next-step"><i class="button__icon icon fa fa-floppy-o" aria-hidden="true"></i> &nbsp Save & Continue </button></li>
		                        </ul>
	                        </div>
	                    </div>
	                    <div class="tab-pane" role="tabpanel" id="step2">
	                        <div class="row">
	                        	<div class="col-sm-12 col-lg-12">
	                        		<div class="col-sm-5 col-lg-4 col-lg-offset-1 text-center ship-address-land">
	                        			<div class=""><h4><strong> Shipping Address </strong></h4></div>
	                        			<span class="input input--kuro cart-select-inputs">
											<select name="sources" id="sources" class="custom-select sources" placeholder="Select Country">
											    <option value="profile">India</option>
											    <option value="word">US</option>
											    <option value="hashtag">Dubai</option>
											</select>
										</span>
										<span class="input input--kuro cart-select-inputs">
											<select name="sources" id="sources" class="custom-select sources" placeholder="Select City">
											    <option value="profile">Delhi</option>
											    <option value="word">Mumbai</option>
											    <option value="hashtag">Hyderabad</option>
											</select>
										</span>
										<span class="input input--kuro cart-inputs">
											<input class="input__field input__field--kuro select-input" type="text" id="input-7" />
											<label class="input__label input__label--kuro" for="input-7">
												<span class="input__label-content input__label-content--kuro select-input-label">ENTER EMAIL ADDRESS</span>
											</label>
										</span>
										<span class="input input--kuro cart-inputs">
											<input class="input__field input__field--kuro select-input" type="text" id="input-7" />
											<label class="input__label input__label--kuro" for="input-7">
												<span class="input__label-content input__label-content--kuro select-input-label">STATE</span>
											</label>
										</span>
										<span class="input input--kuro cart-inputs">
											<input class="input__field input__field--kuro select-input" type="text" id="input-7" />
											<label class="input__label input__label--kuro" for="input-7">
												<span class="input__label-content input__label-content--kuro select-input-label">ZIP CODE</span>
											</label>
										</span>
										<span class="input input--kuro cart-inputs">
											<input class="input__field input__field--kuro select-input" type="text" id="input-7" />
											<label class="input__label input__label--kuro" for="input-7">
												<span class="input__label-content input__label-content--kuro select-input-label">ADDRESS 1</span>
											</label>
										</span>
										<span class="input input--kuro cart-inputs">
											<input class="input__field input__field--kuro select-input" type="text" id="input-7" />
											<label class="input__label input__label--kuro" for="input-7">
												<span class="input__label-content input__label-content--kuro select-input-label">ADDRESS 2</span>
											</label>
										</span>
	                        		</div>
	                        		<div class="col-sm-2 col-lg-2 text-center form-divider hidden-xs">
	                        			<img src="img/divider.png" class="img-responsive">	
	                        		</div>
	                        		<div class="col-sm-5 col-lg-4 text-center ship-info-land">
	                        			<div class=""><h4><strong> Shipping Info</strong></h4></div>
	                        			<span class="input input--kuro cart-inputs">
											<input class="input__field input__field--kuro select-input" type="text" id="input-7" />
											<label class="input__label input__label--kuro" for="input-7">
												<span class="input__label-content input__label-content--kuro select-input-label">First Name</span>
											</label>
										</span>
										<span class="input input--kuro cart-inputs">
											<input class="input__field input__field--kuro select-input" type="text" id="input-7" />
											<label class="input__label input__label--kuro" for="input-7">
												<span class="input__label-content input__label-content--kuro select-input-label">Last Name</span>
											</label>
										</span>
										<span class="input input--kuro cart-inputs">
											<input class="input__field input__field--kuro select-input" type="text" id="input-7" />
											<label class="input__label input__label--kuro" for="input-7">
												<span class="input__label-content input__label-content--kuro select-input-label">Email Address</span>
											</label>
										</span>
										<span class="input input--kuro cart-inputs">
											<input class="input__field input__field--kuro select-input" type="text" id="input-7" />
											<label class="input__label input__label--kuro" for="input-7">
												<span class="input__label-content input__label-content--kuro select-input-label">Phone No</span>
											</label>
										</span>
										<span class="input input--kuro cart-select-inputs">
											<select name="sources" id="sources" class="custom-select sources" placeholder="Payment Option">
											    <option value="profile">Visa</option>
											    <option value="word">Paytm</option>
											    <option value="word">Debit Card</option>
											    <option value="hashtag">Cash On Delivery</option>
											</select>
										</span>
	                        		</div>
	                        	</div>
	                        </div>
	                        <div class="col-lg-12 cart-controls">
		                        <ul class="list-inline pull-right">
		                            <li><button type="button" class="button button--shikoba button--border-thin prev-step"><i class="button__icon icon fa fa-arrow-circle-left" aria-hidden="true"></i> Previous </button></li>
		                            <li><button type="button" class="button button--shikoba button--border-thin next-step"><i class="button__icon icon fa fa-floppy-o" aria-hidden="true"></i> &nbsp Save & Continue </button></li>
		                        </ul>
	                        </div>
	                    </div>
	                    <div class="tab-pane" role="tabpanel" id="step3">
	                        <div class="row">
	                        	<div class="col-lg-12">
	                        		<div class="col-lg-12 form-heading text-center">
		                        		<h4><strong> CARD INFO </strong></h4>
		                        	</div>
		                        	<div class="col-lg-12">
		                        		<span class="input input--kuro cart-inputs cart-inputs-sm cart-input-land">
											<input class="input__field input__field--kuro select-input" type="text" id="input-7" />
											<label class="input__label input__label--kuro" for="input-7">
												<span class="input__label-content input__label-content--kuro select-input-label">Card No (2235-6623-3232-3263)</span>
											</label>
										</span>
										<span class="input input--kuro cart-inputs cart-inputs-sm cart-input-land">
											<input class="input__field input__field--kuro select-input" type="text" id="input-7" />
											<label class="input__label input__label--kuro" for="input-7">
												<span class="input__label-content input__label-content--kuro select-input-label">Card Holder Name</span>
											</label>
										</span>
										<span class="input input--kuro cart-inputs cart-inputs-sm cart-input-land">
											<input class="input__field input__field--kuro select-input" type="text" id="input-7" />
											<label class="input__label input__label--kuro" for="input-7">
												<span class="input__label-content input__label-content--kuro select-input-label">CVV/CVC</span>
											</label>
										</span>
										<div class="col-sm-12 col-lg-12 card-expire">
											<h4 class="inline"><strong> CARD EXPIRE : </strong></h4>
											<span class="input input--kuro cart-select-inputs cart-select-inputs-sm">
												<select name="sources" id="sources" class="custom-select sources" placeholder="Month">
												    <option value="profile">Jan</option>
												    <option value="word">Feb</option>
												    <option value="word">Mar</option>
												    <option value="hashtag">Apr</option>
												</select>
											</span>
											<span class="input input--kuro cart-select-inputs cart-select-inputs-sm">
												<select name="sources" id="sources" class="custom-select sources" placeholder="Year">
												    <option value="profile">2018</option>
												    <option value="word">2019</option>
												    <option value="word">2020</option>
												    <option value="hashtag">2021</option>
												</select>
											</span>
										</div>
										
										
		                        	</div>
	                        	</div>
	                        </div>
	                        <div class="col-lg-12 cart-controls">
		                        <ul class="list-inline pull-right">
		                            <li><button type="button" class="button button--shikoba button--border-thin prev-step"><i class="button__icon icon fa fa-arrow-circle-left" aria-hidden="true"></i> Previous </button></li>
		                            <li><button type="button" class="button button--shikoba button--border-thin next-step"><i class="button__icon icon fa fa-rocket" aria-hidden="true"></i> &nbsp Place Order </button></li>
		                        </ul>
	                        </div>
	                    </div>
	                    
	                    <div class="clearfix"></div>
	                </div>
	            </form>
	        </div>
	    </section>
	   </div>
	</div>
</div>
<section class="subs-back">
						<div class="container-fluid no-padding-lf opacity-back">
							<div class="container subscribe-section">
							<div class="row">
								<nav class="navbar trans-back" role="navigation">
								    <!-- Brand and toggle get grouped for better mobile display -->
								    
								    <div class="navbar-header">
								      <button type="button" class="navbar-toggle toggle-btn" data-toggle="collapse" data-target="#navbar-collapse-1">
								        <span class="sr-only">Toggle navigation</span>
								        <span class="icon-bar fa fa-minus"></span>
								        <span class="icon-bar fa fa-minus"></span>
								      </button>
								      
								    </div>
								    <!-- Collect the nav links, forms, and other content for toggling -->
								    <div class="collapse navbar-collapse" id="navbar-collapse-1">
								     
								      <ul class="nav navbar-nav navbar-left">
								        <li><a href="#">HELP</a></li>
								        <li><a href="#">ORDER STATUS</a></li>
								        <li><a href="#">SHIPPING</a></li>
								        <li><a href="#">CONTACT US</a></li>
								      </ul>
								      <ul class="nav navbar-nav navbar-right">
								        <li><a href="#">ABOUT US</a></li>
								        <li><a href="#">VENDORS</a></li>
								        <li><a href="#">SHOP</a></li>
								        <li><a href="#">PRIVACY AND POLICY</a></li>
								      </ul>
								    </div><!-- /.navbar-collapse -->
								</nav>
								<div class="col-xs-2 col-xs-offset-5 line">
									<img src="img/line.png" class="img-responsive">
								</div>
								<div class="col-xs-12 text-center social-tab">
									<i class="fa fa-facebook-square fa-3x white-color" aria-hidden="true"></i>
									<i class="fa fa-pinterest-square fa-3x white-color" aria-hidden="true"></i>
									<i class="fa fa-twitter-square fa-3x white-color" aria-hidden="true"></i>
									<i class="fa fa-youtube-square fa-3x white-color" aria-hidden="true"></i>
									<i class="fa fa-instagram fa-3x white-color" aria-hidden="true"></i> 
								</div>
								<div class="col-xs-2 col-xs-offset-5 line">
									<img src="img/line.png" class="img-responsive">
								</div>
								<div class="col-xs-12 text-center">
									<h3 class="subs-title">SUBSCRIBE FOR NEW PRODUCTS AND NEW NEWSLETTER</h3>
								</div>
								<div class="col-xs-12 text-center">
									<span class="input input--kuro">
										<input class="input__field input__field--kuro" type="text" id="input-7">
										<label class="input__label input__label--kuro" for="input-7">
											<span class="input__label-content input__label-content--kuro">ENTER EMAIL ADDRESS</span>
										</label>
									</span>
								</div>

							</div>
							</div>
						</div>
					</section>
<?php include'includes/footer.php';?>

<script type="text/javascript">
	$(document).ready(function () {
	    //Initialize tooltips
	    //Wizard
	    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
	        var $target = $(e.target);
	        if ($target.parent().hasClass('disabled')) {
	            return false;
	        }
	    });
	    $(".next-step").click(function (e) {
	        var $active = $('.wizard .nav-tabs li.active');
	        $active.next().removeClass('disabled');
	        nextTab($active);
	    });
	    $(".prev-step").click(function (e) {
	        var $active = $('.wizard .nav-tabs li.active');
	        prevTab($active);
	    });	
	});

	function nextTab(elem) {
	    $(elem).next().find('a[data-toggle="tab"]').click();
	}
	function prevTab(elem) {
	    $(elem).prev().find('a[data-toggle="tab"]').click();
	}
</script>
<script type="text/javascript">
	$(".custom-select").each(function() {
  var classes = $(this).attr("class"),
      id      = $(this).attr("id"),
      name    = $(this).attr("name");
  var template =  '<div class="' + classes + '">';
      template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
      template += '<div class="custom-options">';
      $(this).find("option").each(function() {
        template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
      });
  template += '</div></div>';
  
  $(this).wrap('<div class="custom-select-wrapper"></div>');
  $(this).hide();
  $(this).after(template);
});
$(".custom-option:first-of-type").hover(function() {
  $(this).parents(".custom-options").addClass("option-hover");
}, function() {
  $(this).parents(".custom-options").removeClass("option-hover");
});
$(".custom-select-trigger").on("click", function() {
  $('html').one('click',function() {
    $(".custom-select").removeClass("opened");
  });
  $(this).parents(".custom-select").toggleClass("opened");
  event.stopPropagation();
});
$(".custom-option").on("click", function() {
  $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
  $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
  $(this).addClass("selection");
  $(this).parents(".custom-select").removeClass("opened");
  $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
});
</script>

