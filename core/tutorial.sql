-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2017 at 02:44 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tutorial`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=61 ;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `brand`) VALUES
(1, 'polo'),
(3, 'Nike'),
(5, 'levi'),
(59, 'adidas'),
(60, 'Coopers');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `items` text NOT NULL,
  `expire_date` datetime NOT NULL,
  `paid` tinyint(4) NOT NULL DEFAULT '0',
  `shipped` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `items`, `expire_date`, `paid`, `shipped`) VALUES
(20, '[{"id":"39","size":"small","quantity":"1"},{"id":"36","size":"small","quantity":"1"},{"id":"25","size":"medium","quantity":"1"}]', '2017-08-06 13:53:11', 1, 1),
(21, '[{"id":"64","size":"medium","quantity":2},{"id":"47","size":"25","quantity":2},{"id":"45","size":"small","quantity":2}]', '2017-08-06 14:22:03', 1, 1),
(22, '[{"id":"51","size":"23","quantity":3},{"id":"48","size":"small","quantity":3},{"id":"65","size":"small","quantity":4}]', '2017-08-06 15:27:04', 1, 1),
(23, '[{"id":"53","size":"30","quantity":3},{"id":"42","size":"medium","quantity":3},{"id":"36","size":"medium","quantity":3}]', '2017-08-06 15:42:18', 1, 1),
(24, '[{"id":"26","size":"small","quantity":3}]', '2017-08-06 20:24:21', 1, 1),
(25, '[{"id":"28","size":"medium","quantity":9}]', '2017-08-07 06:38:44', 1, 1),
(26, '[{"id":"41","size":"medium","quantity":31},{"id":"65","size":"medium","quantity":11}]', '2017-08-07 08:45:34', 1, 1),
(27, '[{"id":"44","size":"small","quantity":29},{"id":"40","size":"small","quantity":5}]', '2017-08-07 08:50:29', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `parent`) VALUES
(1, 'Men', 0),
(2, 'Women', 0),
(5, 'Boys', 0),
(6, 'Girls', 0),
(7, 'Shirt', 1),
(8, 'Pants', 1),
(9, 'Shoes', 1),
(11, 'Shirt', 2),
(12, 'Pants', 2),
(15, 'Shirts', 5),
(16, 'Pants', 5),
(17, 'shirts', 6),
(18, 'pants', 6),
(25, 'jeans', 1),
(26, 'jeans', 2),
(27, 'jeans', 6),
(28, 'watch', 1),
(30, 'watches', 5),
(31, 'watches', 6);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `list-price` decimal(10,2) NOT NULL,
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categories` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `featured` tinyint(4) NOT NULL DEFAULT '0',
  `sizes` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=69 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `price`, `list-price`, `brand`, `categories`, `image`, `description`, `featured`, `sizes`, `deleted`) VALUES
(25, 'Levis jeans', '50.00', '90.00', '5', '25', '/tutorial/images/products/multiple/e8005320f01b055c35aa83891542af67.png', 'VERY GOOD FIT AND COLOR.', 0, 'small :0:2,medium:0:2', 0),
(26, 'levis jeans', '80.00', '100.00', '5', '25', '/tutorial/images/products/60acc5413f85f1066d249eb37b538eed.png', 'good fit and nice design.', 1, 'small:-3:,medium:7:,large:8:', 1),
(27, 'polo jeans', '150.00', '200.00', '1', '25', '/tutorial/images/products/ae3c2b668a0a9e7753324039eaa0b24c.png', 'good and nice design make comfortable for it,', 0, 'small:10:2,large:95:2,medium:12:2', 0),
(28, 'polo jeans', '30.00', '50.00', '1', '25', '/tutorial/images/products/b13268ebc2edbbff94ed01a3bda1a011.png', 'old and gold polo jeans, beautiful and long lasting.', 0, 'small:1:,medium:1:', 0),
(29, 'nike jeans', '200.00', '250.00', '3', '25', '/tutorial/images/products/9d46eab83972ebad3c523e130dbbccbe.png', 'new nike jeans best fit ever and nice fucking design.', 0, 'small:25:2,medium:100:2,large:200:2', 0),
(30, 'nike jeans', '100.00', '150.00', '3', '25', '/tutorial/images/products/aa77a3fd509a56643d0414cb999d9171.png', 'new and last product of nike for fit and comfort.', 0, 'small:10:2,medium:20:2,large:100:2', 0),
(31, 'addidas jeans', '120.00', '130.00', '59', '25', '/tutorial/images/products/cf4d69c1b05b1b884416914ce757aa19.png', 'test description', 0, 'small:10:2,medium:10:2,large:10:2', 0),
(32, 'addidas jeans', '300.00', '1000.00', '59', '25', '/tutorial/images/products/56f47c0d62a9ab6a5e5950cb5b6e680d.png', '', 0, 'small:15:2,medium:15:2,large:10:2', 0),
(33, 'black cooper', '300.00', '350.00', '60', '25', '/tutorial/images/products/c473b334b4a699c1769d37e254236ead.png', 'test descrption and nice and goofd fit.', 0, 'small:200:2,medium:100:2,large:50:2', 0),
(34, 'cooper purple jeans', '123.00', '321.00', '60', '25', '/tutorial/images/products/73835e8a706bca9f831471e05d41d062.png', '', 1, 'small:20:2,medium:15:2,large:20:2', 0),
(35, 'black women jeans', '100.00', '150.00', '1', '26', '/tutorial/images/products/b3ab0baf52916a5c74adbdc9f2413db6.png', 'test description but good fit and nice figure jean.', 1, 'small:10:2,medium:15:2,large:12:2', 0),
(36, 'low jeans ', '150.00', '200.00', '3', '26', '/tutorial/images/products/ca22839a49e9ff3763c8c81ae2114dc1.png', 'nice jeans for shape booty and nice design, rough and tough.', 1, 'small:1:2,medium:2:2,large:4:2', 0),
(37, 'black jeans', '150.00', '230.00', '1', '26', '/tutorial/images/products/1ca62d1dc61249d76c5d197f187e3309.png', 'black jeans and good fir buy it.', 1, 'small:10:2,medium:100:2', 0),
(38, 'blue fit jeans', '100.00', '150.00', '3', '26', '/tutorial/images/products/9e4ac75b342f3a847daf6a54f014a209.png', 'blue color and nice cutting jeans, for figured girls,', 1, 'small:60:2,medium:100:2', 0),
(39, 'leather jeans', '200.00', '350.00', '3', '26', '/tutorial/images/products/f419c3f29c5886401f83e46fd264b110.png', 'jeans for hot girls and nice chick for bikes.', 1, 'small:68:2,large:200:2,medium:100:2', 0),
(40, 'dark women blue jeans', '300.00', '400.00', '5', '26', '/tutorial/images/products/258528ab8da094115a4dc50675edadf0.png', 'new design and levis fit.', 1, 'small:5:2,medium:20:2,large:30:2', 0),
(41, 'women fade jeans', '140.00', '360.00', '5', '26', '/tutorial/images/products/75e8542837c985c54d4f2c5605b063e0.png', 'fade and nice fit and design is awesome, melt with your body.', 0, 'small:10:2,medium:238:0,large:200:2', 0),
(42, 'blue girls jeans', '150.00', '200.00', '3', '26', '/tutorial/images/products/a23df2522abee28f831c01ee0e282186.png', 'nice fir and good looking design.', 1, 'small:99:2,medium:197:2', 0),
(43, 'girls black jeans', '130.00', '170.00', '59', '26', '/tutorial/images/products/d214aaa3a9e4373f858419f22df274b8.png', 'black and new colorful jeans', 1, 'small:100:2,medium:100:2', 0),
(44, 'skyblue girl jeans', '300.00', '500.00', '5', '26', '/tutorial/images/products/8d544f3c333ec49d9d585b5e9a2c2d0b.png', 'good fiit and stay long', 1, 'small:71:2,medium:100:2', 0),
(45, 'girls rough jeans', '30.00', '50.00', '3', '27', '/tutorial/images/products/2541e6e7976cbec66fd896530e635d0f.png', '', 1, 'small:13:2,medium:20:2,large:30:2', 0),
(46, 'addidas girl jeans', '200.00', '300.00', '59', '27', '/tutorial/images/products/38518a19e9321ee34b53620adac142a6.png', 'addidas jeans and nice fit.', 1, 'small:10:2,medium:50:2', 0),
(47, 'black jeans', '200.00', '100.00', '5', '27', '/tutorial/images/products/15d91fdd8f255915844fe62533d45bc7.png', 'blaack and nice design jeans.', 1, '25:3:2,25:4:2', 0),
(48, 'dark blue jeans ', '120.00', '130.00', '1', '27', '/tutorial/images/products/290b14fa95de7f473ee55f59bd6aa552.png', 'good and nice fit jeans.', 0, 'small:97:2,large:20:2', 0),
(49, 'girls top', '120.00', '300.00', '60', '17', '/tutorial/images/products/07a2dda2d5f493a5b89aaaf28bfe201f.png', 'good and nice fit jeans.', 1, 'small:10:2,medium:100:2', 0),
(50, 'boy white shirt', '200.00', '300.00', '3', '15', '/tutorial/images/products/93abba9a55758c23c3d039a277e29aa6.png', 'good and nice fit shirt.', 1, '26:12:2,24:20:2', 0),
(51, 'army jeans', '100.00', '200.00', '59', '16', '/tutorial/images/products/fb17b52bd993c0d8172f32420c3c4fe1.png', 'good and nice fit jeans.', 1, '23:17:2,26:10:2,22:10:2', 0),
(52, 'boy summer shirt', '1200.00', '1300.00', '59', '15', '/tutorial/images/products/ffd06c4c58949d3e53092559da5e0821.png', 'good and nice fit shirt.', 0, '22:10:2,24:100:2,26:13:2', 0),
(53, 'boy shirt', '20.00', '30.00', '1', '15', '/tutorial/images/products/92729b94cc02133e812d510293cf549f.png', 'good and nice fit shirt.', 1, '20:20:2,30:0:2,25:20:2', 0),
(54, 'summer shirt', '120.00', '130.00', '1', '15', '/tutorial/images/products/ba0b99208eb699dcf8d7f8671e3a50d2.png', 'good and nice fit shirt for vacation.', 1, '22:20:2,24:10:2,26:30:2', 0),
(64, 'new test', '123.00', '321.00', '3', '16', '/tutorial/images/products/multiple/6b05d82de99cd5b5851596a3adde1f40.jpg,/tutorial/images/products/multiple/9dbd924622dd4ec3a1e89eff3ba2ba5b.jpg', 'test description', 1, 'small:5:2,medium:4:2', 0),
(65, 'multiple image product', '130.00', '213.00', '5', '8', '/tutorial/images/products/multiple/f635daa6e01b0005e1dd547324e3faaa.jpg,/tutorial/images/products/multiple/e0e9a8700e32f98d3ed736f45829eb53.jpg,/tutorial/images/products/multiple/2e3a1f69581fcff89cf0f85f03730372.jpg,/tutorial/images/products/multiple/b8b777244232cac209fdd6ee44ba7d2a.jpg', 'multiple file description', 0, 'small:6:,medium:-11:', 0),
(66, 'polo tshirt', '1200.00', '1300.00', '5', '7', '/tutorial/images/products/multiple/dcb284c53cdbb50ace595c8dfd146e70.jpg,/tutorial/images/products/multiple/c3dfb16fb9661073f536fa7236752f40.jpg,/tutorial/images/products/multiple/774ecd4305dde4c4988200689f58ce24.jpg', 'polo tshirt new and full and half sleef both', 1, 'small:10:2,medium:10:2,large:20:3', 0),
(67, 'sss', '123.00', '521.00', '1', '16', '', '', 0, '12:12:2,30:10:2', 1),
(68, 'new shirt in town', '10.30', '20.33', '3', '15', '/tutorial/images/products/multiple/f49d3ab5148fd292206c99e81ed73a1a.jpg,/tutorial/images/products/multiple/b5aae7fcb18f2cd3ea13d78414692c34.jpg', 'new shirt in town flexible nice fit', 1, 'small:10:2,medium:5:1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_id` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(175) NOT NULL,
  `street` varchar(255) NOT NULL,
  `street2` varchar(255) NOT NULL,
  `city` varchar(175) NOT NULL,
  `state` varchar(175) NOT NULL,
  `zip_code` varchar(50) NOT NULL,
  `country` varchar(175) NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `grand_total` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `txn_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `cart_id`, `full_name`, `email`, `street`, `street2`, `city`, `state`, `zip_code`, `country`, `sub_total`, `tax`, `grand_total`, `description`, `txn_date`) VALUES
(8, '20', 'suraj singh rawat', 'suraj63421@gmail.com', 'sangam park', 'khora colony', 'ghaziabad', 'up', 'zip_code', 'country', '400.00', '34.80', '434.80', '3 items from ecommerce shop.', '2017-07-07 17:24:01'),
(9, '21', 'test pay', 'testpay@gmail.com', 'new orlin', 'street 45', 'Orlin', 'Berlin', 'zip_code', 'country', '706.00', '61.42', '767.42', '6 items from ecommerce shop.', '2017-07-07 17:54:34'),
(10, '22', 'rohit pal', 'rohit@gmail.com', 'street 45', '', 'Amsterdam', 'Canada', 'zip_code', 'country', '1180.00', '102.66', '1282.66', '10 items from ecommerce shop.', '2017-07-07 18:58:15'),
(11, '23', 'new test', 'testmail@gmail.com', 'new streetr', 'new address', 'India', 'indai', 'zip_code', 'country', '960.00', '83.52', '1043.52', '9 items from ecommerce shop.', '2017-07-07 19:13:19'),
(12, '24', 'test name ', 'test@gmail.co', '1234567', 'street 2', 'delhi', 'delhi', 'zip_code', 'country', '240.00', '20.88', '260.88', '3 items from ecommerce shop.', '2017-07-07 23:55:19'),
(13, '24', 'test name ', 'test@gmail.co', '1234567', 'street 2', 'delhi', 'delhi', 'zip_code', 'country', '240.00', '20.88', '260.88', '3 items from ecommerce shop.', '2017-07-08 00:22:51'),
(14, '25', 'test pay', 'testpay@gmail.com', 'test pay', 'new address', 'INdia', 'India', 'zip_code', 'country', '270.00', '23.49', '293.49', '9 items from ecommerce shop.', '2017-07-08 10:09:41'),
(15, '26', 'sanjay goyal', 'suraj63421@gmail.com', 'stree 30', 'new lane', 'Berlin', 'Moscow', 'zip_code', 'country', '5770.00', '501.99', '6271.99', '42 items from ecommerce shop.', '2017-07-08 12:17:07'),
(16, '26', 'sanjay goyal', 'suraj63421@gmail.com', 'stree 30', 'new lane', 'Berlin', 'Moscow', 'zip_code', 'country', '5770.00', '501.99', '6271.99', '42 items from ecommerce shop.', '2017-07-08 12:19:37'),
(17, '27', 'priyanka roda', 'suraj63421@gmail.com', 'new street', 'kali badi', 'New Delhi', 'New Delhi', 'zip_code', 'country', '10200.00', '887.40', '11087.40', '34 items from ecommerce shop.', '2017-07-08 12:21:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(175) NOT NULL,
  `password` varchar(255) NOT NULL,
  `join_data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime NOT NULL,
  `permissions` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `password`, `join_data`, `last_login`, `permissions`) VALUES
(1, 'suraj rawat', 'suraj63421@gmail.com', '$2y$10$8dXg34p1x0kbmoIn5RqVMuTHnpl9MfRurGLr.AGQ7lojtBamMoCxu', '2017-06-28 12:32:14', '2017-07-08 06:41:54', 'admin,editor');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
