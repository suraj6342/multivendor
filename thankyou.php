<?php 
require'core/db.php';


?>

<!doctype html>
<html lang="en" >
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<script src="js/jquery.min.js"></script>		
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.2.0/css/font-awesome.min.css" />
	<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/wow.min.js"></script>
	<script>
		new WOW().init();
	</script>
	<title>ThankYou</title>

	<style type="text/css">
		body{
			color: azure !important;
		}
	</style>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 upper-head">
			<div class="col-md-8 col-md-offset-2 wow slideInRight">
				<h1 class="text-center first">Multivendor</h1>
				<h1 class="text-center second">Thank You</h1>
				<h3 class="text-center third"><strong>FOR SHOPPING WITH US</strong></h3>
			</div>
		</div>
	</div>
	<div style="background-image: url(img/new/rodion-kutsaev-24833.jpg); background-position: bottom; background-repeat: no-repeat; background-size: cover; padding-top: 100px; padding-bottom: 60px; " class="row">
		<div class="col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2 padding-tb-40 wow slideInLeft">
			<h3 class="text-center">Your payment has been successfully Paid. You have been emailed a receipt. Please check ypur spam folder if it is not in your inbox. Additionally You can print this page as a receipt.</h3>
		</div>
		<div class="col-md-8 col-md-offset-2">
			<table class="table">
			    <thead>
			      <tr>
			        <th class="text-center">Receipt No</th>
			        <th class="text-center">Name</th>
			        <th class="text-center">Amount</th>
			        <th class="text-center">Address</th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr class="text-center">
			        <td><strong></strong></td>
			        <td></td>
			        <td></td>
			        <td></td>
			      </tr>
			    </tbody>
			 </table>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 padding-tb-40">
			<div class="col-xs-12 col-sm-5 col-md-5 wow slideInLeft">
				<img src="img/email.png" class="img-responsive">
			</div>
			<div class="col-xs-12 col-sm-7 col-md-7 wow slideInRight">
				<h2>Check your email for gift voucher</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, dolore, impedit eveniet necessitatibus voluptate distinctio quam repellendus voluptates voluptatum inventore rem sapiente minus esse saepe iste harum architecto numquam quis </p>
			</div>
		</div>
		<div style="margin-top: 50px;" class="col-xs-10 col-md-6 col-xs-offset-1 col-md-offset-3 padding-tb-40">
			<div class="text-center">
				<a href="index.php">Back to homepage</a>
			</div>
			<h3 class="text-center">Join us on social media</h3>
			<div id="social wow pulse" class="text-center">
				<a class="facebookBtn smGlobalBtn" href="#" ></a>
				<a class="twitterBtn smGlobalBtn" href="#" ></a>
				<a class="googleplusBtn smGlobalBtn" href="#" ></a>
				<a class="linkedinBtn smGlobalBtn" href="#" ></a>
				<a class="pinterestBtn smGlobalBtn" href="#" ></a>
				<a class="tumblrBtn smGlobalBtn" href="#" ></a>
				<a class="rssBtn smGlobalBtn" href="#" ></a>
			</div>
	
		</div>
	</div>
	<div class="row">
		<div style="padding:25px 0px; border-top: 1px solid #000; background-color: #2c3e50; color: white;" class="text-center col-xs-12">© Copyright 2013-2015 Multivendor</div>
	</div>
</div>
</body>
</html>