<?php	

require_once $_SERVER['DOCUMENT_ROOT'].'/multivendor/core/db.php';

//login ajax
$login_email = sanitize($_POST['login_email']);
$login_pass = sanitize($_POST['login_pass']);
$errorss = array();
$login_count = 0;	

				if(empty($login_email) || empty($login_pass)){
					$errorss[] = 'You must provide email and password';
				}
				//VALIDATE EMAIL

				if (!filter_var($login_email, FILTER_VALIDATE_EMAIL)) {
					$errorss[] = 'You must enter a valid email address'; 
				}

				//password is more than 6 charackter
				if (strlen($login_pass) < 6 ) {
					$errorss[] = 'Password must be atleast 6 character';
				}

				//check if user exist in database

				$query = $db->query("SELECT * FROM users WHERE email = '$login_email'");
				$user = mysqli_fetch_assoc($query);
				$usercount = mysqli_num_rows($query);
				//echo $usercount;
				if ($usercount < 1){

					$errorss[] = 'That email does not exist in our database'; 
				}

				if(!password_verify($login_pass, $user['password'])){
					$errorss[] = 'The password does not match our records. Please try again';
				}
				//check for errors
				if(!empty($errorss)){
					echo wrongmessage($errorss);
				}
				else{
					//start session  user to db
							$user_id = $user['id'];
							login($user_id);
							
							$login_count = 1;							
							echo $login_count;
				 }

?>