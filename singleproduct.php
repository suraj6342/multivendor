<?php include'includes/header.php';?>


<div class="container-fluid">
	<div class="row">
	</div>
</div>
<div class="container-fluid main-back1">
	<div class="row main-back2">
		<div class="col-xs-12 col-sm-12 col-lg-12 single-main-section">
			<div class="col-lg-12 product-heading">
				<h1 class="text-center">APPLE</h1>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 col-xs-offset-4 col-md-offset-4 col-sm-offset-4 col-lg-offset-5 p-img">
				<img src="img/apple-logo.png" class="img-responsive">
			</div>	
		</div>

		<div class="col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 back1 no-padding-lf">
			<div class="col-sm-12 col-md-12 col-lg-12 padding-tb">
				<div class="col-sm-6 col-md-6 col-lg-7 back2 no-padding-lf">
					<div class="col-sm-9 col-md-9 col-lg-6 p-price">
						<h1>₹35,000</h1>
						<h5>Electronics - Mobile - Brand</h5>
					</div>
					<div class="col-sm-9 col-md-9 col-lg-6 p-image">
						<div id="carousel-example-generic-single" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->
						  <ol class="carousel-indicators carosel-indicator-single">
						    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
						    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
						  </ol>

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						    <div class="item active">
						      <img src="img/iphone.png" class="img-responsive">
						    </div>
						    <div class="item">
						      <img src="img/iphone.png" class="img-responsive">
						    </div>
						    <div class="item">
						      <img src="img/iphone.png" class="img-responsive">
						    </div>
						    
						  </div>

						  <!-- Controls -->
						  <a class="left carousel-control slider-contols" href="#carousel-example-generic-single" role="button" data-slide="prev">
						    <span class="glyphicon glyphicon-chevron-left slider-contols-span-single" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control slider-contols" href="#carousel-example-generic-single" role="button" data-slide="next">
						    <span class="glyphicon glyphicon-chevron-right slider-contols-span-single" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
						
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-5 single-p-desc">
					<div class="col-sm-12 col-md-12 col-lg-12 size-form">
						<div class="form">
							<p class="form__answer"> 
								<input type="radio" name="match" id="match_1" value="16gb" class="size-input" checked> 
								<label for="match_1" class="size-label">
									16Gb
								</label> 
							</p>
							<p class="form__answer"> 
								<input type="radio" name="match" id="match_2" value="32gb" class="size-input"> 
								<label for="match_2" class="size-label">
									32Gb
								</label> 
							</p>
							<p class="form__answer"> 
								<input type="radio" name="match" id="match_3" value="64gb" class="size-input"> 
								<label for="match_3" class="size-label">
									64Gb
								</label> 
							</p>
							<p class="form__answer"> 
								<input type="radio" name="match" id="match_4" value="128gb" class="size-input"> 
								<label for="match_4" class="size-label">
									128Gb
								</label> 
							</p>
						</div>
					</div>
					<div class="col-sm-12 col-md-12 col-lg-12 color-form">
						<input type="radio" name="color" id="red" value="red" class="color-input" />
						<label for="red" class="color-label"><span class="red"></span></label>

						<input type="radio" name="color" id="green" class="color-input" />
						<label for="green" class="color-label"><span class="green"></span></label>

						<input type="radio" name="color" id="yellow" class="color-input" />
						<label for="yellow" class="color-label"><span class="yellow"></span></label>

						<input type="radio" name="color" id="olive" class="color-input" />
						<label for="olive" class="color-label"><span class="olive"></span></label>

						<input type="radio" name="color" id="orange" class="color-input" />
						<label for="orange" class="color-label"><span class="orange"></span></label>

						<input type="radio" name="color" id="teal" class="color-input" />
						<label for="teal" class="color-label"><span class="teal"></span></label>

						<input type="radio" name="color" id="blue" class="color-input" />
						<label for="blue" class="color-label"><span class="teal"></span></label>

						<input type="radio" name="color" id="violet" class="color-input" />
						<label for="violet" class="color-label"><span class="violet"></span></label>

						<input type="radio" name="color" id="purple" class="color-input" />
						<label for="purple" class="color-label"><span class="purple"></span></label>

						<input type="radio" name="color" id="pink" class="color-input" />
						<label for="pink" class="color-label"><span class="pink"></span></label>
					</div>
					<div class="col-sm-12 col-md-12 col-lg-12 p-description">
						<div class="col-sm-8 col-md-8 col-lg-8 no-padding-lf">
							<p>Lorem ipsum dolor sit amet, nec quas virtute ei, no amet quaerendum necessitatibus vis, errem definitiones ut per. Ei nec audiam reprimique, phaedrum</p>
						</div>
						<div class="col-sm-4 col-md-4 col-lg-4 p-heading">
							<h1>product heading</h1>
						</div>
						<div class="col-sm-12 col-md-12 col-lg-12">
							<button class="circle pull-right">
						    	<img src="img/cart-btn.png" alt="" />
						  	</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
						<div class="row">
							 <div class="wrapper margin-b-160">
							 	<div class="col-sm-12 text-center">
							 		<h1 class="related-head">Related Product</h1>
							 	</div>
					            <div class="jcarousel-wrapper jcarousel-wrapper-xs ">
					                <div class="jcarousel">
					                    <ul>
					                        <li>
					                        	<img src="img/products/laptop/3.jpg" alt="Image 1">
					                        	<div class="product-details">
						                        	<h3> ₹ 12,000 </h3>
						                        	<h4> Apple Laptop </h4>
						                        	<h5> Electronics - Laptops </h5>
					                        	</div>
					                        </li>
					                        <li>
					                        	<img src="img/products/datacable/1.jpg" alt="Image 2">
					                        	<div class="product-details">
						                        	<h3> ₹ 650 </h3>
						                        	<h4> Mobile Datacable </h4>
						                        	<h5> Mobile - Accessories  </h5>
					                        	</div>

					                        </li>
					                        <li>
					                        	<img src="img/products/headphone/1.jpg" alt="Image 3">
					                        	<div class="product-details">
						                        	<h3> ₹ 3,000 </h3>
						                        	<h4> Beats Headphone </h4>
						                        	<h5> Electronics - Headphones </h5>
					                        	</div>
					                        </li>
					                        <li>
					                        	<img src="img/products/phonecover/5/1.jpg" alt="Image 4">
												<div class="product-details">
						                        	<h3> ₹ 900 </h3>
						                        	<h4> Iphone 7 Cover </h4>
						                        	<h5> Mobile - Accessories </h5>
					                        	</div>
					                        </li>
					                        <li>
					                        	<img src="img/products/shoes/4/12.jpg" alt="Image 5">
					                        	<div class="product-details">
						                        	<h3> ₹ 9,000 </h3>
						                        	<h4> Nike Air </h4>
						                        	<h5> Fitness ~ Shoes </h5>
					                        	</div>
					                        </li>
					                        <li>
					                        	<img src="img/products/xbox/1.jpg" alt="Image 6">
					                        	<div class="product-details">
						                        	<h3> ₹ 37,000 </h3>
						                        	<h4> Xbox - One </h4>
						                        	<h5> Electronics - Games </h5>
					                        	</div>
					                        </li>
					                    </ul>
					                </div>
					                <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
					                <a href="#" class="jcarousel-control-next">&rsaquo;</a>
					            </div>
					        </div>
						</div>
					</div>

<?php include'includes/footer.php';?>
	